# pysctrack (python tools for SDtracker)

## 0. Introduction
Python tools for handling spacecraft data processed with the software spectrometer (swspec) and the multi-tone spacecraft tracking software (sctracker). 

Main functionalitiies are:
* Visualize and plot spectra
* Quantify Doppler measurements
* Digital Phase Lock Loop
* Estimate scintillation measurements

## 1. Installation

> python setup.py install (--user)

or using flit

> flit install 

and it will install automatically all the depedencies.

## 2. Requirements

0. Python>=3.8
1. numpy
2. matplotlib
3. pandas
4. altair (opt)
5. pyproj (opt)

## 3. Structure

* bin/ contains all the executable python script. Use -h option for additional help
* cats/ catalogs and auxiliary files
* nb/ jupyter notebooks
* pysctrack/ includes all the libraries used for handling the spacecraft data
