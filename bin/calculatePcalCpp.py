#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 26 21:08:56 2020

Calculate the first polynomial fit to the full bandwidth
of spectra for the PCal signal
@author: gofrito
"""

import argparse
import errno
import os
import re
import time
from dataclasses import dataclass

import matplotlib.pyplot as plt
import numpy as np

from pysctrack import core, handler


class calculate_pcal_cpp:
    def __init__(self):
        self.start = time.perf_counter()
        self.Npf: int = 1
        self.Npp: int = self.Npf + 1
        self.t0: int = 0
        self.t1: int = 0

        parser = argparse.ArgumentParser()
        parser.add_argument("filename", nargs="?", help="Input spectra file", default="check_string_for_empty")
        parser.add_argument("-p", "--plot", help="Plot some results for Pcal", action="store_true", default=0)
        parser.add_argument("-bw", dest="bandwidth", help="Select the recording bandwidth", type=float, default=16)
        parser.add_argument("-zf", dest="zoom_frequency", help="enter the boundaries of the freq search window [MHz]", nargs="+", type=float, metavar="N")
        parser.add_argument("-rf", dest="radio_frequency", help="Sky frequency of the channel", type=float, default=8400)
        parser.add_argument("-ep", dest="epoch", help="specify your own epoch - format ex. 2020.02.02", type=str)

        args = parser.parse_args()

        self.plot: bool = args.plot

        self.Fstart: float = args.radio_frequency

        # Check if the file exists
        if os.path.exists(args.filename):
            path, self.filename = os.path.split(args.filename)
            self.path = f"{os.path.abspath(path)}/"
        else:
            raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), args.filename)

        """
        Filename format : Session name + Station + Data Format + scan number + FFT points + integration time + channel + _swpec.bin
        Example: m200101_Mh_VDIF_No0001_3200000pt_5s_ch1_swspec.bin
        Split the file in 7 fields:
        """
        match = re.search(r"^([^_]*)_([^_]*)_(?:([^_]*)_)?(?:N)?([oabcdefgh])?(\d+)_(\d+)(?:pt)?_(\d+)s?_(?:ch)?([^_]+)_(?:swspec|scspec)", self.filename, re.I)

        # Extract all observing params from the filename
        self.session: str = match.group(1)
        self.station: str = match.group(2)
        self.raw_format: str = match.group(3)
        self.ifband: str = match.group(4)
        self.scan_number: str = match.group(5)
        self.FFTpt = int(match.group(6))
        self.dts = int(match.group(7))
        self.channel: int = match.group(8)

        self.spacecraft: str = "cal"

        if args.bandwidth:
            self.BW: float = args.bandwidth
        if args.zoom_frequency:
            self.Fmin, self.Fmax = args.zoom_frequency
        else:
            self.Fmin, self.Fmax = 5.95, 6.05

        # print if the Fdets are correct
        print(f'Freq Limits: {self.Fmin}:{self.Fmax}')
        # Convert to MHz
        self.BW *= 1e6
        self.Fmin *= 1e6
        self.Fmax *= 1e6

        # fill the epoch variable
        if args.epoch:
            self.epoch: str = args.epoch
        else:
            self.epoch: str = f"20{self.session[1:3]}.{self.session[3:5]}.{self.session[5:7]}"

    def peak_detection(self):
        # if t1 = 0 use the entire file, otherwise use t1
        if self.t1:
            self.Nspec = int(self.t1 / self.dts)
        else:
            fsize: int = os.path.getsize(f"{self.path}{self.filename}")
            self.Nspec: int = int(np.floor(0.5 * fsize / (self.FFTpt + 1)))
            self.t1: float = self.Nspec * self.dts

        print(f"Fmin : {self.Fmin*1e-3:.2f} kHz - Fmax : {self.Fmax*1e-3:.2f} kHz")

        print(f"TimeS : {self.t1} s - Num Spectra : {self.Nspec}")

        SR: int = 2 * self.BW
        dtsam: float = 1 / SR
        df: float = SR / self.FFTpt
        # Increase the bandwidth to calculate properly the SNR
        Hwin: int = int(2e3 / df)
        Avoid: int = int(1e3 / df)
        Nbins: int = int(self.FFTpt / 2 + 1)
        jf = np.arange(Nbins)
        self.ff = df * jf
        self.tsp = self.dts * (0.5 + np.arange(self.Nspec))

        # Read file
        fd = open(f"{self.path}{self.filename}", "rb")

        self.bfs: int = int(self.Fmin / df)
        self.bfm: int = int(self.Fmax / df)

        Sps = np.zeros((self.Nspec, self.bfm - self.bfs))
        self.Aspec = np.zeros((self.bfm - self.bfs))
        ffs = self.ff[self.bfs : self.bfm]
        Tspan = np.max(self.tsp)

        for ip in np.arange(self.Nspec):
            read_data = np.fromfile(file=fd, dtype="float32", count=Nbins)
            vspec = read_data * np.hanning(Nbins)
            Sps[ip] = vspec[self.bfs : self.bfm]
            self.Aspec = Sps[ip] + self.Aspec

        # close the spectra file
        fd.close()

        # Average all spectra and copy the last one to lSpec for plotting purposes
        self.Aspec = np.sum(Sps, axis=0) / self.Nspec
        mSp: float = Sps.max()
        Sps = Sps / mSp
        self.lSpec = read_data

        xfc = np.zeros((self.Nspec, 3))
        RMS = np.zeros((self.Nspec, 3))
        Smax = np.zeros(self.Nspec)
        SNR = np.zeros(self.Nspec)
        self.Fdet = np.zeros(self.Nspec)
        dxc = np.zeros(self.Nspec)
        MJD = np.zeros(self.Nspec)

        # 'Seeking for the Max and estimating the RMS'
        for ip in np.arange(self.Nspec):
            xfc[ip] = core.FindMax(Sps[ip], ffs)
            Smax[ip] = xfc[ip, 2]
            self.Fdet[ip] = df * xfc[ip, 1] + ffs[0]
            RMS[ip] = core.GetRMS(Sps[ip], xfc[ip, 1], Hwin, Avoid)
            SNR[ip] = (xfc[ip, 2] - RMS[ip, 0]) / RMS[ip, 1]

        mSNR = SNR.mean()

        # Calculating the centre of the gravity correction
        for ip in np.arange(self.Nspec):
            dxc[ip] = core.PowCenter(Sps[ip], xfc[ip, 1], 3)

        dxc = dxc * df
        FdetC = self.Fdet + dxc
        Weight = np.power(SNR / mSNR, 2)

        Cfs = np.zeros(self.Npp)
        Cps = np.zeros(self.Npp + 1)
        Cpr = np.zeros(self.Npp + 1)

        Cf = core.PolyfitW1C(self.tsp, FdetC, Weight, self.Npf)
        Ffit = core.PolyfitW1(self.tsp, FdetC, Weight, self.Npf)
        rFit = FdetC - Ffit

        for ip in np.arange(self.Npp):
            Cfs[ip] = Cf[ip] * np.power(Tspan, -ip)

        for ip in np.arange(1, self.Npp + 1):
            Cps[ip] = 2 * np.pi * Cfs[ip - 1] / ip
            Cpr[ip] = Cps[ip] * np.power(dtsam, ip)

        basefilen = np.size(self.filename) - 12

        cppname = f"{self.filename[0:basefilen]}.poly{self.Npp}.txt"
        cfsname = f"{self.filename[0:basefilen]}.X{self.Npf}cfs.txt"

        # Let's force the second order of polys at 0
        Cpr[-1] = Cfs[-1] = Cpr[0]

        np.savetxt(cppname, Cpr)
        np.savetxt(cfsname, Cfs)

        # Reading starting time of the scan
        timebin = f"{self.filename[0:basefilen]}_starttiming.txt"
        Tsinfo = np.loadtxt(timebin, skiprows=1)

        self.Start = Tsinfo[1]
        MJD[0 : self.Nspec] = Tsinfo[0]

        self.tsp += self.Start

        self.fdets = np.array([MJD, self.tsp, SNR, Smax, FdetC, rFit])
        print(f'{self.fdets[1,:].min()}---{self.fdets[1,:].max()}')
        handler.write_fdets(self, 0)

        print(f"\n\033[94m- Initial frequency  : {self.Fstart + FdetC[0] / 1e6:.2f} GHz")

        print(f"- Average SNR        : {mSNR:.2f}")
        print(f"- Doppler noise      : {rFit.std():.3f} Hz")
        print(f"- Total elapsed time : {time.perf_counter() - self.start:.3f} s\033[0m\n")

    def print_plots(self):
        if self.plot:
            print(f"Creating the Figure for plotting the spectra")
            print(f"Select the figure that you would like to display")
            print(f"1- Last spectrum read from the file - full bandwdith")
            print(f"2- Averaged time-integrated spectra - Span zoomed")
            print(f"3- Frequency detections of the S/C signal")
            print(f"4- Summary of the Signal detection")
            print(f"5- Summary of Frequency detection")

            graph = input("Make a choice: 1-2-3-4-5\n")
            print(graph)
            if graph == "1":
                plt.semilogy(self.ff, self.lSpec)
                plt.ylabel("Spectrum")
                plt.xlabel("Freq [Hz]")
                plt.title(f"Last spectrum read from the file")
                plt.show()

            if graph == "2":
                plt.semilogy(self.ff[self.bfs : self.bfm], self.Aspec)
                plt.ylabel("Spectra")
                plt.xlabel("Freq [Hz]")
                plt.title(f"Averaged time-integrated spectra")
                plt.show()

            if graph == "3":
                plt.plot(self.Fdet, "ro")
                plt.xlim(0, self.Nspec)
                plt.ylim(self.Fmin, self.Fmax)
                plt.title(f'Frequency detections of the phase cal tone')
                plt.show()

            if graph == "4":
                ax1 = plt.subplot2grid((2, 2), (0, 0), colspan=2)
                ax1.semilogy(self.ff, self.lSpec)
                plt.title('Summary I - Full, Zoom and Fdets')
                ax2 = plt.subplot2grid((2, 2), (1, 0))
                ax2.semilogy(self.ff[self.bfs : self.bfm], self.Aspec)
                ax3 = plt.subplot2grid((2, 2), (1, 1))
                ax3.plot(self.tsp + self.t0, self.Fdet, "ro")
                plt.tight_layout()
                plt.show()

            if graph == "5":
                ax1 = plt.subplot2grid((3, 1), (0, 0), colspan=1)
                ax1.plot(self.fdets[1, :] - self.Start, self.fdets[3, :], "r")
                plt.title(f'Summary II - Fdets')
                plt.ylabel(f'SNR')
                ax2 = plt.subplot2grid((3, 1), (1, 0), colspan=1)
                ax2.plot(self.fdets[1, :] - self.Start, self.fdets[4, :], "bo")
                plt.ylabel(f'Frequency')
                ax3 = plt.subplot2grid((3, 1), (2, 0), colspan=1)
                ax3.plot(self.fdets[1, :] - self.Start, self.fdets[5, :], "rx")
                plt.ylabel(f'Dnoise')
                plt.show()


first_polynomials = calculate_pcal_cpp()
first_polynomials.peak_detection()
first_polynomials.print_plots()
