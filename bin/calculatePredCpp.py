#!/usr/bin/env python
"""
CalculateCppFromPreds.py - Calculate the first polynomial from the given spectra in
    binary format.

User calculatePredCpp.py -h to see the help menu
@author: tbahamon
v0.2 GMC: ported to pysctrack
v0.1 TBB: first version
"""

import argparse
import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from astropy.table import Table
from astropy.time import Time, TimeDelta
from pandas.tseries.offsets import Second

from pysctrack import core, handler

# hard code values to have an output matching VLBI style
'''
channel: int = 1
FFTpt: int = 3200000
scan_number: int = 1
session: str = 'm210000'
station: str = 'Mh'
raw_format: str = 'VDIF'
base_fn: str = f'{path}/{session}_{station}_{raw_format}_No{scan_number:04d}_{FFTpt}pt_{dts}s_ch{channel}'
'''
path: str = os.path.curdir
base_fn: str = f'{path}/m210000_Yg_VDIF_No0001_3200000pt_5s_ch1'

# initialise
parser = argparse.ArgumentParser()

# input parameters
parser.add_argument("-it", dest="integration_time", help="define integration when is a fraction of 1", type=int, default=10)
parser.add_argument("-p", dest="plot", help='plot results', type=bool, default=False)
parser.add_argument("-p1", dest="polynomial", help="order of the first polynomial fit", type=int, default=6)
parser.add_argument("-rf", dest="radio_frequency", help="initial sky freq of the channel", type=float, default=8400)
parser.add_argument("-t0", dest="ini_time", help="initial time within the scan", type=int, default=0)
parser.add_argument("-t1", dest="end_time", help="end time within the scan", type=int, default=1000)
parser.add_argument("-u0", dest="utcstart", help="UTC start", type=str, default='2021-03-21T10:03:00')
parser.add_argument("--mode", help="Define input ramp table", type=int, default=0)
parser.add_argument("--max_rows", help="Different file formats, select amount of lines to read", type=int, default=73)
parser.add_argument("--skip_rows", help="Different file formats, select amount of lines to skip", type=int, default=25)
parser.add_argument("filename", nargs='?', help="Input spectra file", default="check_string_for_empty")

args = parser.parse_args()

# Set observation-dependant parameters
t0: int = args.ini_time
t1: int = args.end_time
Npp: int = args.polynomial
Npf: int = Npp - 1
dts: int = args.integration_time

# Format 0 and 1:
# We may have different input files for the frequency predictions
format: int = args.mode

# utc0: start time of the observation
utc0 = Time(args.utcstart, format='isot')

# epoch0: midnight time on the day of he observation
epoch0 = Time(f"{args.utcstart.split('T')[0]}T00:00:00.000", format='isot')

# At the moment open the file, read amount of lines select it manually
skip_rows: int = args.skip_rows
max_rows: int = args.max_rows - skip_rows

# Fstart: initial frequency of the baseband filter
Fstart: float = args.radio_frequency * 1e6

# Initialise parameters for the polynomial fit
b0: int = int(t0 / dts)
b1: int = int(t1 / dts)
tsp: float = dts * (0.5 + np.arange(b0, b1))
Nspek: int = b1 - b0

# Read input ramp file
file_preds: str = args.filename

'''
Data Format 1:
F X-BAND 1-WAY     START=21/158 13:12:18 END=21/159 01:18:03                  
   TIME        FREQUENCY (HZ)      D2N          D2N+1        D4N         D4N+1
13:12:18.000   8405091627.2866      -96.888      -91.612        8.34        2.88
13:42:05.588   8405093798.8356    -1007.537     -368.042      390.78      845.40

Data Format 2: 
TRANSMITTER U.T.                 RTT           DOPPLER     AZI      DEC      RA       EL        FREQ           D-RTT   
2021 MAR 21 10:30:00       0.2506699316   -39904.175  43.2523  24.4937 172.0972  15.7170  7159489904.175  0.0000000000
'''
# Load the ascii file to a numpy array
if format == 0:
    preds = np.loadtxt(file_preds, skiprows=skip_rows, max_rows=max_rows, \
        dtype={'names': ('TimeString', 'FPreds', 'corr1', 'corr2', 'corr3', 'corr4'), \
        'formats': ('S12', float, float, float, float, float)})
elif format == 1:
    preds = np.loadtxt(file_preds, skiprows=skip_rows, max_rows=max_rows, \
        dtype={'names': ['year', 'month', 'day', 'hour', 'rtt', 'doppler', 'ra', 'dec', 'az', 'el', 'FPreds', 'drtt'], \
        'formats': [int, 'S3', int, 'S8', float, float, float, float, float, float, float, float]})

# Convert to Data Frame
df_preds = pd.DataFrame(data=preds)

# format 0: Need to add epoch0 to the Time stamps
if format == 0:
    total_samples = len(preds['FPreds'])
    SecondsFromNight = np.zeros(total_samples)

    # Go through time sample and convert to seconds since midnight
    for ip in range(total_samples):
        tmp = preds['TimeString'][ip].decode('utf8')
        start_time = int(tmp.split(':')[0]) * 3600 + int(tmp.split(':')[1]) * 60 + float(tmp.split(':')[2])
        SecondsFromNight[ip] = start_time

    # Now add epoch0 + seconds since midnight
    utc = epoch0 + TimeDelta(SecondsFromNight, format='sec')

    # Convert Astropy Time values to data frame - We use Astropy Table
    t = Table()
    t['TimeUTC'] = utc
    df_preds['TimeUTC'] = t.to_pandas()
    
    # Clean columns
    df_preds.pop('TimeString')
elif format == 1:
    for ip in range(df_preds.shape[0]):
        df_preds.loc[ip, ('TimeUTC')] = Time.strptime(f"{df_preds['year'][ip]}-{df_preds['month'][ip].decode('utf-8')}-{df_preds['day'][ip]} {df_preds['hour'][ip].decode('utf-8')}", '%Y-%b-%d %H:%M:%S')

    # Clean columns
    df_preds.pop('year')
    df_preds.pop('month')
    df_preds.pop('day')
    df_preds.pop('hour')

# Add together all contributions
if format == 0:
    Fpreds0: float = df_preds['FPreds'] + df_preds['corr1'] + df_preds['corr2'] + df_preds['corr3'] + df_preds['corr4']
elif format == 1:
    Fpreds0: float = df_preds.FPreds

# Just plot the results now
debug = args.plot
if debug:
    plt.plot(utc.to_value('mjd') - epoch0, Fpreds0)
    plt.show()

# Now define the frames where we want to calculate polynomials
if format == 0:
    # utc0 is set by the user - start of recording scan (t0: offset - t1: end of scan)
    UTCstart = utc0 + TimeDelta(t0, format='sec')
    UTCend = utc0 + TimeDelta(t1, format='sec')

    # tt is the time sample in the scan
    tt = np.arange(UTCstart, UTCend, TimeDelta(dts, format='sec'))  

    Ntt: int = len(tt)
    tt_JD: int = np.zeros(Ntt)

    # convert each tt sample to jd
    for ip in range(Ntt):
        tt_JD[ip] = tt[ip].to_value('jd')

    # convert the full range of time samples to jd
    preds_JD = utc.to_value('jd')
elif format == 1:
    # set up correct initial times
    UTCstart = utc0 + TimeDelta(t0, format='sec')
    UTCend = utc0 + TimeDelta(t1, format='sec')

    tt = np.arange(UTCstart, UTCend, TimeDelta(dts, format='sec'))

    Ntt: int = len(tt)
    tt_JD: int = np.zeros(Ntt)

    for ip in range(Ntt):
        tt_JD[ip] = tt[ip].to_value('jd')

    # convert the full range of time samples to jd
    preds_JD = utc.to_value('jd')

# Convert frequency to baseband
Fpreds0 -= Fstart

# Interpolate time to extract Frequencies
Fpreds: float = np.interp(tt_JD, preds_JD, Fpreds0)

# Initialize the Polynomial coefficients
Cfs: float = np.zeros(Npp)
Cps: float = np.zeros(Npp + 1)
Cpr: float = np.zeros(Npp + 1)

# Take into account if ini time different than 0
tsp -= t0
Tspan: float = tsp.max()
Weight: float = np.ones(Nspek)

# Calculate polys and fit
Cf: float = core.PolyfitW1C(tsp, Fpreds, Weight, Npf)

# Calculate the phase polynomials
for jpf in range(Npp):
    Cfs[jpf] = Cf[jpf] * np.power(Tspan, -jpf)

# Calculate all polynomials set
for jpf in range(1, Npp + 1):
    Cps[jpf] = 2 * np.pi * Cfs[jpf - 1] / jpf
    Cpr[jpf] = Cps[jpf] * np.power(dts, jpf)

# Save the polynomial coefficients into a file
cppname: str = f'{base_fn}.poly{Npp}.txt'
cfsname: str = f'{base_fn}.X{Npf}cfs.txt'

# output information
print(f'Saving the Polynomial files at: {cppname}')

# Save the polynomials to files
np.savetxt(cppname, Cpr)
np.savetxt(cfsname, Cfs)