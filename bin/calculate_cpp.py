#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 27 10:49:40 2020

Calculate the first polynomial fit to the full bandwidth
of spectra.
v0.7 Moved from a class to script 03.06.2023
v0.6 Merged with Tatiana's version 01.05.2021
v0.5 added dts and df in the Fdets header file 12.04.2021
v0.4 updated phase-referencing 24.03.2021
v0.3 improved chunks selection for phase-referencing 21.01.2020
v0.2 new declaration format
v0.1 original version from pre-repo
@author: gofrito
"""

import argparse
import configparser
import errno
import os
import re
import shutil
import sys

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import plotly.graph_objects as go
import scipy.signal as scs

from astropy.time import Time, TimeDelta
from colorama import Fore, Style
from pysctrack import core, handler, sctracker_ini
from time import process_time

mpl.rcParams['agg.path.chunksize'] = 10000

# take time
process_start: float = process_time()

# find the directory where pysctrack and catalogues are installed
PYSCTRACK_DIR: str = os.path.dirname(os.path.realpath(__file__))[:-3]
PYSCTRACK_CAT: str = os.path.join(PYSCTRACK_DIR, 'cats')

# List of parsed parameters
parser = argparse.ArgumentParser()
parser.add_argument("-o", "--output", \
                    help="Store spectra to ASCII file", action="store_true", default=False)
parser.add_argument("-p", "--plot", \
                    help="plot S/C signal", action="store_true", default=False)
parser.add_argument("-s", "--savefig", \
                    help="save S/C signal to PDF", action="store_true", default=False)
parser.add_argument("-bw", dest="bandwidth", \
                    help="channel bandwidth", type=float, default=32)
parser.add_argument("-db", dest="debug", \
                    action="store_true", default=False)
parser.add_argument("-dp", dest="dual_polarisation", \
                    help="add two linear polarisations together",
                    action="store_true", default=False)
parser.add_argument("-ep", dest="epoch", \
                    help="set epoch [yyyy.mm.dd]", type=str)
parser.add_argument("-it", dest="integration_time", \
                    help="define integration time < 1s", type=float)
parser.add_argument("-lf", dest="long_format", \
                    help="Output stack all spectra", action="store_true", default=False)
parser.add_argument("-ni", "--no_inifile", \
                    help="don't create inifile", action="store_true", default=False)
parser.add_argument("-of", dest='output_format', \
                    help='Output format of Fdets', default=1)
parser.add_argument("-p1", dest="polynomial", \
                    help="order of the first polynomial fit", type=int, default=6)
parser.add_argument("-pc", "--power_centre", \
                    help="do NOT estimate power centre", action="store_true", default=False)
parser.add_argument("-rf", dest="radio_frequency", \
                    help="initial sky freq of the channel", type=float)
parser.add_argument("-sc", dest="spacecraft", \
                    help="specify spacecraft", type=str)
parser.add_argument("-sn", dest="scan_number", \
                    help="Split output into multiple scans for sctracker", type=int)
parser.add_argument("-sf", "--sky_frequency", \
                    help="use sky frequency label", action="store_true", default=False)
parser.add_argument("-t0", dest="ini_time", \
                    help="initial time within the scan", type=int, default=0)
parser.add_argument("-t1", dest="end_time", \
                    help="end time within the scan", type=int, default=0)
parser.add_argument("-vx", "--vexade", \
                    help="Not using SNR-based weight", action="store_true", default=False)
parser.add_argument("-wc", "--wide_carrier", \
                    help="Increase RMS window for wide carriers", \
                    action="store_true", default=False)
parser.add_argument("-zf", dest="zoom_frequency", \
                    help="search boundaries in [MHz]", \
                    nargs="+", type=float, metavar="N", default=(0, 0))
parser.add_argument("filename", nargs="?", \
                    help="spectra input file", default="check_string_for_empty")
parser.add_argument("--version", \
                    action="version", version="%(prog)s 0.7")

args = parser.parse_args()

# Check the flags pre-selected in command line
plot: bool = args.plot
savefig: bool = args.savefig
textout: bool = args.output
vexade: bool = args.vexade
debug: bool = args.debug
sky_frequency: bool = args.sky_frequency
long_format: bool = args.long_format
wide_carrier: bool = args.wide_carrier

# By default we create the inifile, set -ni to skip
create_inifile: bool = True
if args.no_inifile:
    create_inifile = False

# By default we use the weighted power, set -pc to skip
powcen: bool = True
if args.power_centre:
    powcen = False

# Set observation-dependant parameters
ini_time: float = args.ini_time
end_time: float = args.end_time

npp: int = args.polynomial
npf: int = npp - 1

# Check if the file exists
if os.path.exists(args.filename):
    path, filename = os.path.split(args.filename)
    path: str = f"{os.path.abspath(path)}/"
else:
    raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), args.filename)

# Split the input filename
match = re.search(r"^([^_]*)_([^_]*)_(?:([^_]*)_)?(?:N)?([oabcdefgh])?(\d+)_(\d+)(?:pt)?_(\d+)s?_(?:ch)?([^_]+)_([^_]*)", \
                  filename, re.I)

# Extract all observing params from the filename
session: str = match.group(1)
station: str = match.group(2)
raw_format: str = match.group(3)
ifband: str = match.group(4)
scan_number: int = int(match.group(5))
fftpts: int = int(match.group(6))
dts: int = int(match.group(7))
channel: int = int(match.group(8))
software: str = match.group(9)[0:6]

# If Inifile is enable : Read the inifile.Xx
if create_inifile is True:
    swspec_ini = configparser.ConfigParser()

    # Single File case - One single inifile to process all scan
    if os.path.isfile(f"inifile.{station}"):
        swspec_ini.read(f"inifile.{station}")
        bandwidth: float = float(swspec_ini.get("Spectrometer", "BandwidthHz"))
    # Multiple file case - One inifile per scan recorded
    elif f"inifile.{station}{scan_number}":
        swspec_ini.read(f"inifile.{station}{scan_number}")
        bandwidth: float = float(swspec_ini.get("Spectrometer", "BandwidthHz"))
    # No inifile in the directory - input manually
else:
    bandwidth: float = args.bandwidth * 1e6

# In case we want to set the bandwidth manually
if args.bandwidth:
    bandwidth: float = args.bandwidth * 1e6

# Save the polynomial coefficients into a file
basefilen: str = np.size(filename) - 12  # 12 char: _swspec.bin

# Manually force the scan number output
if args.scan_number:
    scan_number = args.scan_number
    polys_name = f'{session}_{station}_{raw_format}' + \
                        f'_N{ifband}{scan_number:04d}_{fftpts}' + \
                        f'pt_{dts}s_ch{channel}'
else:
    polys_name = filename[0:basefilen]

# Read binary file to calculate rev 0 or 1
if software == 'swspec':
    SW_REV: int = 0
elif software == 'scspec':
    SW_REV: int = 1

# If integration time was 0 - select it manually
if args.integration_time:
    dts = args.integration_time

# Find the spacecraft observed
if args.spacecraft:
    spacecraft: str = args.spacecraft
else:
    spacecraft: str = handler.assign_spacecraft(session[0:-6])

# Unless we force the settings manually
if args.radio_frequency:
    baseband_freq: float = args.radio_frequency
else:
    stations = pd.read_json(os.path.join(PYSCTRACK_CAT, 'station.json'))
    try:
        baseband_freq = stations.loc[stations['code'] == station][session[:-6]].values[0]
    except:
        print(f"Station {station} and spacecraft {session[:-6]} combination do not exist in station.json, please update or else use the -rf argument instead")
        print("Default value will be used")
        baseband_freq = 8400

min_freq, max_freq = args.zoom_frequency

if max_freq == 0:
    scraft = pd.read_json(os.path.join(PYSCTRACK_CAT, 'sc.json'))
    try:
        transmission_freq = scraft.loc[scraft['code'] == session[:-6]]["down_link_x"].values[0] * 1e-6
        min_freq = transmission_freq - baseband_freq - 0.02 * bandwidth * 1e-6
        max_freq = transmission_freq - baseband_freq + 0.02 * bandwidth * 1e-6
        if min_freq < 0:
            min_freq = 0
        if max_freq > bandwidth:
            max_freq = bandwidth
    except:
        print(f"Spacecraft {spacecraft} downlink frequency does not exist in sc.json")
        print("Default values will be used")
        min_freq = 4.7
        max_freq = 4.9

# Convert to MHz
min_freq *= 1e6
max_freq *= 1e6

# Convert integers
min_freq = int(min_freq)
max_freq = int(max_freq)

# fill the epoch variable
if args.epoch:
    epoch: str = args.epoch
else:
    epoch: str = f"20{session[-6:-4]}.{session[-4:-2]}.{session[-2:]}"

# if t1 = 0 use the entire file, otherwise use end_time
if end_time:
    num_spectra = int(end_time / dts)
else:
    fsize: int = os.path.getsize(f"{path}{filename}")
    num_spectra: int = int(np.floor(0.5 * fsize / (fftpts + 1)))
    #end_time: float = num_spectra * dts

# 0 = Old format
# 1 = New format
# 2 = Extended format
output_format: int = int(args.output_format)

# initialise variables
sampling_rate: int = 2 * bandwidth
sampling_interval: np.double = 1 / sampling_rate
df: np.double = sampling_rate / fftpts

# Set the search window
bfs: int = int(min_freq / df)
bfm: int = int(max_freq / df)

# DFT products
fft_points: int = int(fftpts / 2 + 1)
jf: int = np.arange(start=0, stop = fft_points, dtype=int)
ff: np.double = df * jf

'''
Do all the calculations
Note 18.05.2021
The smoothing of the spectra is optional. Sometimes it helps to
improve the detection quality (sometimes does not)

The smoothing can be done by:
if 1:
    The plotting of the filtered spectrum is smooth
    vspec: float = read_data * np.hanning(fft_points)
if 2:
    The plotting shows the shoulders at the edges
    spectra[ip-ini_spectra]=read_data[bfs:bfm]*np.hanning(bfm-bfs)
'''
# Select the initial and end spectra
ini_spectra: int = round(ini_time / dts)
end_spectra: int = num_spectra
tsp: np.double = dts * (0.5 + np.arange(ini_spectra, end_spectra))
num_valid_spectra: int = end_spectra - ini_spectra

# If signal is too strong RMS is saturated.
# Increase half_window and line_avoidance x10 if the signal is too strong or wide.
if wide_carrier:
    half_window: int = int(1e4 / df)
    line_avoidance: int = int(1e3 / df)
else:
    half_window: int = int(1e3 / df)
    line_avoidance: int = int(1e2 / df)

# Initialize some vectors
spectra: float = np.zeros((num_valid_spectra, bfm - bfs))
ffs: float = ff[bfs : bfm]

# Create the monstruous array only if long_format is selected
if long_format:
    full_spectra: float = np.zeros((num_valid_spectra, fft_points))
all_spectra: float = np.zeros(fft_points)

# Open the file descriptor
with open(f"{path}{filename}", "rb") as fd:
    # Read the data and prepare the matrix
    for ip in range(num_spectra):
        one_spec: float = np.fromfile(file=fd, dtype="float32", count=fft_points)
        # Only store values that are above t0 and until end_time (end_time == num_spectra)
        if ip >= ini_spectra:
            #Do the Hanning in the full spectra or the chunk
            if True:
                #han_spec = one_spec * np.hanning(fft_points)
                #spectra[ip - ini_spectra] = core.smooth_hanning(one_spec[bfs : bfm], 1)
                han_spec = scs.savgol_filter(one_spec, window_length=3, polyorder=2, mode='interp')
                spectra[ip - ini_spectra] = han_spec[bfs : bfm]
            else:
                #spectra[ip - ini_spectra] = one_spec[bfs : bfm] * np.hanning(bfm - bfs)
                han_spec = scs.savgol_filter(one_spec[bfs : bfm], window_length=3, polyorder=2, mode='interp')
                spectra[ip - ini_spectra] = han_spec
            if long_format:
                full_spectra[ip-ini_spectra] += han_spec
            else:
                all_spectra += han_spec

if args.dual_polarisation:
    spectra_y: float = np.zeros((num_valid_spectra, bfm - bfs))

    if filename.find('Nc') > 0:
        filename_y = filename.replace('Nc', 'Nd')
    elif filename.find('Nd') > 0:
        filename_y = filename.replace('Nd', 'Nc')
    elif filename.find('Ne') > 0:
        filename_y = filename.replace('Ne', 'Nf')
    elif filename.find('Nf') > 0:
        filename_y = filename.replace('Nf', 'Ne')

    with open(f"{path}{filename_y}", "rb") as fd:
        # Read the data and prepare the matrix
        for ip in range(num_spectra):
            one_spec: float = np.fromfile(file=fd, dtype="float32", count=fft_points)
            if ip >= ini_spectra:
                han_spec = scs.savgol_filter(one_spec, window_length=3, polyorder=2, mode='interp')
                spectra_y[ip - ini_spectra] = han_spec[bfs : bfm]
                all_spectra += han_spec

    spectra += spectra_y

# Note: 02.03.2021
# For phase referencing we need to calculate the SNR from the section with S/C tone
# and dont use the section without signals. Now SNR is calculated from 0 to end_time,
# instead we need to do from t0-t1
# We use though spectra that laready contains ini_spectra> so num_spectra is changed

aver_spectra = np.sum(spectra, axis=0) / num_valid_spectra
max_spectra: float = spectra.max()
spectra: float = spectra / max_spectra

# Adding the full bandwidth stack of spectrum
if long_format:
    full_spectra /= full_spectra.mean()
    print(f'\n{Fore.LIGHTRED_EX}DEGUG-Total size ' + \
          f'{int(sys.getsizeof(full_spectra)/1024**2)} MB{Style.RESET_ALL}')
else:
    # all_spectra is the spectra version of the full averaged bandwidth of data
    all_spectra /= num_valid_spectra
    all_spectra /= all_spectra.mean()

# Just copy the last one to last_spectra for plotting purposes and normalise
last_spectra: float = one_spec

if debug:
    print(f'\n{Fore.LIGHTRED_EX}DEGUG-noise floor: ' + \
          f'{last_spectra.mean() / fft_points}{Style.RESET_ALL}')

# normalise the last spectra for plotting purposes
#last_spectra /= last_spectra.mean()

# If text output is enabled dump spectra to a text file
# Make sure the window is small otherwise the file will be large
if textout:
    output_fn: str = f'Spec.{spacecraft}.{epoch}.{station}.{scan_number:04d}.if{ifband}.txt'
    spec_header: str = f'Observation conducted on {epoch} at {station}\n\n\n'
    out_spectra = np.transpose([ffs, aver_spectra])
    np.savetxt(output_fn, out_spectra, newline="\n", header=spec_header)

# Initialize vectors for determining the Doppler detections
xfc: float = np.zeros((num_valid_spectra, 3))
rms: float = np.zeros((num_valid_spectra, 3))
Smax: float = np.zeros(num_valid_spectra)
SNR: float = np.zeros(num_valid_spectra)
freq_dets: float = np.zeros(num_valid_spectra)
dxc: float = np.zeros(num_valid_spectra)
MJD: int = np.zeros(num_valid_spectra)

zbins: int = bfm - bfs

# Seeking for the Max and estimating the RMS
for ip in range(num_valid_spectra):
    xfc[ip] = core.FindMax(spectra[ip], ffs)
    Smax[ip] = xfc[ip, 2]
    freq_dets[ip] = df * xfc[ip, 1] + ffs[0]
    rms[ip] = core.GetRMS(spectra[ip], xfc[ip, 1], half_window, line_avoidance)
    SNR[ip] = (xfc[ip, 2] - rms[ip, 0]) / rms[ip, 1]

    # Calculate the centre power of the signal with 1, 3, 5 or 7 bins
    if powcen:
        if (xfc[ip, 0]) == 1 or (xfc[ip, 0] == zbins - 1):
            dxc[ip] = core.PowCenter(spectra[ip], xfc[ip, 1], 1)
        elif (xfc[ip, 0]) == 2 or (xfc[ip, 0] == zbins - 2):
            dxc[ip] = core.PowCenter(spectra[ip], xfc[ip, 1], 2)
        else:
            dxc[ip] = core.PowCenter(spectra[ip], xfc[ip, 1], 3)

if debug:
    print(f'{Fore.LIGHTRED_EX}DEGUG-noise floor RMS: {rms.mean()}{Style.RESET_ALL}')

# Storing the initial bin for latter
ibin: int = xfc[0, 1]

# Adding the correction to our Frequency detections
mean_snr = SNR.mean()
dxc *= df

# if power centre True add PowCenter correction
if powcen:
    freq_dets_c = freq_dets + dxc
else:
    freq_dets_c = freq_dets

# If -vx selected use weight of 1s
# By default use weight-mask based on the SNR
if vexade:
    mean_fdet: float = freq_dets_c.mean()
    freq_dets_c -= mean_fdet
    weight: float = np.ones(num_valid_spectra)
else:
    weight: float = np.power(SNR, 2) / mean_snr

freq_dets: float = freq_dets_c

# Initialize the Polynomial coefficients
'''
cf:  Frequency polynomials
cfs: Frequency polynomials per second
cps: Phase polynomials per second
cpp: Phase polynomials per radian
'''
cfs: float = np.zeros(npp)
cps: float = np.zeros(npp + 1)
cpp: float = np.zeros(npp + 1)

# Take into account if ini time different than 0
tsp -= ini_time
Tspan: float = tsp.max()

# Calculate polys and fit
cf: float = core.PolyfitW1C(tsp, freq_dets_c, weight, npf)
freq_fit: float = core.PolyfitW1(tsp, freq_dets_c, weight, npf)

# Estimate the residual frequency fit
residual_fit = freq_dets_c - freq_fit

# Correct Freq detections if vexade mode is set
if vexade:
    cf[0] += mean_fdet
    freq_fit += mean_fdet
    freq_dets_c += mean_fdet

# Convert to frequency polynomials per second
for jpf in range(npp):
    cfs[jpf] = cf[jpf] * np.power(Tspan, -jpf)

# Calculate the phase polynomials coefficients
for jpf in range(1, npp + 1):
    cps[jpf] = 2 * np.pi * cfs[jpf - 1] / jpf
    cpp[jpf] = cps[jpf] * np.power(sampling_interval, jpf)

cppname: str = f"{path}{polys_name}.poly{npp}.txt"
cfsname: str = f"{path}{polys_name}.X{npf}cfs.txt"

# Save the polynomials to files
np.savetxt(cppname, cpp)
np.savetxt(cfsname, cfs)

# Parsing starting time of the scan
swspec_time_file: str = f"{path}{filename[0:basefilen]}_starttiming.txt"

# Parsing runlog file name
swspec_runlog_file: str = swspec_time_file.replace('starttiming', 'runlog')

start_time: float = 0

# Find out if it contains time stamp or not in the second line
with open(swspec_time_file) as f:
   count = sum(1 for _ in f)

if count == 2:
    # If 2 lines - standard VLBI data format
    swspec_time_info = np.loadtxt(swspec_time_file, skiprows=1)
    MJD[:] = int(swspec_time_info[0])
    start_time = swspec_time_info[1]
else:
    # If 1 line - presume that is Ettus, otherwise it might give an error
    os.system(f'parse_sdr_timestamp.py {swspec_runlog_file}')

# Save the frequency detections revision 0 into a file
tsp += ini_time
ttsp: float = tsp + start_time

# Format False = old with six columns including MJD and second of the day since 00:00:00
# Format True = new with five columns including full epoch

fdets_fn: str = f"{path}Fdets.{spacecraft}{epoch}.{station}.{scan_number:04d}.r{SW_REV}i.txt"

if output_format == 0:
    fdets_header: str = (
        f'Observation conducted on {epoch} at {station} rev. {SW_REV}'
        f'\nBase frequency: {baseband_freq:.2f} MHz dF: {df} Hz dT:'
        f' {dts} s Nscans: 1\nFormat: MJD |'
        f' Time [s] | Signal-to-Noise |  Spectral max  '
        f'| Freq detection [Hz] | Doppler noise [Hz]  |\n'
    )

    data = {'mjd': MJD,
            'timestamp': ttsp,
            'SNR': SNR,
            'Spectral Max': Smax,
            'Doppler': freq_dets_c,
            'Residual fit': residual_fit}

    np.savetxt(fdets_fn, pd.DataFrame(data=data),delimiter=' ', \
                fmt='%i %.6f %.18f %.18f %.12f %.16e', header=fdets_header)
elif output_format == 1:
    epochs = Time(MJD[0], format='mjd', scale='utc')
    seconds = TimeDelta(ttsp, format='sec')

    time = epochs + seconds

    fdets_header: str = (
        f'Observation conducted on {epoch} at {station} rev. {SW_REV}'
        f'\nBase frequency: {baseband_freq:.2f} MHz dF: {df} Hz dT:'
        f' {dts} s Nscans: 1\nFormat: UTC Time     '
        f'|   Signal-to-Noise  |    Spectral max    '
        f'| Freq detection [Hz] |   Doppler noise [Hz]  |\n'
    )

    data = {'timestamp': time.isot,
            'SNR': SNR,
            'Spectral Max': Smax,
            'Doppler': freq_dets_c,
            'Residual fit': residual_fit}

    np.savetxt(fdets_fn, pd.DataFrame(data=data), delimiter=' ', \
               fmt='%15s %.18e %.18e %21.12f %+.16e', header=fdets_header)
               #fmt='%15s %22.18f %.18f %21.12f %+.16e', header=fdets_header)

    # Copy the template of fdets info to the working directory
    if os.path.exists(f'{PYSCTRACK_CAT}/fdets_info.r0i.md'):
        shutil.copy(f'{PYSCTRACK_CAT}/fdets_info.r0i.md', 'fdets_info.r0i.md')
elif output_format == 2:
    epochs = Time(MJD[0], format='mjd', scale='utc')
    seconds = TimeDelta(ttsp, format='sec')

    time = epochs + seconds
    index = np.arange(start=1, stop=len(time) + 1)

    data = {'index': index,
            'timestamp': time.isot,
            #'SNR': SNR,
            #'Spectral Max': Smax,
            'Doppler': freq_dets_c + baseband_freq * 1e6}
            #'Residual fit': residual_fit}

    np.savetxt(fdets_fn, pd.DataFrame(data=data), delimiter=" ", fmt='%05d %15s %9.6f')

    print('to be implemented')
else:
    print('wrong format')

sky_freq: float = baseband_freq + freq_dets_c[0] * 1e-6

# Print out the stats
print('')
print(f'Input file            : {filename}')
print(f'Polys coefficients    : {os.path.split(cppname)[1]}')
print(f'Freq detections       : {os.path.split(fdets_fn)[1]}')
print(f'{Fore.BLUE}')
print(f"- S/C sky frequency   : {sky_freq:.2f} MHz ({sky_freq * 1e6:.3f}) Hz")
print(f"- S/C band frequency  : {freq_dets_c[0] / 1e6:.3f} MHz ({freq_dets_c[0]:.3f}) Hz")
print(f"- S/C initial bin     : {ibin:.2f} ")
print(f"- Total nr of spectra : {num_spectra} ")
print(f"- Nr of spectra used  : {num_valid_spectra} ")
print(f"- Average SNR         : {mean_snr:.2f} ")
print(f"- Doppler noise       : {residual_fit.std():.3f} Hz")
print(f"- Doppler shift       : {np.abs(freq_dets_c[0] - freq_dets_c[-1]):.2f} Hz")
print(f'{Style.RESET_ALL}')

# Create the inifile
if create_inifile:
    # Create an inifile classe
    inifile = sctracker_ini.SctrackerINI(f"{path}{filename}", ini_time, end_time, scan_number, npp)

    # Write out the inifile
    inifile.write_ini()

if sky_frequency:
    ff += baseband_freq * 1e6
    freq_dets += baseband_freq * 1e6
    freq_fit += baseband_freq * 1e6

# Convert to MHz
ff /= 1e6
freq_dets /= 1e6
freq_fit /= 1e6
if plot:

    print("Checking the output spectra from swspectrometer")
    print("Select the figure that you would like to display")
    print("1- Last spectrum from the scan - full bandwdith")
    print("2- Averaged time-integrated spectra - zoom window")
    print("3- Frequency detections of the S/C signal")
    print("4- Summary of the spectra and signal detected")
    print("5- Dynamic spectra")
    # print(f"6- 3D dynamic spectra")
    graph = input("Make a choice: 1-2-3-4-5\n")

    plt.figure(graph)

    if graph == "1":
        plt.semilogy(ff, last_spectra, color="deepskyblue", label="spectral power")
        plt.ylabel("Spectral power")
        plt.xlabel("Frequency [MHz]")
        plt.grid('on')
        plt.title(f"Last spectrum from the session {session} at {station}")
        plt.show()

    elif graph == "2":
        plt.semilogy(ff[bfs : bfm], aver_spectra, color="deepskyblue", label="spectral power")
        plt.ylabel("Spectral power")
        plt.xlabel("Frequency [MHz]")
        plt.title(f"Averaged time-integrated spectra {session} at {station}")
        plt.show()

    elif graph == "3":
        ax1 = plt.subplot2grid((3, 1), (0, 0))
        plt.title(f"Summary I of the Frequency detections {session} at {station}")
        ax1.plot(tsp, SNR, color="indigo", marker="x", linestyle="none")
        ax1.set_ylabel("SNR")
        ax2 = plt.subplot2grid((3, 1), (1, 0))
        ax2.plot(tsp, freq_dets, color="mediumseagreen", marker="o", linestyle="none")
        ax2.plot(tsp, freq_fit, color='red', marker='.', linestyle='none')
        ax2.set_ylabel("Freq detections [MHz]")
        ax3 = plt.subplot2grid((3, 1), (2, 0))
        ax3.plot(tsp, residual_fit, color="orangered", marker="x", linestyle="none")
        ax3.set_ylabel("Dnoise [Hz]")
        ax3.set_xlabel("Time [s]")
        plt.tight_layout()
        plt.show()

    elif graph == "4":
        ax1 = plt.subplot2grid((2, 2), (0, 0), colspan=2)
        plt.title(f"Summary II of the Frequency detections {session} at {station}")
        ax1.semilogy(ff, last_spectra, color="blue", label="spectral power")
        ax1.set_xlabel("Frequency [MHz]")
        ax1.set_ylabel("Spectral power")
        ax2 = plt.subplot2grid((2, 2), (1, 0))
        ax2.semilogy(ff[bfs : bfm], aver_spectra, color="blue")
        ax2.set_ylabel("Spectra power")
        ax2.set_xlabel("Frequency [MHz]")
        ax3 = plt.subplot2grid((2, 2), (1, 1))
        ax3.plot(tsp, freq_dets, color="mediumseagreen", marker="x", linestyle="none")
        ax3.set_ylabel("Freq detections [MHz]")
        ax3.set_xlabel("Time [s]")
        plt.tight_layout()
        plt.show()

    elif graph == "5":
        dynspec = plt.pcolormesh(tsp, ffs, np.transpose(10 * np.log10(spectra)), \
                                 cmap='magma', shading='auto')
        plt.colorbar(dynspec)
        plt.ylabel('Frequency [MHz]')
        plt.xlabel('Time [s]')
        plt.title(f'Dynamic spectrum {session} at {station}')
        plt.show()

    #elif graph == "6":
        #ax = plt.axes(projection="3d")
        #x = np.zeros((num_valid_spectra, bfm - bfs))
        #y = np.zeros((num_valid_spectra, bfm - bfs))
        #z = np.zeros((num_valid_spectra, len(x)))
        #for ip in range(num_valid_spectra):
        #    x[ip] = ffs
        #    y[ip] = spectra[ip,:]
        #    z[ip] = np.ones(len(x)) * ip
        #ax.plot3D(x, y, np.log10(z), cmap='hsv')
        #dynspec = ax.pcolormesh(tsp, ffs, np.transpose(10 * np.log10(spectra)), \
        # cmap='seismic', shading='auto')
        #plt.plot(ffs, tsp, np.log10(spectra), cmap='hsv')
        #plt.colorbar(dynspec)
        #ax.set_xlabel('Frequency [MHz]')
        #ax.set_ylabel('Time [s]')
        #plt.title('3D dynaminc spectra')
        #ax.set_xlim(bfs, bfm)
        #plt.show()

if savefig:
    fig = go.Figure()

    print("Checking the output spectra from swspectrometer")
    print("Select the figure that you would like to display")
    print("1- Last spectrum from the scan - full bandwdith")
    print("2- Averaged time-integrated spectra - zoom window")
    #print("3- Frequency detections of the S/C signal")
    #print("4- Summary of the spectra and signal detected")
    print("5- Dynamic spectra")
    graph = input("Make a choice: 1-2-5\n")

    if graph == "1":
        fig.add_traces(go.Scatter(mode='lines', x=ff, y=10 * np.log10(last_spectra)))

        # Add figure title
        fig.update_layout(
            title_text=f"Normalised power spectra from {spacecraft} on {epoch} " + \
                        f"at {station}<br><sup>Frequency of transmission was</sup>",
            font_family="lato",
            font_color="grey",
            title_font_size=18,
            title_font_color="blue",
            font_size=15,
            width=1200,
            height=600,
        )
        # Set y-axes titles
        fig.update_xaxes(title="<b>Frequency band [MHz] </b>")
        fig.update_yaxes(title='<b>Spectral power [dB]</b>')

        # Save to disk the image
        fig.write_image(f'{session}_{spacecraft}_{station}_Spectra.pdf')

    elif graph == "2":
        fig.add_traces(go.Scatter(mode='lines', x=ff, y=10 * np.log10(aver_spectra)))

        # Add figure title
        fig.update_layout(
            title_text=f"Zoom power spectra from {spacecraft} on {epoch} " + \
                        f"at {station}<br><sup>Frequency of transmission was</sup>",
            font_family="lato",
            font_color="grey",
            title_font_size=18,
            title_font_color="blue",
            font_size=15,
            width=1200,
            height=600,
        )
        # Set y-axes titles
        fig.update_xaxes(title="<b>Frequency band [MHz] </b>")
        fig.update_yaxes(title='<b>Spectral power [dB]</b>')

        # Save to disk the image
        fig.write_image(f'{session}_{spacecraft}_{station}_zoomSpectra.pdf')

    elif graph == "5":
        fig.add_traces(go.Heatmap(x=tsp, y=ffs, z=np.transpose(10 * np.log10(spectra)), \
                                  colorscale = 'Electric'))

        # Add figure title
        fig.update_layout(
            title_text=f"Zoom power spectra from {spacecraft} on {epoch} at " + \
                       f"{station}<br><sup>Frequency of transmission was</sup>",
            font_family="lato",
            font_color="grey",
            title_font_size=18,
            title_font_color="blue",
            font_size=15,
            width=1200,
            height=600,
        )
        # Set y-axes titles
        fig.update_xaxes(title="<b>Time [s]</b>")
        fig.update_yaxes(title='<b>Frequency band [MHz]</b>')

        # Save to disk the image
        fig.write_image(f'{session}_{spacecraft}_{station}_waterfall.pdf')

print(f'{Fore.GREEN}* DEBUG-Total time    : ' + \
      f'{process_time() - process_start:.2} s{Style.RESET_ALL}\n')
