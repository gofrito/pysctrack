#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Sat My 14 07:49:56 2020

CheckSpectra.py plots the spectra of a VLBI observation
Settings are  placed by default for new VGOS system: 32e6 Hz bandwidth
@author: gofrito
"""

import argparse
import errno
import os
import re

import matplotlib.pyplot as plt
import numpy as np
import plotly.graph_objects as go

from pysctrack import core, handler

class plot_spectra:
    def __init__(self):
        parser = argparse.ArgumentParser()
        parser.add_argument("-f", "--simpolyfit", \
                            help="simulate a polynomial fit", action="store_true", default=False)
        parser.add_argument("-o", "--output", \
                            help="Store spectra to ASCII file", action="store_true", default=False)
        parser.add_argument("-p", "--plot", \
                            help="plot S/C signal", action="store_true", default=False)
        parser.add_argument("-s", "--savefig", \
                            help="save S/C signal to PDF", action="store_true", default=False)
        parser.add_argument("-sf", "--skyfrequency", \
                            help="change  labels to show results in sky frequency", action="store_true", default=False)
        parser.add_argument("-bw", dest="bandwidth", \
                            help="observing bandwidth", type=float, default=32)
        parser.add_argument("-ep", dest="epoch", \
                            help="specify your own epoch - format ex. 2020.02.02", type=str)
        parser.add_argument("-it", dest="integration_time", \
                            help="define integration when is a fraction of 1", type=float)
        parser.add_argument("-rf", dest="radio_frequency", \
                            help="initial frequency band of the channel", type=float, default=8400)
        parser.add_argument("-sc", dest="spacecraft", \
                            help="specify spacecraft ortherwise use filename", type=str)
        parser.add_argument("-t0", dest="initialtime", \
                            help="initial time within the scan", type=int, default=0)
        parser.add_argument("-t1", dest="endtime", \
                            help="end time within the scan", type=int, default=0)

        parser.add_argument("-zf", dest="zoom_frequency", \
                            help="enter the boundaries of the freq search window [MHz]", \
                            nargs="+", type=float, metavar="N", default=(4.7, 4.9))
        parser.add_argument("filename", nargs="?", \
                            help="spectra input file", default="check_string_for_empty")
        parser.add_argument("--version", \
                            action="version", version="%(prog)s 0.3")
        args = parser.parse_args()

        self.filename: str = args.filename
        self.plot: bool = args.plot
        self.textout: bool = args.output
        self.simpolyfit: bool = args.simpolyfit
        self.savefig: bool = args.savefig

        self.t0: int = args.initialtime
        self.t1: int = args.endtime

        # Check if the file exists
        if os.path.exists(args.filename):
            path, self.filename = os.path.split(args.filename)
            self.path = f"{os.path.abspath(path)}/"
        else:
            raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), self.filename)

        match = re.search(r"^([^_]*)_([^_]*)_(?:([^_]*)_)?(?:N)?([oabcdefgh])?(\d+)_(\d+)(?:pt)?_(\d+)s?_(?:ch)?([^_]+)_(?:swspec|scspec)", self.filename, re.I)

        # Extract all observing params from the filename
        self.session: str = match.group(1)
        self.station: str = match.group(2)
        self.raw_format: str = match.group(3)
        self.ifband: str = match.group(4)
        self.scan_number: int = int(match.group(5))
        self.FFTpt: int = int(match.group(6))
        self.dts: int = int(match.group(7))
        self.channel: str = match.group(8)

        # If integration time was 0 - select it manually
        if args.integration_time:
            self.dts = args.integration_time

        self.Fmin, self.Fmax = args.zoom_frequency
        self.Fmin *= 1e6
        self.Fmax *= 1e6
        self.BW = args.bandwidth * 1e6

        if args.radio_frequency:
            self.Fstart: float = args.radio_frequency

        if self.Fmin < 0 or self.Fmax > self.BW:
            print(f"Zoom Frequency must be within the bandwidth")
            raise ValueError(errno.EINVAL, os.strerror(errno.EINVAL), self.BW)

        # if t1 = 0 use the entire file, otherwise use t1
        if self.t1:
            self.Nspec = int(self.t1 / self.dts)
        else:
            fsize: int = os.path.getsize(f"{self.path}{self.filename}")
            self.Nspec: int = int(np.floor(0.5 * fsize / (self.FFTpt + 1)))
            self.t1: float = self.Nspec * self.dts

        if args.spacecraft:
            self.spacecraft: str = args.spacecraft
        else:
            self.spacecraft: str = handler.assign_spacecraft(self.session[0])

        # fill the epoch variable
        if args.epoch:
            self.epoch: str = args.epoch
        else:
            self.epoch: str = f"20{self.session[1:3]}.{self.session[3:5]}.{self.session[5:7]}"

    def data_process(self):
        SR: int = 2 * self.BW
        df: float = SR / self.FFTpt
        Nfft: int = int(self.FFTpt / 2 + 1)
        jf = np.arange(Nfft, dtype=int)
        self.ff: float = df * jf

        s0: int = int(np.round(self.t0 / self.dts))
        s1: int = self.Nspec
        tsp: float = self.dts * (0.5 + np.arange(s0, s1))

        # Read file
        fd = open(f"{self.path}{self.filename}", "rb")

        self.bfs: int = int(self.Fmin / df)
        self.bfm: int = int(self.Fmax / df)

        # Initialize some vectors
        Sps = np.zeros((self.Nspec, self.bfm - self.bfs))
        self.Aspec = np.zeros(self.bfm - self.bfs)
        self.ffs = self.ff[self.bfs : self.bfm]

        # Read the data and prepare the matrix
        for ip in np.arange(self.Nspec):
            read_data = np.fromfile(file=fd, dtype="float32", count=Nfft)
            vspec = read_data[self.bfs : self.bfm]
            # vspec     = read_data*np.hanning(Nfft)
            Sps[ip] = vspec

        # close the opened file
        fd.close()

        self.Aspec = np.sum(Sps, axis=0) / self.Nspec
        mSp: float = Sps.max()
        Sps = Sps / mSp
        self.lSpec = read_data

        # If text output is enabled dump Sps to a text file
        # Make sure the window is small otherwise the file can be large
        if self.textout == True:
            output_fn = f'Spec.{self.spacecraft}.{self.epoch}.{self.station}.{self.scan_number:04d}.if{self.ifband}.txt'
            spec_header = f'Observation conducted on {self.epoch} at {self.station}\n\n\n'

            spectra = np.transpose([self.ffs, self.Aspec])
            np.savetxt(output_fn, spectra, newline="\n", header=spec_header)

        # Spectra file opened and copied to an array
        xfc = np.zeros((self.Nspec, 3))
        Smax = np.zeros(self.Nspec)
        self.Fdet = np.zeros(self.Nspec)
        RMS = np.zeros((self.Nspec, 3))
        SNR = np.zeros(self.Nspec)

        # Seeking for the Max and estimating the RMS
        for ip in np.arange(self.Nspec):
            xfc[ip] = core.FindMax(Sps[ip], self.ffs)
            Smax[ip] = xfc[ip, 2]
            self.Fdet[ip] = df * xfc[ip, 1] + self.ffs[0]

        if self.simpolyfit:
            Hwin = int(1e3 / df)
            Avoid = int(100 / df)
            # Take into account if ini time different than 0
            tsp = tsp - self.t0
            for ip in np.arange(self.Nspec):
                RMS[ip] = core.GetRMS(Sps[ip], xfc[ip, 1], Hwin, Avoid)
                SNR[ip] = (xfc[ip, 2] - RMS[ip, 0]) / RMS[ip, 1]
            weight = np.power(SNR, 2) / np.mean(SNR)

            # Calculate polys and fit
            self.Ffit = core.PolyfitW1(tsp, self.Fdet, weight, 6)

            self.rFit = self.Fdet - self.Ffit
            print(f"Goodness of the polynomial fit: {np.std(self.rFit)}")

        self.SNR = SNR
        self.tsp = tsp
        self.Sps = Sps

    def create_plot(self):
        '''
        Display a plot in screen or store to a file in pdf format
        '''
        if self.plot:
            print("Creating the Figure for plotting the spectra")
            print("Select the figure that you would like to display")
            print("1- Last spectrum read from the file - full bandwdith")
            print("2- Averaged time-integrated spectra - Span zoomed")
            print("3- Frequency detections of the S/C signal")
            print("4- Summary plot with scan, zoom and Fdet")
            print("5- Waterfall")
            graph = input("Make a choice: 1-2-3-4-5\n")

            plt.figure()
            self.tsp += self.t0

            if graph == "1":
                plt.semilogy(self.ff, self.lSpec, color="blue", label="spectral power")
                plt.ylabel("Spectral power")
                plt.xlabel("Frequency [MHz]")
                plt.title(f"Last spectrum from {self.session} at {self.station}")
                plt.show()

            elif graph == "2":
                plt.semilogy(self.ff[self.bfs : self.bfm], self.Aspec, color="blue", label="spectral power")
                plt.ylabel("Spectral power")
                plt.xlabel("Frequency [MHz]")
                plt.title(f"Averaged time-integrated spectra from {self.session} at {self.station}")
                plt.show()

            elif graph == "3":
                if self.simpolyfit:
                    ax1 = plt.subplot2grid((3, 1), (0, 0))
                    plt.title(f"Summary I of the Frequency detections {self.session} at {self.station}")
                    ax1.plot(self.tsp, self.SNR, color="indigo", marker="x", linestyle="none")
                    ax1.set_ylabel("SNR")
                    ax2 = plt.subplot2grid((3, 1), (1, 0))
                    ax2.plot(self.tsp, self.Fdet, color="mediumseagreen", marker="o", linestyle="none")
                    ax2.plot(self.tsp, self.Ffit, color='red', marker='.', linestyle='none')
                    ax2.set_ylabel("Fdets [MHz]")
                    ax3 = plt.subplot2grid((3, 1), (2, 0))
                    ax3.plot(self.tsp, self.rFit, color="orangered", marker="x", linestyle="none")
                    ax3.set_ylabel("Dnoise [Hz]")
                    ax3.set_xlabel("Time [s]")
                    plt.tight_layout()
                    plt.show()
                else:
                    ax2 = plt.subplot2grid((1, 1), (0, 0))
                    plt.title(f"Summary I of the Frequency detections {self.session} at {self.station}")
                    ax2.plot(self.tsp, self.Fdet, color="mediumseagreen", marker="o", linestyle="none")
                    ax2.set_ylabel("Fdets [MHz]")
                    ax2.set_xlabel("Time [s]")
                    plt.tight_layout()
                    plt.show()

            elif graph == "4":
                ax1 = plt.subplot2grid((2, 2), (0, 0), colspan=2)
                plt.title(f"Summary II of the Frequency detections {self.session} at {self.station}")
                ax1.semilogy(self.ff, self.lSpec, color="blue", label="spectral power")
                ax1.set_xlabel("Frequency [MHz]")
                ax1.set_ylabel("Spectral power")
                ax2 = plt.subplot2grid((2, 2), (1, 0))
                ax2.semilogy(self.ff[self.bfs : self.bfm], self.Aspec, color="blue")
                ax2.set_ylabel("Spectra power")
                ax2.set_xlabel("Frequency [MHz]")
                ax3 = plt.subplot2grid((2, 2), (1, 1))
                ax3.plot(self.tsp + self.t0, self.Fdet, color="mediumseagreen", marker="x", linestyle="none")
                ax3.set_ylabel("Fdets [MHz]")
                ax3.set_xlabel("Time [s]")
                plt.tight_layout()
                plt.show()

            elif graph == "5":
                dynspec = plt.pcolormesh(self.tsp, self.ffs, np.transpose(10 * np.log10(self.Sps)), cmap='magma', shading='auto')
                plt.colorbar(dynspec)
                plt.ylabel(f'Frequency [MHz]')
                plt.xlabel(f'Time [s]')
                plt.title(f'Dynamic spectrum {self.session} at {self.station}')
                plt.show()
            return graph

        if self.savefig:
            print("Creating the Figure for plotting the spectra")
            print("Select the figure that you would like to display")
            print("1- Last spectrum read from the file - full bandwdith")
            print("2- Averaged time-integrated spectra - Span zoomed")
            print("3- Frequency detections of the S/C signal")
            print("4- Summary plot with scan, zoom and Fdet")
            print("5- Waterfall")
            graph = input("Make a choice: 1-2-3-4-5\n")

            if graph == "1":
                fig = go.Figure()

                fig.add_traces(go.Scatter(mode='lines', x=self.ff, y=10 * np.log10(self.lSpec)))

                # Add figure title
                fig.update_layout(
                    title_text=f"Normalised power spectra from {self.spacecraft} on {self.epoch} at {self.station}<br><sup>Frequency of transmission was</sup>",
                    font_family="lato",
                    font_color="grey",
                    title_font_size=18,
                    title_font_color="blue",
                    font_size=15,
                    width=1200,
                    height=600,
                )
                # Set y-axes titles
                fig.update_xaxes(title_text="<b>Frequency band [MHz] </b>")
                fig.update_yaxes(title='<b>Spectral power [dB]</b>')

                # Save to disk the image
                fig.write_image(f'{self.session}_{self.spacecraft}_{self.station}_Spectra.pdf')

            elif graph == "2":
                fig = go.Figure()

                fig.add_traces(go.Scatter(mode='lines', x=self.ff, y=10 * np.log10(self.Aspec)))

                # Add figure title
                fig.update_layout(
                    title_text=f"Zoom power spectra from {self.spacecraft} on {self.epoch} at {self.station}<br><sup>Frequency of transmission was</sup>",
                    font_family="lato",
                    font_color="grey",
                    title_font_size=18,
                    title_font_color="blue",
                    font_size=15,
                    width=1200,
                    height=600,
                )
                # Set y-axes titles
                fig.update_xaxes(title_text="<b>Frequency band [MHz] </b>")
                fig.update_yaxes(title='<b>Spectral power [dB]</b>')

                # Save to disk the image
                fig.write_image(f'{self.session}_{self.spacecraft}_{self.station}_zoomSpectra.pdf')

show_plots = plot_spectra()
show_plots.data_process()
show_plots.create_plot()
