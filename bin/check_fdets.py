#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Fri May  15 13:21:04 2020

@author: gofrito
"""

import argparse
import re

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns

from colorama import Fore, Style

#sns.set_theme(style="darkgrid")
parser = argparse.ArgumentParser()

parser.add_argument("filename", nargs="?", help="Input file with Doppler detections")
parser.add_argument("-p", "--plot", help="Plot results", action="store_true", default=False)
parser.add_argument("-s", "--statistics", help="Show stats", action="store_true", default=False)

def extract_freqbase(fdets_fn):
    '''
    Extract the base frequency from the Fdets file
    '''
    # read the input file
    with open(fdets_fn, 'r', encoding="utf8") as fd:
        # discard the first line
        fd.readline()

        # Second line contains the base frequency
        freq_line = fd.readline()
        frequency_base: float = float(freq_line.split(' ')[3])

        fdets_format = len(freq_line.split(' '))
        num_scans = 0

        if fdets_format > 12:
            # This is the new fdets data format
            # Base frequency: 8419.50 MHz dF: 5.0 Hz dT: 2 s Nscans: 1
            scans = freq_line[:-1].split(' ')[-1]
            num_scans = int(scans)
        elif fdets_format == 11:
            # This is the old fdets data format without the number of scans
            num_scans: int = 1

    return frequency_base, num_scans

args = parser.parse_args()

filename: str = args.filename
plot: bool = args.plot
statistics: bool = args.statistics

# Just display something if we forgot to plot
if plot is False:
    statistics = True

with open(filename, 'r', encoding="utf8") as my_file:
    content = my_file.readlines()
    data_format = content[4].count(' ')
    column_header = content[2].split(' ')[2]

# Single or multiple scan(s) per file
if filename.count(".") == 7:
    MULT_SCANS = False
elif filename.count(".") == 6:
    MULT_SCANS = True

if column_header in ('Modified', 'Time(UTC)'):
    fdets = pd.read_csv(filename, skiprows=4, sep=r'\s+', \
                        names=['mjd', 'time', 'snr', 'spectra', 'doppler', 'residuals'])
elif column_header in ('UTC', 'Time(UTC)'):
    fdets = pd.read_csv(filename, skiprows=4, sep=r'\s+', \
                        names=['time', 'snr', 'spectra', 'doppler', 'residuals'])
    fdets['time'] = pd.to_datetime(fdets['time'], format='%Y-%m-%dT%H:%M:%S.%f')
elif column_header == 'scan':
    fdets = pd.read_csv(filename, skiprows=4, sep=r'\s+', \
                        names=['index', 'time', 'snr', 'spectra', 'doppler', 'residuals'])
    fdets['time'] = pd.to_datetime(fdets['time'], format='%Y-%m-%dT%H:%M:%S.%f')

# Extract the initial frequency baseband
freq_base, n_scans = extract_freqbase(filename)

# Extract all metadata from the filename
match = re.search(r"^([^.]*).([^.]*).([^.]*).([^.]*).([^.]*).([^.]*).([^.]*).", filename, re.I)

file: str = match.group(1)
spacecraft: str = match.group(2)[0:3]
yy: str = match.group(2)[5:7]
mm: str = match.group(3)
dd: str = match.group(4)
station: str = match.group(5)
scan: str = match.group(6)
rev = match.group(7)

if plot:
    plt.figure(figsize=(8, 9), dpi=80)
    plt.suptitle(f"Summary I of the Frequency detections {dd}.{mm}.{yy} at {station}")
    plt.subplot(3, 1, 1)
    sns.scatterplot(data=fdets, x='time', y='snr', color="indigo", marker='.', s=100)
    plt.ylabel("SNR")
    plt.subplot(3, 1, 2)
    sns.scatterplot(data=fdets, x='time', y='doppler', color="green", \
                marker='o', s=40, edgecolor=None)
    plt.ylabel("Fdets [MHz]")
    plt.subplot(3, 1, 3)
    sns.scatterplot(data=fdets, x='time', y='residuals', color="orangered", \
                marker='.', s=40, edgecolor=None)
    plt.ylabel("Dnoise [Hz]")
    plt.xlabel("Time [s]")
    plt.tight_layout()
    plt.show()

elif statistics:
    ttotal: float = fdets['time'].iloc[-1] - fdets['time'].iloc[0]

    if column_header == 'UTC' or column_header == 'scan':
        ttotal = ttotal.seconds

    mSNR: float = fdets['snr'].mean()
    Fmin: float = np.min(fdets['doppler'])
    Fmax: float = np.max(fdets['doppler'])
    dFreq: float = Fmax - Fmin
    rDopp: float = np.std(fdets['residuals']) * 1e3
    vDopp: float = np.var(fdets['residuals']) * 1e3

    flags = np.zeros(n_scans)
    sDop = np.zeros(n_scans)
    sSNR = np.zeros(n_scans)

    if n_scans > 1 :
        # Case 1: the first column contains the scan index
        if column_header == 'scan':
            nrows, ncols = np.shape(fdets)

            valid_scans = fdets['index'].unique()

            for ip in np.arange(n_scans):
                sDop[ip] = np.std(fdets[fdets['index'] == valid_scans[ip]].residuals)
                sSNR[ip] = np.mean(fdets[fdets['index'] == valid_scans[ip]].snr)
                if sDop[ip] > 1:
                    flags[ip] = True

        # Case 2: Old format with time stamps in the first column
        else:
            # Total number of time samples (nrows) and measurements (ncols)
            nrows, ncols = np.shape(fdets)

            # interval sample
            ts = fdets['time'][1] - fdets['time'][0]

            jp = 0
            lscans = np.zeros(n_scans + 1, dtype=int)
            for ip in np.arange(nrows - 1):
                if np.abs(fdets['time'][ip + 1] - fdets['time'][ip]) > 2 * ts:
                    lscans[jp + 1] = ip + 1
                    jp += 1
            lscans[-1] = -1

            for ip in np.arange(n_scans):
                sDop[ip] = np.std(fdets['residuals'][lscans[ip] : lscans[ip + 1]])
                sSNR[ip] = fdets['snr'][lscans[ip] : lscans[ip + 1]].mean()
                if sDop[ip] > 1:
                    flags[ip] = True

    print(f"\nSession observed {dd}.{mm}.20{yy} - {station}")
    print(f"Number of scans {n_scans}, No samples {np.shape(fdets)[0]}")
    print(f"Scan initial time {fdets['time'][0]}\n")
    print(f"{Fore.BLUE}Average SNR:         {mSNR:.2f}")

    # Select the first value of the frequency detection
    print(f"Bband init freq:     {fdets['doppler'][0]:.1f} Hz")
    print(f"Sky init freq:       {freq_base + fdets['doppler'][0] / 1e6:.7f} MHz")

    print(f"Doppler velocity:    {np.divide(dFreq, ttotal):.2f} Hz/s")
    print(f"                     {np.divide(dFreq, n_scans):.2f} Hz/scan")
    print(f"Total Doppler shift: {dFreq:.2f} Hz{Style.RESET_ALL}\n")
    print(f"Doppler noise mean:  {rDopp:.3f} mHz")
    print(f"Doppler noise var:   {vDopp:.3f} mHz\n")

    if n_scans > 1:
        for ip in np.arange(n_scans):
            if flags[ip]:
                print(f"{Fore.RED}Scan no {ip+1:2d} std noise {sDop[ip] * 1e3:.2f} mHz" + \
                      f" & SNR of {10 * np.log10(sSNR[ip]):.1f} dB - {sSNR[ip]:.0f} {Style.RESET_ALL}")
            else:
                print(f"{Fore.GREEN}Scan no {ip+1:2d} std noise {sDop[ip] * 1e3:.2f} mHz" + \
                      f" & SNR of {10 * np.log10(sSNR[ip]):.1f} dB - {sSNR[ip]:.0f} {Style.RESET_ALL}")
