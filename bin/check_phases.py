#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Thu May 21 08:07:37 2020

@author: gofrito
"""

import argparse
import errno
import os
import re
import sys

import matplotlib.pyplot as plt
import numpy as np

from pysctrack import handler

parser = argparse.ArgumentParser()
parser.add_argument("filename", nargs="?", help="Input file with Phasen detections")
parser.add_argument("-p", "--plot", help="Plot results", action="store_true", default=False)
parser.add_argument("-s", "--statistics", help="Show stats", action="store_true", default=False)
args = parser.parse_args()

filename: str = args.filename
plot: bool = args.plot
STATS: bool = args.statistics

# by default activate stats
if plot is False:
    STATS = True

# if we have 1 scan
if filename.count(".") == 6:
    match = re.search(r"^([^.]*).([^.]*).([^.]*).([^.]*).([^.]*).([^.]*).", filename, re.I)
    scan = match.group(6)
# if we have multiple scans
elif filename.count(".") == 5:
    match = re.search(r"^([^.]*).([^.]*).([^.]*).([^.]*).([^.]*).", filename, re.I)
else:
    raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), filename)

file: str = match.group(1)
spacecraft: str = match.group(2)[0:3]
yy: str = match.group(2)[5:7]
mm: str = match.group(3)
dd: str = match.group(4)
station: str = match.group(5)

epoch = f"{dd}.{mm}.20{yy}"

try:
    phases = handler.read_phases(filename)
except FileNotFoundError:
    print("File not found. Check the path variable and filename")
    sys.exit()

nscan: int = handler.calc_scans(phases, fmt="phases")

if STATS:
    for ip in np.arange(nscan):
        #print(f"\033[94mNumber of samples: {np.shape(phases)[0]}")
        print(f"\033[94mScan no {ip+1:2d} phase std dev {1000*np.std(phases[:,ip+1]):.3f} mrad\033[0m")

if plot:
    plt.figure()
    plt.plot(phases[:, 0], phases[:, 1], color="deepskyblue", label="Phase 1")
    plt.xlabel("Time [s]")
    plt.ylabel("Residual phase [rad]")
    plt.title(f"Residual phase (1st scan) recorded of {spacecraft} on {epoch} at {station}")
    plt.show()
