#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Mon May 18 16:04:22 2020

v0.2 - add new fdets format
v0.1 - compile results for Fdets and Phase
@author: gmolera
"""

import argparse
import os
import sys
import numpy as np
import pandas as pd

from colorama import Fore, Style

def read_phases(phase_file):
    phase = np.loadtxt(phase_file, skiprows=4)
    return phase

def merge_phases(filename, nfiles, flag):
    """
    Merge multiple residual phases file

    Parameters
    ----------
    filename : Phase files
        text file.
    nfiles : int
        Total number of files to merge.
    flag : list
        File number to exclude

    """
    phase = np.array([])

    if os.path.isfile(filename):
        fd = open(filename, "r")
        header = []
        for ip in np.arange(4):
            header.append(fd.readline())
        fd.close()
        first = 1

    valid_files = 0
    for nf in range(1, nfiles):
        phase_file = f'{filename[0:-9]}.{nf:04d}.txt'
        if os.path.isfile(phase_file):
            if str(nf) in flag:
                print(f"Opening : {phase_file}\033[93m Skipped {Style.RESET_ALL}")
            else:
                valid_files += 1
                if first == 1:
                    phase = read_phases(phase_file)
                    first = 0
                    print(f"Opening : {phase_file}{Fore.GREEN} OK {Style.RESET_ALL}")
                else:
                    phases = read_phases(phase_file)
                    phase = np.append(phase, np.delete(phases, 0, axis=1), axis=1)
                    print(f"Opening : {phase_file}{Fore.GREEN} OK {Style.RESET_ALL}")
        else:
            print(f"Opening : {phase_file}{Fore.RED} Not Found {Style.RESET_ALL}")

    phase_fn = f"{filename[0:-9]}.txt"
    phase_header = header[0][2:] + header[1][2:] + header[2][2:]
    format = "%.18e"
    format += " %+.18e" * valid_files
    np.savetxt(phase_fn, phase, fmt=format, delimiter=" ", header=phase_header)

    print(f"File stored as: {phase_fn}")

def merge_fdets(fname: str, n_files: int, flag):
    """
    Merge multiple Frequency detections file

    Parameters
    ----------
    filename : Fdets files
        text file.
    nfiles : int
        Total number of files to merge.
    flag : list
        File number to exclude

    """
    print('')

    if os.path.isfile(fname):
        with open(fname, 'r', encoding='utf8') as my_file:
            header = []
            for _ in np.arange(4):
                header.append(my_file.readline())
        first = True

    valid_scan: int = 0

    for scan in np.arange(start=1, stop=n_files):
        # Read a new Fdets file every time
        fdets_file = f'{filename[0:-13]}.{scan:04d}{filename[-8:-3]}txt'

        if os.path.isfile(fdets_file):
            if str(scan) in flag:
                print(f'Opening : {fdets_file}\033[93m Skipped {Style.RESET_ALL}')
            else:
                # File is valid increment the counter
                valid_scan += 1

                # print out status to screen
                print(f'Opening : {fdets_file}{Fore.GREEN} OK {Style.RESET_ALL}')

                if first:
                    fdets_ar = pd.read_csv(
                        fdets_file,
                        skiprows=4,
                        sep=r'\s+',
                        names=['time', 'snr', 'spectra_max', 'doppler', 'residuals'],
                    )

                    # Add a new column at the beginning with scan number
                    fdets_ar.insert(0, 'scan', f'{scan:04d}')

                    # Fdets dataframe create -> Append now the rest
                    first = False
                else:
                    fdets_new = pd.read_csv(
                        fdets_file,
                        skiprows=4,
                        sep=r'\s+',
                        names=['time', 'snr', 'spectra_max', 'doppler', 'residuals'],
                    )

                    # Add a new column at the beginning with scan number
                    fdets_new.insert(0, 'scan', f'{scan:04d}')

                    # Concatenate fdets data frame with the new file
                    fdets_ar = pd.concat([fdets_ar, fdets_new])
        else:
            print(f'Opening : {fdets_file}{Fore.RED} Not Found {Style.RESET_ALL}')

    # New output file name
    fdets_fn = f'{filename[0:-13]}{filename[-8:-3]}txt'

    # Add number of the scans at the end of line 2
    header_line2 = f'{header[1][2:-2]}{valid_scan}\n'

    header_line3 = (
        'Format: scan | UTC Time '
        + '|   Signal-to-Noise  |    Spectral max    '
        + '| Freq detection [Hz] |   Doppler noise [Hz]  |\n'
    )

    # Concatenate all the lins
    fdets_header = header[0][2:] + header_line2 + header_line3

    cols = fdets_ar.shape[1]

    # Save the file
    if cols == 6:
        np.savetxt(
            fdets_fn,
            fdets_ar,
            delimiter=' ',
            fmt='%3s %15s %.18e %.18e %21.12f %+.16e',
            header=fdets_header,
        )
    elif cols == 7:
        np.savetxt(
            fdets_fn,
            fdets_ar,
            delimiter=' ',
            fmt='%3s %i %.6f %.18f %.18f %.12f %.16e',
            header=fdets_header,
        )

    print('')
    print(f'File stored as: {fdets_fn}')


parser = argparse.ArgumentParser(
    description='Append several result files in Fdets or Phase files'
)
parser.add_argument(
    'filename',
    help='First file of Phases or Fdets to merge',
    nargs='?',
    default='check_string_for_empty',
)
parser.add_argument(
    'nfiles', help='Number of files to append', nargs='?', type=int, default=1
)
parser.add_argument('-flag', help='flag scans to not include', nargs='+')

args = parser.parse_args()

filename: str = args.filename
nfiles: int = args.nfiles + 1

with open(filename, 'r', encoding='utf-8') as fd:
    content = fd.readlines()
column_header = content[2].split(' ')[2]


if args.flag:
    files_flag = args.flag
else:
    files_flag = []

print(f'Initial file to start merging: {filename}')

# All the data mergins is done in handler library
if filename[0:5] == 'Fdets':
    if column_header in ('Modified', 'Time(UTC)', 'MJD'):
        FDETS_FORMAT = 'v0'
        fdets = pd.read_csv(
            filename,
            skiprows=4,
            sep=r'\s+',
            names=['mjd', 'time', 'snr', 'spectra', 'doppler', 'residuals'],
        )
    elif column_header == 'UTC':
        FDETS_FORMAT = 'v1'
        fdets = pd.read_csv(
            filename,
            skiprows=4,
            sep=r'\s+',
            names=['time', 'snr', 'spectra', 'doppler', 'residuals'],
        )
    elif column_header == 'scan:':
        print('Data are already in merged format')
    else:
        print('Format not recognized')
        sys.exit()
    print(f'Fdets data format {FDETS_FORMAT}')
    if FDETS_FORMAT == 'v0':
        handler.merge_fdets(filename, nfiles, files_flag)
    elif FDETS_FORMAT == 'v1':
        merge_fdets(filename, nfiles, files_flag)
elif filename[0:5] == 'Phase':
    merge_phases(filename, nfiles, files_flag)
else:
    print('\033[91mError parsing the input file\033[0m')
