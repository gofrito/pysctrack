#!/usr/bin/env python
# -*- coding: utf-8 -*-
import argparse
import shutil
import os
import numpy as np
import pandas as pd
from astropy.time import Time, TimeDelta

parser = argparse.ArgumentParser()

parser.add_argument("filename", nargs="?", \
                    help="spectra input file", default="check_string_for_empty")

args = parser.parse_args()

fd = open(args.filename)
for ip in range(5):
    line = fd.read()

num_columns = len(line.split(' '))
new_format= False
if num_columns == 6:
    new_format = False
elif num_columns == 5:
    new_format = True

if new_format:
    fdets = pd.read_csv(args.filename, skiprows=4, delimiter=' ', \
                        names=['time', 'snr', 'spectra_max', 'doppler', 'residuals'])
    fd = open(args.filename, 'r')
    fdets_header = []
    for ip in range(2):
        fdets_header.append(fd.readline())
    fheader = fdets_header[0].replace('# ','') + fdets_header[1].replace('# ','') + 'Format: Modified JD   |       Time(UTC) [s]    |    Signal-to-Noise     |      Spectral max      | Freq detection [Hz] |   Doppler noise [Hz]  |\n'
    fd.close()

    epoch = np.zeros(len(fdets.time))
    seconds = np.zeros(len(fdets.time))
    for ip in range(len(fdets.time)):
        epoch[ip] = int(Time(fdets.time.iloc[ip], format='isot', scale='utc').mjd)
        seconds[ip] = (Time(fdets.time.iloc[ip], format='isot', scale='utc').mjd - epoch[ip])*86400

    data = {'timestamp': epoch,
        'seconds' : seconds,
        'SNR': fdets.snr,
        'Spectral Max': fdets.spectra_max,
        'Doppler': fdets.doppler,
        'Residual fit': fdets.residuals}

    shutil.copy(args.filename, f'{args.filename}'.replace("txt","old"))
    np.savetxt(f'{args.filename}', pd.DataFrame(data=data), \
                fmt='%i %15s %.18e %.18e %21.12f %+.16e', header=fheader)
else:
    fdets = pd.read_csv(args.filename, skiprows=4, delimiter=' ', \
                        names=['mjd', 'time', 'snr', 'spectra_max', 'doppler', 'residuals'])

    #print(fdets)
    fd = open(args.filename, "r")
    fdets_header = []
    for ip in range(2):
        fdets_header.append(fd.readline())
    fheader = fdets_header[0].replace('# ','') + fdets_header[1].replace('# ','') + 'Format: UTC Time     |    Signal-to-Noise     |      Spectral max      | Freq detection [Hz] |   Doppler noise [Hz]  |\n'
    fd.close()

    epoch = Time(fdets.mjd[0], format='mjd', scale='utc')
    seconds = TimeDelta(fdets.time, format='sec')

    time = epoch + seconds

    print(epoch.to_value('iso', subfmt='date'))

    data = {'timestamp': time.isot,
            'SNR': fdets.snr,
            'Spectral Max': fdets.spectra_max,
            'Doppler': fdets.doppler,
            'Residual fit': fdets.residuals}

    shutil.copy(args.filename, f'{args.filename}'.replace("txt","old"))
    np.savetxt(args.filename, pd.DataFrame(data=data), \
                fmt='%15s %.18e %.18e %21.12f %+.16e', header=fheader)
