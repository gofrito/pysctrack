#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Mon May 25 20:41:24 2020

Core of the digital Phased-Lock-Loop. Run a sctracker
version with a narrow-band tone and extract the residual
phase.
v0.7 - Fixed makeFilt + Phase Doppler building
v0.6 - Moved from a class to script 03.06.2023
v0.5 - Merging with TBB
v0.4 - Moved to use only f-strings
v0.3 - New version with Fdets3 output
@author: gofrito
"""

import argparse
import configparser
import errno
import os
import re
import shutil
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import pandas as pd

from astropy import time
from astropy.time import Time, TimeDelta
from colorama import Fore
from scipy import stats, signal
from pysctrack import core, handler

# Compute phase shift normalised_time_based - time_based or sample_based
SELECT_METHOD = 'sample_based'
sns.set_theme(style="darkgrid")

# find the directory where pysctrack and catalogues are installed
PYSCTRACK_DIR: str = os.path.dirname(os.path.realpath(__file__))[:-3]
PYSCTRACK_CAT: str = os.path.join(PYSCTRACK_DIR, 'cats')

# debug information
start: float = Time.now()

def parse_inifile(station, scan_number):
    sctracker = configparser.ConfigParser()
    sctracker.read(f"inifile{station}{int(scan_number)}")

    # Read the INI config file and extract the time to add
    start_t = int(sctracker.get("Settings", "SourceSkipSeconds"))
    stop_t = int(sctracker.get("Settings", "SourceStopSeconds"))

    # In case stop time is 0 then use the size of the file
    if stop_t == 0:
        stop_t = int(total_file)
        scan_t = stop_t - start_t
        # If the start time is greater than the size of the file, set the scan time to be the stop time
        if start_t >= stop_t:
           scan_t = stop_t
    else:
        # Remove the 20 s added in calculateCpp
        scan_t = stop_t - start_t - 20

    return(scan_t, start_t)

parser = argparse.ArgumentParser()
parser.add_argument("filename", nargs="?", help="Input spectra file")
parser.add_argument("-bw", dest="bandwidth", \
                    help="Bandwidth", type=float, default=2000)
parser.add_argument("-bw2", dest="bandwidthStep2", \
                    help="Bandwidth used with 2nd fit", type=float, default=20)
parser.add_argument("-bw3", dest="bandwidthStep3", \
                    help="Bandwidth used with 3rd fit", type=float, default=20)
parser.add_argument("-bw4", dest="bandwidthStep4", \
                    help="Bandwidth used for ultra narrow band", type=float, default=5)
parser.add_argument("-ch", dest="channel", help="Number of channel", type=int, default=-1)
parser.add_argument("-dp", "--doppler", \
                    help="Generate Doppler rev. 3", action="store_true", default=False)
parser.add_argument("-ep", dest="epoch", \
                    help="specify your own epoch - format ex. 2020.02.02", type=str)
parser.add_argument("-ft", dest="forcetime", \
                    help="Force integration time if swspec and sctracker use different", type=int)
parser.add_argument("-ff", dest="forcefft", \
                    help="Force number of FFT if swspec and sctracker use different", type=int)
parser.add_argument("-ni", "--no_inifile", help="don't read obs details from the inifile", \
                    action="store_true", default=False)
parser.add_argument("-nav", dest="nav", help="Number of averaged FFT", type=int, default=2)
parser.add_argument("-nf", dest="nfft", \
                    help="Number of FFT points", type=int, default=20000)
parser.add_argument("-o", "--output", \
                    help="create ultra narrow band tone", action="store_true", default=False)
parser.add_argument("-ol", dest="overlap", help="FFT overlapping factor", type=int, default=2)
parser.add_argument("-of", dest="output_format", \
                    help="Data format output", type=int, default=1)
parser.add_argument("-p", "--plot", \
                    help="Plot a summary of the results on screen", action="count", default=False)
parser.add_argument("-pa", dest="padding", help="Padding factor", type=int, default=2)
parser.add_argument("-pc", "--powercentre", \
                    help="Dont use centroid bin for Doppler", action="store_true", default=True)
parser.add_argument("-p1", dest="cpp1", help="First Polynomial", type=int, default=6)
parser.add_argument("-p2", dest="cpp2", help="Second Polynomial", type=int, default=6)
parser.add_argument("-p3", dest="cpp3", help="Third Polynomial", type=int, default=6)
parser.add_argument("-rf", dest="radio_frequency", \
                    help="Starting Radio Frequency", type=float)
parser.add_argument("-sc", dest="spacecraft", \
                    help="specify spacecraft ortherwise use filename", type=str)
parser.add_argument("-ta", dest="addtime", \
                    help="Time added in SCtracker", type=float, default=0)
parser.add_argument("-t0", dest="starttime", \
                    help="Seconds to skip in dPLL", type=float, default=0)
parser.add_argument("-t1", dest="endtime", \
                    help="Total number of seconds in dPLL", type=float, default=0)
parser.add_argument("-vx", "--vexade", \
                    help="Fast S/C -> dont use SNR weight", action="store_true", default=False)

args = parser.parse_args()

vexade: bool = args.vexade
powcen: bool = args.powercentre
dopp3: bool = args.doppler
output: bool = args.output
t_add: int = args.addtime
t_start: int = args.starttime
fft_points: int = args.nfft

# Pass settings related to the swspec - They may differ from sctracker
force_time: bool = False
if args.forcetime:
    force_time: int = args.forcetime

force_fft: bool = False
if args.forcefft:
    force_fft: int = args.forcefft

# Settings related to PLL
BW: int       = args.bandwidth
BW2: int      = args.bandwidthStep2
BW3: int      = args.bandwidthStep3
BW4: int      = args.bandwidthStep4
npp1: int     = args.cpp1
npp2: int     = args.cpp2
npp3: int     = args.cpp3
Ovlp: int     = args.overlap
Nav: int      = args.nav
Padd: int     = args.padding

# Define output format
verbose: bool = False
fverbose: bool = False
output_format = args.output_format

if args.plot == 1:
    verbose: bool = True
elif args.plot > 1:
    fverbose: bool = True

# Check if the file exists
if os.path.exists(args.filename):
    path, filename = os.path.split(args.filename)
    path: str = f"{os.path.abspath(path)}/"
    fil_lng: int = len(filename) - 10
else:
    raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), args.filename)

# Read settings used in sctracker
match = re.search(r"^([^_]*)_([^_]*)_(?:([^_]*))_([^_]*)_(\d+)(?:pt)?_(\d+)s?_(?:ch)?([^_]+)_tone(\d+).bin", filename, re.I)

# Assuming std nomenclature assign settings
session: str = match.group(1)
station: str = match.group(2)
raw_format: str = match.group(3)
scan_number: int = int(match.group(4)[2:6])
scan_mode: str = match.group(4)[0:2]
FFT: int = match.group(5)
ints: int = match.group(6)
channel: int = match.group(7)
tone_nr: int = int(match.group(8))

if args.channel != -1:
    channel = args.channel

# Estimate the number of samples in the file
fsize: float = os.path.getsize(f'{path}{filename}')
total_file: float = fsize / (2 * 8 * BW)

# Use t1 or total number of samples
if args.endtime:
    t_stop: float = args.endtime
    Ns: int = t_stop * 2 * 8 * BW
elif not args.no_inifile:
    t_stop, t_add = parse_inifile(station, scan_number)
    Ns: int = t_stop * 2 * 8 * BW
else:
    t_stop = total_file
    Ns: int = int(fsize / 8)

# length of the scan in seconds is t1 - t0
t_scan = t_stop - t_start

print('\n# Settings')
print(f"Length of the file   : {total_file} sec")
print(f"Length selected      : {t_scan} sec")
print(f"Samples selected     : {Ns} samples")

# Make sure that is a finite number of samples
dts: float = fft_points / BW
t_scan = np.floor(t_scan / dts) * dts

# Recalculate t_stop accounting for the number of samples
t_stop = t_start + t_scan

# Set the epoch manually
if args.epoch:
    epoch: str = args.epoch
else:
    epoch: str = f"20{session[-6:-4]}.{session[-4:-2]}.{session[-2:]}"

# Set the spacecraft manually
if args.spacecraft:
    spacecraft: str = args.spacecraft
else:
    spacecraft: str = handler.assign_spacecraft(session[0])

# Unless we force the settings manually
if args.radio_frequency:
    baseband_freq: float = args.radio_frequency
else:
    stations = pd.read_json(os.path.join(PYSCTRACK_CAT, 'station.json'))
    try:
        baseband_freq = stations.loc[stations['code'] == station][session[:-6]].values[0]
    except:
        print(f"Station {station} and spacecraft {session[:-6]} combination do not exist in station.json, please update or else use the -rf argument instead")
        print("Default value will be used")
        baseband_freq = 8400

# Define frequency polynomial order
npf1: int = npp1 - 1
npf2: int = npp2 - 1

# Reconstruct base filename for sctracker
sc_file: str = f"{session}_{station}_{raw_format}_" + \
               f"{scan_mode}{scan_number:04d}_{FFT}pt_{ints}s_ch{channel}"

# sctracker and swspec output files may have different basename
sw_file: str = sc_file

# we force the integration time
if force_time:
    sw_file = f"{session}_{station}_{raw_format}_" + \
        f"{scan_mode}{scan_number:04d}_{FFT}pt_{force_time}s_ch{channel}"

# we force the FFT
if force_fft:
    sw_file = f"{session}_{station}_{raw_format}_" + \
        f"{scan_mode}{scan_number:04d}_{force_fft}pt_{ints}s_ch{channel}"

# we force the FFT and the integration time
if force_fft and force_time:
    sw_file = f"{session}_{station}_{raw_format}_" + \
        f"{scan_mode}{scan_number:04d}_{force_fft}pt_{force_time}s_ch{channel}"

# We read data from auxiliary files
tonebin = f"{path}{sc_file}_tonebinning.txt"
timebin = f"{path}{sc_file}_starttiming.txt"
runlog  = f"{path}{sc_file}_runlog.txt"
filecpp = f"{path}{sw_file}.poly{npp1}.txt"
filecfs = f"{path}{sw_file}.X{npf1}cfs.txt"

# Raise an error if a file is not found
if os.path.isfile(filecpp) is False:
    print(f'{Fore.RED} Poly coefficients not found : {filecpp}{Fore.BLACK}')
    raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), filecpp)

if os.path.isfile(tonebin) is False:
    print(f'{Fore.RED} Tone file not found : {tonebin}{Fore.BLACK}')
    raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), tonebin)

if os.path.isfile(timebin) is False:
    print(f'{Fore.RED} Start time file not found : {timebin}{Fore.BLACK}')
    raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), timebin)

# Read Tone and Start time to know the beginning of the raw data file
Tbinfo: float = np.loadtxt(tonebin, skiprows=1, max_rows=1)
Tsinfo: float = np.loadtxt(timebin, skiprows=1, max_rows=1)

# Take the initial time stamp from swspec
if len(Tsinfo) > 0:
    StartT: float = Tsinfo[1]
    mod_jd: int = Tsinfo[0]
else:
    if os.path.exists(runlog):
        with open(runlog, 'r', encoding='utf-8') as fd:
            log_file = fd.readlines()

        # Usually 2nd line contains filename information
        if log_file[1].split(' ')[0] == 'Input':
            input_file = log_file[1].split(' ')[-1]
        else:
            # Sometimes the second line contains SkipSeconds
            input_file = log_file[2].split(' ')[-1]

        # Make a simple string split
        yy = input_file.split('-')[1]
        mm = input_file.split('-')[2]
        dd = input_file.split('-')[3]
        HH = input_file.split('-')[4]
        MM = input_file.split('-')[5]
        SS = input_file.split('-')[6].split('_')[0]

        # Convert to astropy time
        t = Time(f'{yy}-{mm}-{dd}T{HH}:{MM}:{SS}', format='isot', scale='utc')

        # We need mjd and seconds of the day
        new_mjd = int(t.mjd)
        new_sec = int(86400 * (t.mjd - new_mjd))

        # Overwrite the start timing file
        with open(timebin, 'w', encoding='utf-8') as fd:
            fd.write('// USRP Ettus start time <MJD sec ns>\n')
            fd.write(f'{new_mjd} {new_sec} 0.000000')

        StartT: float = new_mjd
        mod_jd: int = new_sec
    else:
        # Test case
        StartT: float = 0.0
        mod_jd: int = 0

# Take the initial frequency bin from swspec
if np.ndim(Tbinfo) > 1:
    StartF: float = Tbinfo[tone_nr][3]
else:
    StartF: float = Tbinfo[3]

# Read the Phase Polynomial Coefficients iteration 1
cpp1: float = np.loadtxt(filecpp)
cfs1: float = np.loadtxt(filecfs)

# inialise variables
SR: int = 2 * BW                           # sampling rate
Nt: int = int(np.round(t_scan * SR))       # total number of samples
sampling_interval: np.double = 1 / SR      # sampling interval
df: float = SR / fft_points                # frequency resolution of the FFT
tw: float = sampling_interval * fft_points # filter set up time

# sample and time grids
jt: np.double = np.arange(start=0, stop=Nt, dtype=np.double)
tt: np.double = jt * sampling_interval

# Initialise more variables
spec_start: int = int(np.round(t_start * df / Nav))
spec_stop: int = int(np.round(t_stop * df / Nav))

num_spectra: int = spec_stop - spec_start

# Start and Stop in sample number
Nt_start = int(np.round(t_start * SR))
Nt_stop = int(np.round(t_stop * SR))

# Total number of samples to process
Nt_scan: int = t_scan * SR

segm_start: int = int(Nt_start / fft_points * Ovlp)
segm_stop: int = int(Nt_stop / fft_points * Ovlp - 1)
num_segm: int = int(np.floor(Nt / fft_points) * Ovlp - (Ovlp - 1))

# Fix jspec goes to spec_stop -1 -> t_spec goes until 1125 since we skip non-full 10s
jspec: int = np.linspace(0, num_spectra - 1, num_spectra)

Bav: int = fft_points * Nav

# time grid which is in the middle of time interval
t_spec: float = (jspec + 0.5) * Bav * sampling_interval

# lenght of the vector including the zero-padding
if Padd == 1:
    Npadd = 1
elif Padd > 1:
    Npadd: int = fft_points * (Padd - 1)
dpadd: int = np.zeros(Npadd)

print(f'Skip  # of spectra   : {spec_start}')
print(f'Total # of spectra   : {spec_stop}')
print(f'Frequency resolution : {df} Hz')
print(f'Sampling interval    : {1e3 * sampling_interval} ms')
print('\n# Measurements')

# Keep initiliasing variables
jps: int = np.arange(fft_points)
Win: float = np.power(np.cos(np.pi / fft_points * (jps - 0.5 * fft_points + 0.5)), 2)
Nfp: float = fft_points * Padd / 2 + 1
jfs: float = np.arange(Nfp)
dfs: float = 1 / (tw * Padd)

ffs: float = jfs * dfs
# print(f'Frequency resolution with filter : {dfs} Hz')

# make_spec is the core function that reads the tone and outputs the spectra
Sp: float = core.make_spec(f"{path}{filename}", spec_stop, fft_points, Nav, Ovlp, Win, Padd, dpadd)

# Sp contains all spectra from 0 to t_stop, reshape if t0 > 0
Sp = Sp[spec_start : , : ]

# Calculate the mean value to normalise
xSp: float = np.mean(Sp)
Sp = np.divide(Sp, xSp)

# Spa contains the sum of all spectra
Spa: float = np.divide(Sp.sum(axis=0), num_spectra)

# Full verbose: Plot spectrum of the initial tone
if fverbose:
    ax = sns.lineplot(x=ffs, y=10 * np.log10(Spa), color="blue", linewidth=1.5)
    plt.title("Step 1 - Full stacked spectra")
    plt.xlim([0, BW])
    plt.ylabel("Power spectrum [dB]")
    plt.xlabel("Frequency [Hz]")
    plt.show()

# Full verbose: Plot spectrum of the initial tone - Zoom
if fverbose:
    sns.lineplot(x=ffs, y=10 * np.log10(Spa), color="blue", markers=True)
    plt.title("Step 1 - Full stacked spectra - Zoom to carrier")
    plt.ylabel("Power spectrum [dB]")
    plt.xlabel("Frequency [Hz]")
    plt.xlim([0.475 * BW, 0.525 * BW])
    plt.show()

# Set limits to calculate peak and SNR
half_window: int = 40
line_avoidance: int = 10

# Initialise all possible vecttors
xf: float = np.zeros((num_spectra, 3))
rmsd: float = np.zeros((num_spectra, 3))
dxc: float = np.zeros(num_spectra)
snr: float = np.zeros(num_spectra)
Smax: float = np.zeros(num_spectra)
freq_det: float = np.zeros(num_spectra)
MJD: float = np.zeros(num_spectra)
weight: float = np.ones((num_spectra, 1))

# Implement calculateCpp in narrow band
for ip in np.arange(start=0, stop=num_spectra):
    xf[ip] = core.FindMax(Sp[ip], ffs)
    Smax[ip] = xf[ip, 2]
    freq_det[ip] = dfs * xf[ip, 1] + ffs[0]
    if powcen:
        dxc = core.PowCenter(Sp[ip], xf[ip, 1], 3) * dfs
        freq_det[ip] += np.transpose(dxc)
    rmsd[ip] = core.GetRMS(Sp[ip], xf[ip, 1], half_window / dfs, line_avoidance / dfs)
    snr[ip] = (xf[ip, 2] - rmsd[ip, 0]) / rmsd[ip, 1]

# Calculate mean values
msnr: float = snr.mean()
mfreq_det: float = freq_det.mean()

# If vexade mode is enabled don't use SNR weights
if vexade:
    freq_det -= mfreq_det
else:
    weight = np.power(snr, 2) / msnr

# Calculate the second set of polynomical fits
freq_fit: np.double = core.PolyfitW1(t_spec, freq_det, weight, npf2)
cf2: np.double = core.PolyfitW1C(t_spec, freq_det, weight, npf2)

# Calculate the residual of the fit
rFdet: float = freq_det - freq_fit

# Verbose: Make a summary plot of the frequency detections
if verbose or fverbose:
    plt.figure(figsize=(8, 9), dpi=80)
    plt.suptitle("Step 2 - Summary of the frequency fit rev. 2")
    plt.subplot(4, 1, 1)
    sns.lineplot(x=ffs, y=10 * np.log10(Spa))
    plt.ylabel("Spectrum")
    plt.xlabel("Frequency [Hz]")
    plt.xlim([0, BW])
    plt.subplot(4, 1, 2)
    sns.lineplot(x=t_spec, y=snr, markers='x', color='red')
    plt.ylabel("SNR")
    plt.xlim([0, t_spec.max()])
    plt.xticks([], [])
    plt.subplot(4, 1, 3)
    sns.lineplot(x=t_spec, y=freq_det, color='red', markers='.')
    sns.lineplot(x=t_spec, y=freq_fit, color='purple', markers='.')
    plt.ylabel("Freq [Hz]")
    plt.xlim([0, t_spec.max()])
    plt.xticks([], [])
    plt.subplot(4, 1, 4)
    sns.scatterplot(x=t_spec, y=rFdet, color="brown", markers='x')
    plt.xlabel("Time [s]")
    plt.ylabel("Residual Freq [Hz]")
    plt.xlim([0, t_spec.max()])
    plt.show()

# Full verbose: Plot the histogram - A Gaussian is spot on
if fverbose:
    plt.title("Step 2 - Histogram of the frequency fit residuals")
    n, bins, patches = plt.hist(rFdet, density=True, bins=25)
    (mu, sigma) = stats.norm.fit(rFdet)
    y = stats.norm.pdf(bins, mu, sigma)
    plt.plot(bins, y, "r--")
    plt.xlabel("Residual Freq [Hz]")
    plt.ylabel("Probability")
    plt.show()

# Full full verbose: waterfall @TBB
if fverbose:
    plt.title("Step 2 - Waterfall of the carrier")
    plt.rcParams['axes.grid'] = False
    dyn_spectra = plt.pcolormesh(ffs, t_spec, 10 * np.log10(Sp), cmap='magma', shading='auto')
    plt.colorbar(dyn_spectra)
    plt.xlabel('Frequency [Hz]')
    plt.ylabel('Time [sec]')
    plt.show()

# correct mean fdets if if vexade is used
if vexade:
    cf2[0] += mfreq_det

# Print out results
print(f"\033[94mStd dev  : {1e3 * rFdet.std():.2f} mHz\033[0m")
print(f"\033[94mSNR mean : {snr.mean():.2f} \033[0m")

# Initialise the Coefficient variables
cfs2: float = np.zeros(npp2)
cpp2: float = np.zeros(npp2 + 1)
cps2: float = np.zeros(npp2 + 1)
cpr2: float = np.zeros(npp2 + 1)

# Build the frequency detections from the First polynomials
Ffirst: float = np.zeros(num_spectra)

# Time span is equal to t_spec max as t_spec always start at 0
Tspan = t_spec.max()

'''
Note on 26.01.2023
Be careful with Tspan and t_scan - previously i used Tspan as the total length of tone data
read, but to calculate Cfs we must use the maximum time length used in PolyfitW1C, which is
t_spec.max() - see Mathcad - Step2.MakeFdets.Polys.pdf
'''
# Convert to frequency polynomials per second
for jp in np.arange(npp2):
    cfs2[jp] = cf2[jp] * np.power(Tspan, -jp)

# Calculate the phase polynomials coefficients
for jp in np.arange(1, npp2 + 1):
    cps2[jp] = 2 * np.pi * cfs2[jp - 1] / jp
    cpp2[jp] = cps2[jp] * np.power(sampling_interval, jp)
    cpr2[jp] = 2 * np.pi * cf2[jp - 1] / jp

# Build the new Doppler detections based on the first polynomial
for ip in np.arange(start=0, stop=num_spectra):
    for jp in np.arange(start=1, stop=npp1):
        Ffirst[ip] += cfs1[jp] * np.power(t_spec[ip], jp)

# Fvideo includes the 1st polynomials fit and the residuals (r2i)
Fvideo: float = StartF + Ffirst + freq_det

# Correct mean Fdets in vexade
if vexade:
    Fvideo += mfreq_det

# Add all the start times
tts: float = t_spec + StartT + t_add

# Build a proper vector with MJD time stamps
# Initialise the first
MJD[0] = mod_jd

for ip in np.arange(start=1, stop=len(tts)):
    # If timestamp is smaller, it means pass 00:00:00
    if tts[ip] < tts[ip-1]:
        # Just increase the modified julian date reference
        mod_jd += 1

    # Assign each element of the vector
    MJD[ip] = mod_jd

# Storing the day of the day, timestamp, snr, Spectral max, Fdet, Residual Fdets
fdets_fn: str = f"{path}Fdets.{spacecraft}{epoch}.{station}.{scan_number:04d}.r2i.txt"

if output_format == 0:
    fdets_header: str = (
        f'Observation conducted on {epoch} at {station} rev. 2'
        f'\nBase frequency: {baseband_freq:.2f} MHz dF: {df} Hz dT:'
        f' {Bav * sampling_interval} s Nscans: 1\nFormat: MJD |'
        f' Time [s] | Signal-to-Noise |  Spectral max  '
        f'| Freq detection [Hz] | Doppler noise [Hz]  |\n'
    )

    data = {'mjd': MJD,
            'timestamp': tts,
            'SNR': snr,
            'Spectral Max': Smax,
            'Doppler': Fvideo,
            'Residual fit': rFdet}

    np.savetxt(fdets_fn, pd.DataFrame(data=data),delimiter=' ', \
               fmt='%i %.6f %.18f %.18f %.12f %.16e', header=fdets_header)

elif output_format == 1:
    epochs = Time(MJD[0], format='mjd', scale='utc')
    seconds = TimeDelta(tts, format='sec')

    time = epochs + seconds

    fdets_header: str = (
        f'Observation conducted on {epoch} at {station} rev. 2'
        f'\nBase frequency: {baseband_freq:.2f} MHz BW: {int(BW/1000)} kHz dF: {df} Hz dT:'
        f' {Bav * sampling_interval} s Nscans: 1\nFormat: UTC Time     '
        '|    Signal-to-Noise     |      Spectral max      '
        '| Freq detection [Hz] |   Doppler noise [Hz]  |\n'
    )

    data = {'timestamp': time.isot,
            'SNR': snr,
            'Spectral Max': Smax,
            'Doppler': Fvideo,
            'Residual fit': rFdet}

    np.savetxt(fdets_fn, pd.DataFrame(data=data), delimiter=' ', \
               fmt='%15s %.18e %.18e %21.12f %+.16e', header=fdets_header)

    # Copy the template of fdets info to the working directory
    if os.path.exists(f'{PYSCTRACK_CAT}/fdets_info.r2i.md'):
        shutil.copy(f'{PYSCTRACK_CAT}/fdets_info.r2i.md', 'fdets_info.r2i.md')

# Name the polynomial files and save to text
cppname = f"{path}{filename[0 : fil_lng]}.poly{npp2}.rev2.txt"
cfsname = f"{path}{filename[0 : fil_lng]}.X{npf2}cfs.rev2.txt"

# Save to disk
np.savetxt(cppname, cpp2)
np.savetxt(cfsname, cfs2)

# Prepare for the narrow-band phase rotation
# Initialise variables for the phase rotation
filter_ratio: int = int(BW / BW2)

Nffto: int = int(fft_points / filter_ratio)     # Output number of FFT points
jpso: int = np.arange(start=0, stop=Nffto)      # Output frequency bin grid
npfo: int = Nffto * 0.5 + 1                     # Number of spectral points to extract to output FFT
npfz: int = Nffto * 0.5 - 1                     # Zero padding of output FFT
BWih: float = 0.5 * BW2

# to do: change t_scan * SR for Nt
Nto: int = int(Nt / filter_ratio)               # Number of samples in the output signal
dto: float = sampling_interval * filter_ratio   # Output sampling interval
jto: int = np.arange(Nto)                       # Output bin grid

tto: float = jto * dto                          # Output time grid
dfto: float = 1 / t_scan                        # Output frequency resolution
fto: float = dfto * jto                         # Output frequency grid

# Shift (in samples) between overlapping segments
Oshifti: float = fft_points / Ovlp
Oshifto: float = Nffto / Ovlp

# Apodisation function for the input and output FFTT
Wini: float = np.cos(np.pi / fft_points * (jps - 0.5 * fft_points + 0.5))
Wino: float = np.cos(np.pi / Nffto * (jpso - 0.5 * Nffto + 0.5))

# Here Cf was confused for Cfs and Cpp for Cps
# - cfs1 was called to retrieve polys from previous round but never used
# - We would need cpp1 in case fine tunning on original data

# Store the carrier centre frequency
Fcc: np.double = cf2[0]

# Create the temporal vector for Doppler correction
res = []

# Compute phase shift normalised time based - time_based or sample_based
if SELECT_METHOD == 'normalised_time_based':
    # scale tt to per second
    ts: float = np.divide(tt, t_scan)

    for jp in np.arange(start=2, stop=npp2 + 1):
        res.append(cpr2[jp] * np.power(ts, jp))

    PhDopp = np.multiply(np.sum(np.array(res), axis=0), t_scan)
elif SELECT_METHOD == 'time_based':
    # use phase polynomials per second
    for jp in np.arange(start=2, stop=npp2 + 1):
        res.append(cps2[jp] * np.power(tt, jp))

    PhDopp = np.sum(np.array(res), axis=0)
elif SELECT_METHOD == 'sample_based':
    # Use phase polynomials per sample
    for jp in np.arange(start=2, stop=npp2 + 1):
        res.append(cpp2[jp] * np.power(jt, jp))

    PhDopp = np.sum(np.array(res), axis=0)
else:
    # Create a vector of 0 to not apply any correction
    PhDopp = np.zeros(np.size(jt))

# Bins consistent with MathCad.Task2a.Filter-PLL.Pass1.r4
# Esc -> keeps sign 26.01.2023

Bsc: int = int(np.floor((Fcc - BWih) / df))
Bec: int = int(np.floor(Bsc + npfo))
Fstartc: float = Bsc * df

Pssc = Fstartc * Oshifti * sampling_interval - np.floor(Fstartc * Oshifti * sampling_interval)
Esc = np.exp(1j * 2 * np.pi * Pssc)

# Conduct the phase correction now
#spf: np.double = core.MakeNewFiltX(f'{path}{filename}', PhDopp, Bsc, Bec,
#    Esc, Nto, Bav, spec_start, spec_stop, Wini, Wino, segm_start, num_segm, fft_points, Nffto, Ovlp)
spf: float = core.MakeFiltX(f'{path}{filename}', PhDopp, Bsc, Bec, Esc, Nto, \
                            Bav, spec_stop, Wini, Wino, num_segm, fft_points, Nffto, Ovlp)

# We now calculate the FFT of the full scan - No more segments
ssf: np.complex128 = np.fft.fft(spf)

# Calculate the absolute power and normalise
ssfp: np.double = np.power(np.abs(ssf), 2)
ssfp = np.divide(ssfp, ssfp.max())

# Save data to a file for presenting purposes or debugging
# 08.01.2024 GMC/JE -> Store the re/im tone instead of the normalised
# for spectral broadening - change np.save (ssf and ssfp)
if output:
    print(f'Saving the {BW2} Hz tone in numpy format')
    tone20_file = f'{path}{filename[0 : fil_lng]}_tone{int(BW2)}Hz.npy'
    np.save(f'{tone20_file}', [fto, ssf])

    print(f'Saving the {BW2} Hz tone file as complex floats')
    spf.tofile(f'{path}{filename[0 : fil_lng]}_tone0_{int(BW2)}Hz.bin')

# Check if the phase stop has worked properly
if fverbose:
    plt.title("Step 3 - Spacecraft tone after phase correction")
    ax = plt.subplot(1, 1, 1)
    #sns.lineplot(x=fto, y=10 * np.log10(ssfp))
    plt.semilogy(fto, ssfp)
    plt.grid(True, which="both", axis="y")
    ax.minorticks_on()
    plt.xlim([0, BW2])
    plt.ylim([1e-9, 1])
    plt.xlabel("Frequency [Hz]")
    plt.ylabel("Normalized Power")
    plt.show()

# Determine the frequency of the max power after phase correction
xft: float = core.FindMax(ssfp, fto)
fmax: float = dfto * xft[1]
spnoise: float = ssfp[200:900]
snr: float = np.divide(1, spnoise.std())
dBsnr: float = 10 * np.log10(snr)

print('\n# PLL')
print(f"\033[94mFreq: {(baseband_freq * 1e6 + StartF + fmax):.3f} Hz")
print(f"SNR :          {dBsnr:.2f} dB")

# Step 4.
# We can take a close look to the new tone in narrower bandwidth.
# The power of the signal is still split between multiple spectral bins.
# The phase correction is NOT error free.
# We iterate a new step on a narrower band to improve it.
# We rotate the tone by half of the output frequency (BW3)
# freq_rotation: Rotation frequency necessary
# sfc:  down converted signal
# ssf:  down converted FFT
# ssfp: down converted Power spectrum (normalized)

# Ftarg: Target frequency for the tone to rest
Ftarg: int = np.floor(0.5 * BW3)
freq_rotation: float = fmax - Ftarg

# Rotate the signal by freq_rotation
sfc: float = spf * np.exp(-1j * 2 * np.pi * freq_rotation * tto)
ssf: float = np.fft.fft(sfc)
ssfp: float = np.power(np.abs(ssf), 2)
ssfp /= ssfp.max()

# Plot the tone after shifting it down
if fverbose:
    plt.title("Step 4 - Power spectrum down converted S/C tone - Zoomed")
    sns.lineplot(x=fto, y=10 * np.log10(ssfp), color="blue")
    plt.xlim([BW3 / 4, 3 * BW3 / 4])
    plt.ylabel("Power (log scale)")
    plt.xlabel("Frequency [Hz]")
    plt.show()

# We filter everything above the output bandwidth BW3
ssff: float = ssf
bwo: int = np.where(fto == BW3)[0][0]
ssff[bwo:] = 0

# Return the filtered signal to time domain
sfc: float = np.fft.ifft(ssff)

# Measure the amplitude and phase of the signal in time-domain
# Phr: Spacecraft tone phase unwrapped
# dPhr: Linear trend free phase (it could be + or -)
sc_amp: float = np.abs(sfc)
sc_pha: float = np.angle(sfc)
Phr: float = np.unwrap(sc_pha)
dPhr: float = Phr - 2 * np.pi * Ftarg * tto

if fverbose:
    plt.title("Step 4 - Spacecraft tone amplitude and phase")
    plt.subplot(2, 1, 2)
    sns.lineplot(x=tto, y=sc_amp, color='red')
    plt.ylabel("Amplitude")
    plt.xlabel("Time")
    plt.subplot(2, 1, 2)
    sns.lineplot(x=tto, y=Phr, color='blue')
    plt.ylabel("Phase [rad]")
    plt.xlabel("Time")
    plt.show()

# Step 5. Clean the residual phase from noise
# We generate the phase polynomial fit (3rd fit correction).
# Defined as npp3
# rdPhr: residual of the linear trend free phase

wto: float = np.ones(Nto)
PhFit: float = core.PolyfitW(tto, dPhr, wto, npp3)
cf3: float = core.PolyfitWC(tto, dPhr, wto, npp3)

rdPhr: float = dPhr - PhFit

# Save to disk
if output:
    # Initialise the Coefficient variables
    cfs3: float = np.zeros(npp3)
    cpp3: float = np.zeros(npp3 + 1)
    cps3: float = np.zeros(npp3 + 1)

    # Convert to frequency polynomials per second
    for jp in np.arange(npp3):
        cfs3[jp] = cf3[jp] * np.power(Tspan, -jp)

    # Calculate the phase polynomials coefficients
    for jp in np.arange(1, npp3 + 1):
        cps3[jp] = 2 * np.pi * cfs3[jp - 1] / jp
        # SAMPLING_INTERVAL -> THIS SAMPLING INTERVAL SHOULD BE UPDATED TO 20 Hz
        cpp3[jp] = cps3[jp] * np.power(sampling_interval, jp)

    np.savetxt(cppname.replace(f'poly{npp2}.rev2', f'poly{npp3}.rev3'), cpp3)
    np.savetxt(cfsname.replace(f'X{npp2-1}cfs.rev2', f'X{npp3-1}cfs.rev3'), cfs3)

if fverbose or verbose:
    plt.suptitle("Step 5 - Summary of the PLL tone detection")
    plt.subplot(2, 1, 1)
    sns.lineplot(x=tto[5:-5], y=dPhr[5:-5], color='red')
    sns.lineplot(x=tto[5:-5], y=PhFit[5:-5], color='blue')
    plt.ylabel("phase [rad]")
    plt.subplot(2, 1, 2)
    sns.lineplot(x=tto[5:-5], y=rdPhr[5:-5], color='brown')
    plt.ylabel("phase [rad]")
    plt.xlabel("time [sec]")
    plt.show()

# b. We discard some initial values at the edges. We do a safe solution
#    to use +15 and -15 offset
#    phase file  t0 is +15 offset
#               t1 is t_scan -15 offset
# Store the phase data in Phases.S/C.YYYY.MM.DD.ST.txt format

bmin: int = np.where(tto == 15)[0][0]
bmax: int = np.where(tto == t_scan - 15)[0][0]

print(f"Phase rms: {rdPhr[bmin:bmax].std() * 1e3:.2f} mrads \033[0m")

# Store the phase into disk
phase = np.array([tto[bmin:bmax], rdPhr[bmin:bmax]])
amp = np.array([tto, sc_amp])

scan_start = Time(Tsinfo[0], format='mjd', scale='utc') + TimeDelta(Tsinfo[1] + t_add, format='sec')

if output:
    # If we enable verbose output we can store a the amplitude values for scintillation
    amp_fn: str = f"{path}Amplitude.{spacecraft}{epoch}.{station}.{scan_number:04d}.txt"
    amp_header: str = f'Residual phases sesion on {epoch} at {station}\n' + \
                    f'{scan_start.isot} | SNR {10 * np.log10(snr.mean()):.2f} dB |' + \
                    f' Doppler noise {1e3 * rFdet.std():.3f} mHz\n' + \
                    'Timestamp [s]         |     Amplitude [dB]      |\n'

    np.savetxt(amp_fn, amp.transpose(), delimiter=' ', header=amp_header)

phase_fn: str = f"{path}Phases.{spacecraft}{epoch}.{station}.{scan_number:04d}.txt"
phase_header: str = f'Residual phases sesion on {epoch} at {station}\n' + \
                    f'{scan_start.isot} | SNR {10 * np.log10(snr.mean()):.2f} dB |' + \
                    f' Doppler noise {1e3 * rFdet.std():.3f} mHz\n' + \
                    'Timestamp [s]         |     Phase [rad]         |\n'

np.savetxt(phase_fn, phase.transpose(), fmt='%.18e %+.18e', delimiter=' ', header=phase_header)

# c.  Very quick and dirty estimate of the phase scintillation
#     It gives an overlook of the quality of the data

bss: int = int(0.003 / df + 1.5)
bse: int = int(3.003 / df + 1.5)
lph = np.size(phase[1, :])
wcos = signal.windows.cosine(lph)
sp = np.fft.fft(phase[1, :])
spw = np.fft.fft(np.multiply(phase[1, :], wcos))
pspw = np.power(np.abs(spw), 2)
pspw = 2 * pspw / BW3
dta: float = phase[0, 1] - phase[0, 0]
Tsa: float = dta * lph
dfa: float = 1 / Tsa
ff = np.arange(0, BW2 * 2 - dfa, dfa)

FiltScint = np.zeros(lph)
FiltScint[bss:bse] = 2

spsc = np.zeros((np.size(phase[1, :])), dtype=np.complex64)
spsc = np.multiply(sp, FiltScint)
phs = np.real(np.fft.ifft(spsc, axis=0))
rmsphs = np.std(phs, axis=0)

if fverbose:
    print(f"Phase scintillation RMS: {rmsphs}")
    plt.loglog(ff, pspw)
    plt.title('Step 3: Quick estimation of the fluctuations density power')
    plt.vlines(0.003,1,1e12,colors='red')
    plt.vlines(3.003,1,1e12,colors='red')
    plt.xlim([1e-4, 10])
    #plt.ylim([1e-6, 10*np.max(pspw)])
    plt.show()
    #plt.vlines

# Step 6. Remove the 3rd Phase Polynomial Fit from the signal
# We correct the downconverted signal with the linear trend
# free phase

sfcc: float = sfc * np.exp(-1j * PhFit)
ssf: float = np.fft.fft(sfcc)
ssfp: float = np.power(np.abs(ssf), 2)
ssfp /= ssfp.max()

rmsf = core.GetRMSf(ssfp, fto, Ftarg, 0.4 * BW3, 0.1 * BW3)
snr = np.divide(1 - rmsf[0], rmsf[1])

if fverbose:
    plt.title("Step 6 - Power spectrum downconverted with linear-trend free phase")
    plt.semilogy(fto, ssfp, color='deepskyblue', label='s/c carrier', marker='o')
    plt.legend()
    plt.xlim([0, BW3])
    plt.ylim([1e-11, 1])
    plt.ylabel("Power received [log]")
    plt.xlabel("Frequency [Hz]")
    plt.grid()
    plt.show()

# If output is selected store to a file
if output:
    # out_filename contains the base filename
    out_filename: str = f'{path}{filename[0 : fil_lng + 6]}'

    # We can use 4 header lines as usually
    header_lines: str = '#\n#\n# Frequency     |    Spectral power (normalised to 1)\n#'

    # First we output a text file with 2 columns: ff and Spectra
    np.savetxt(f'{out_filename}_20Hz.spec', np.transpose([fto, ssfp]),
               newline="\n", header=header_lines)

    # Second we ouptut a binary file with the corrected 20 Hz tone signal
    sfcc.tofile(f'{out_filename}_20Hz.bin', sep="", format='float64')

    # Reconstruct the full 'sky' phase
    # we add all the subtracted phases in all different steps

    # Residual + Constant frequency terms + polynomial frequency lock
    freq_comp1 = 2 * np.pi * freq_rotation * tto
    freq_comp2 = 2 * np.pi * Fstartc * tto
    freq_comp3 = 2 * np.pi * Ftarg * tto

    res = []
    for jp in np.arange(2, npp2 + 1):
        res.append(cpp2[jp] * np.power(tto, jp))

    PhDopp = np.sum(np.array(res), axis=0)
    full_phase = -rdPhr + freq_comp1 + freq_comp2 + freq_comp3 + PhDopp

# Step 7. Ultra phase stop
# Filter again the data to a narrower band defined as BW4. By default I use
# 5 or 1 Hz, depending on the application

#Ftarg2: float = np.floor(0.5 * BW4) ### WHY? INTEGER 2 instead in the middle???
Ftarg2: float = BW4 / 2
fmax2: float = dfto * core.FindMax(ssfp, fto)[1]
freq_rotation2: float = fmax2 - Ftarg2

# make phase rotation to lower frequencies - store freq_rotation2
sfcn = sfc * np.exp(-1j * 2 * np.pi * freq_rotation2 * tto)

# Convert to frequency domain and filter
nsfc = np.fft.fft(sfcn)

# High pass filter above BW4 Hz
bwnh: int = np.where(fto >= BW4)[0][0]
nsfc[bwnh:] = 0
#nsfc = nsfc[0: bwnh + 1]

if fverbose:
    plt.title(f"Step 6 - Check that narrow filter has worked {BW4} Hz")
    plt.semilogy(fto, np.abs(nsfc), 'c')
    plt.xlim([0, BW4])
    plt.ylabel("Power (log scale)")
    plt.xlabel("Frequency [Hz]")
    plt.show()

# Get the signal back in time
nfc = np.fft.ifft(nsfc)

# Get the phase and unwrap it
nsc_pha = np.angle(nfc)
nPhr = np.unwrap(nsc_pha)

dnPhr = nPhr - 2 * np.pi * Ftarg2 * tto
rdnPhr = dnPhr - PhFit

# correct offset
#rdPhr = rdPhr - np.floor(np.mean(rdPhr[1000:2000]) / np.pi) * np.pi

print("\n\033[0m# Narrow band")
#print(f"\033[94mDownconverted Freq RMS: {rmsf} \033[0m")
print(f"\033[94mDownconverted Freq SNR: {np.mean(snr)} \033[0m")
print(f"Ultra Phase Stop rms: {1e3 * rdPhr.std():.2f} mrad")

if fverbose:
    plt.suptitle("Step 7 - Phase detection in ultra narrow band")
    plt.subplot(2, 1, 1)
    sns.lineplot(x=tto, y=dnPhr, color='blue')
    plt.ylabel('Rd phase offset')
    plt.xlim([0, tto.max()])
    plt.xticks([], [])
    plt.subplot(2, 1, 2)
    sns.lineplot(x=tto, y=rdnPhr, color='navy')
    plt.ylabel("Residual [rad]")
    plt.xlabel("time [sec]")
    plt.xlim([0, tto.max()])
    plt.show()

# If Doppler3 variable is set, then we store the fdets3 in a file:
#    tto: same time sampling as residual phase
#    fdets3: recondstructed fdets at high-resolution'
#    dFr: residual Dopplers

if dopp3:
    # Initialise first the variables - Only works for ORDER 1
    ORDER: int = 1
    int_time: int = 1

    phase_sampling: float = tto[1] - tto[0]
    num_samples = len(rdPhr)
    phase_span = phase_sampling * num_samples
    phase_interval = np.linspace(start=0, stop=phase_span, num=num_samples)
    freq_interval = np.linspace(start=0, stop=phase_span, num=int(num_samples * phase_sampling / int_time))

    num_freq_samples = len(freq_interval)

    freq_model: float = np.zeros(num_freq_samples - 1)
    diff_freq: float = np.zeros(num_freq_samples - 1)

    # Built the reconstructed topocentric frequency model
    for kp in np.arange(start=0, stop=num_freq_samples - 1):
        for jp in np.arange(start=1, stop=npp1):
            freq_model[kp] += cfs1[jp] * np.power(freq_interval[kp], jp) + cfs2[jp] * np.power(freq_interval[kp], jp)

    # Interpolate over different Finite_difference_coefficient (see generate_fdets3)
    for ip in np.arange(start=0, stop=num_freq_samples - 1):
        x0 = int(np.floor(int_time * (ip + 0.5) / phase_sampling))
        x1 = int(np.floor(int_time * (ip + 1.5) / phase_sampling))
        x2 = int(np.floor(int_time * (ip + 2.5) / phase_sampling))
        x3 = int(np.floor(int_time * (ip + 3.5) / phase_sampling))

        match ORDER:
            case 1:
                diff_freq[ip] = (-rdnPhr[x0] + rdnPhr[x1]) / (2 * np.pi * int_time)
            case 2:
                diff_freq[ip] = (-1.5 * rdnPhr[x0] + 2 * rdnPhr[x1] - 0.5 * rdnPhr[x2]) / (2 * np.pi * int_time)
            case 3:
                diff_freq[ip] = (-11/6 *rdnPhr[x0] + 3 * rdnPhr[x1] - 1.5 * rdnPhr[x2] + 1/3 * rdnPhr[x3]) / (2 * np.pi * int_time)

    freq_dets3 = StartF + cfs2[0] + freq_model + diff_freq #- freq_rotation2 - freq_rotation

    # needed here because we have not done the ultra phase stop
    # Ftarg2 = (BW4 - BW3)/2

    # freq_dets3 = StartF + cfs2[0] + Ffirst + dFr - Ftarg2

    print("\n\033[0m# Doppler revision 3")
    print(f'\033[94mStartF: {StartF} Hz')
    print(f'cfs2: {cfs2[0]} Hz')
    print(f'F first : {freq_model[0]} Hz')
    print(f'Frot and Frot2: {freq_rotation} : {freq_rotation2}')
    print(f'dFr: {diff_freq[0]} Hz')
    print(f'Frequencies: {freq_dets3[0]}')
    print('\033[0m')
    fdets_fn = fdets_fn.replace('r2i', 'r3i')
    fdets_header: str = (
        f'Observation conducted on {epoch} at {station} rev. 3\n'
        f'Base frequency: {baseband_freq:.2f} MHz dF: {df} Hz dT:'
        f' {tto[1] - tto[0]} s Nscans: 1\nFormat: UTC Time [s] '
        '|   Signal-to-Noise  |    Spectral max    '
        '| Freq detection [Hz] |   Doppler noise [Hz]  |\n'
    )


    # We loose the last sample
    length_fdets3 = num_freq_samples - 1

    # Length of the data frame will depend on the ORDER selected
    data = {'timestamp': Time(scan_start + TimeDelta(freq_interval[:-1], format='sec'), format='mjd').isot,
        'SNR': np.full(length_fdets3, snr),
        'Spectral Max': np.full(length_fdets3, Smax.mean()),
        'Doppler': freq_dets3,
        'Residual fit': diff_freq}

    np.savetxt(fdets_fn, pd.DataFrame(data=data), delimiter=' ', \
               fmt='%15s %.18e %.18e %.12f %+.16e', header=fdets_header)
    if verbose:
        plt.plot(StartT + t_add + freq_interval[:-1], diff_freq, 'b')
        plt.title('Step 4:')
        plt.show()

    print(f"\033[94mDoppler rev. 3 rms: {1e3 * diff_freq.std()} mHz\033[0m")

print(f"Total running time: {(Time.now() - start).sec:.2f} s")
