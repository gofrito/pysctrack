#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 27 10:49:40 2020

calculate the TECS vealues for the ionosphere and troposphere
@author: gofrito
         oasis
"""
import argparse
import datetime

import numpy as np
import pandas as pd

# import vintlib as vl


def calctec(scan):
    # scan line with all measurement values
    print(f'To implement')


# create parser
parser = argparse.ArgumentParser()
# optional arguments
parser.add_argument('-s', '--spacecraft', type=str, choices=['vex', 'mex'], help='spacecraft')
parser.add_argument('-u', '--uppoint', type=str, choices=['sc', 'planet'], default='planet', help='where to point when calculating TEC on uplink:' + ' \'planet\' to point at the S/C host planet (default),' + ' \'sc\' to point at the S/C')
parser.add_argument('-i', '--ionomodel', type=str, choices=['igs', 'igr'], default='igr', help='IGS\' ionospheric TEC model to use: final or rapid (default)')
parser.add_argument('-p', '--parallel', action='store_true', help='run computation in parallel mode')
# positional argument
parser.add_argument('inpFile', type=str, help="input ScintObsSummary table")

args = parser.parse_args()

scint_table_file: str = args.inpFile

# which spacecraft?
if args.spacecraft == 'vex':
    source: str = 'vex'
elif args.spacecraft == 'mex':
    source: str = 'mex'
else:
    # try to guess:
    if 'vex' in scint_table_file.lower():
        source: str = 'vex'
    elif 'mex' in scint_table_file.lower():
        source: str = 'mex'
    else:
        raise Exception('Spacecraft not set; failed to guess.')

# where to point uplink?
if args.uppoint == 'sc':
    tec_uplink: str = 'sc'
elif args.uppoint == 'planet':
    tec_uplink: str = 'planet'
else:
    # let's do it quickly by default
    tec_uplink: str = 'planet'

''' at the moment is hardcoded, shall i use inp.cfg?'''
iono_model = 'igs'

''' Parse Scint Table '''
tags = [
    'Observation',
    'Run',
    'Scan',
    'Station',
    'DOY',
    'Time',
    'Duration',
    'RAh',
    'RAm',
    'RAs',
    'DEd',
    'DEm',
    'DEs',
    'Azimuth',
    'Elevation',
    'Longitude',
    'Latitude',
    'Distance',
    'SOT',
    'STO',
    'Scint',
    'SysNoise',
    'SciSlope',
    'ErrSlope',
    'PeakSPD',
    'Noise',
    'Dnoise',
    'CarrierSNR',
    'SolarAct',
    'GroundStation',
    'DownIonos',
    'UpIonos',
    'TEC',
]
scint_table = pd.read_csv(scint_table_file, sep="\s+", header=None, skiprows=5, skipfooter=70, engine='python', names=tags)
# dates = pd.DataFrame()
dates = []
for scan in range(scint_table.shape[0]):
    yy, mm, dd = scint_table.DOY[scan].split('.')
    dates.append(datetime.datetime(int(yy), int(mm), int(dd)))
dates = np.unique(dates)

print(pd.DataFrame(data=dates))

print(f'Fetching ionospheric data...')
# tec_model = 'igs' # final 'igs' or rapid 'igr' IGS solution
tec_model = inp['iono_model']
for t in dates:
    vl.doup(False, inp['do_ion_calc'], inp['cat_eop'], inp['meteo_cat'], inp['ion_cat'], t, t, tec_model)

''' iterate over records in scint table '''
for scan in range(scint_table.shape[0]):
    print(scint_table.shape[0] - scan, 'records to go')
