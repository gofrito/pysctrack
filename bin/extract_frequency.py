#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

@author: gofrito
"""

import argparse
import os

import numpy as np


parser = argparse.ArgumentParser()
parser.add_argument(
    'filename', nargs='?', help='Time input file', default='check_string_for_empty'
)

args = parser.parse_args()
freqbin = args.filename

Fsinfo: str = np.loadtxt(freqbin, skiprows=1)

# epoch = converter.mjd2gd(Tsinfo[0])
# time = converter.sec2hmi(Tsinfo[1])

print(f'Frequency initial tone: {Fsinfo[3]} Hz')
print(f'Frequency final tone:   {Fsinfo[4]} Hz')
print(f'Doppler shift:              {Fsinfo[4] - Fsinfo[3]} Hz\n')

# Read the frequency detection from the polynomial coefficient
base_filename: str = freqbin.split('_')

xfsfile: str = f"{'_'.join(base_filename[:-1])}.X5cfs.txt"
xfs2file: str = f"{'_'.join(base_filename[:-1])}.X5cfs.rev2.txt"

if os.path.exists(xfsfile):
    'All fine'
elif os.path.exists(
    f"{'_'.join(base_filename[:4])}_3200000pt_5s_{base_filename[6]}.X5cfs.txt"
):
    xfsfile = f"{'_'.join(base_filename[:4])}_3200000pt_5s_{base_filename[6]}.X5cfs.txt"
elif os.path.exists(
    f"{'_'.join(base_filename[:4])}_3200000pt_2s_{base_filename[6]}.X5cfs.txt"
):
    xfsfile = f"{'_'.join(base_filename[:4])}_3200000pt_2s_{base_filename[6]}.X5cfs.txt"
elif os.path.exists(
    f"{'_'.join(base_filename[:4])}_3200000pt_1s_{base_filename[6]}.X5cfs.txt"
):
    xfsfile = f"{'_'.join(base_filename[:4])}_3200000pt_1s_{base_filename[6]}.X5cfs.txt"
elif os.path.exists(
    f"{'_'.join(base_filename[:4])}_6400000pt_2s_{base_filename[6]}.X5cfs.txt"
):
    xfsfile = f"{'_'.join(base_filename[:4])}_6400000pt_2s_{base_filename[6]}.X5cfs.txt"
elif os.path.exists(
    f"{'_'.join(base_filename[:4])}_6400000pt_5s_{base_filename[6]}.X5cfs.txt"
):
    xfsfile = f"{'_'.join(base_filename[:4])}_6400000pt_5s_{base_filename[6]}.X5cfs.txt"
elif os.path.exists(
    f"{'_'.join(base_filename[:4])}_6400000pt_1s_{base_filename[6]}.X5cfs.txt"
):
    xfsfile = f"{'_'.join(base_filename[:4])}_6400000pt_1s_{base_filename[6]}.X5cfs.txt"
else:
    print('error')

Freq: float = np.loadtxt(xfsfile)

print(f'Initial tone:           {Freq[0]:.1f} Hz')

Freq: float = np.loadtxt(xfs2file)
print(f'Frequency tone in PLL:      {Freq[0]:.1f} Hz')
