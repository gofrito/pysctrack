#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

@author: gofrito
"""

import argparse

import numpy as np

from pysctrack import converter

parser = argparse.ArgumentParser()
parser.add_argument(
    'filename', nargs='?', help='Time input file', default='check_string_for_empty'
)

args = parser.parse_args()
timebin = args.filename

Tsinfo: str = np.loadtxt(timebin, skiprows=1)

epoch = converter.mjd2gd(Tsinfo[0])
time = converter.sec2hmi(Tsinfo[1])

print(f'Day of the observation: {epoch[2]:02d}.{epoch[1]:02d}.{epoch[0]}')
print(f'Start time of the scan: {time[0]:02d}:{time[1]:02d}:{int(time[2]):02d}UT')
