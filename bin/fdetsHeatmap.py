#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 27 10:49:40 2020

Plot the heatmap given a full spectra
@author: gofrito
"""
import argparse
import errno
import os
import re

import numpy as np
import pandas as pd
import plotly.graph_objects as go

parser = argparse.ArgumentParser()
parser.add_argument("filename", nargs="?", help="input spectra", default="check_string_for_empty")
parser.add_argument("-bw", dest="bandwidth", help="observing bandwidth", type=float, default=32)
parser.add_argument("-zf", dest="zoom_frequency", help="enter the boundaries of the freq search window [MHz]", nargs="+", type=float, metavar="N", default=(4.7, 4.9))

args = parser.parse_args()

bandwidth: float = args.bandwidth
fmin, fmax = args.zoom_frequency

fn: str = args.filename

# Check if the file exists
if os.path.exists(fn):
    path, spectra_fn = os.path.split(fn)
    path = f"{os.path.abspath(path)}/"
    spectra_file = os.path.join(path, spectra_fn)
else:
    raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), fn)

# Use re package to observation-related parameters
match = re.search(r"^([^_]*)_([^_]*)_(?:([^_]*)_)?(?:N)?([oabcdefgh])?(\d+)_(\d+)(?:pt)?_(\d+)s?_(?:ch)?([^_]+)_(?:swspec|scspec)", spectra_fn, re.I)

# Extract all observing params from the filename
session: str = match.group(1)
station: str = match.group(2)
raw_format: str = match.group(3)
ifband: str = match.group(4)
scan_number: str = match.group(5)
FFTpt: int = int(match.group(6))
dts: int = int(match.group(7))
channel: int = int(match.group(8))

# Define other parameters
fsize: int = os.path.getsize(spectra_file)
Nspec: int = int(np.floor(0.5 * fsize / (FFTpt + 1)))
Nfft: int = int(FFTpt / 2 + 1)
df: int = int(bandwidth * 2e6 / FFTpt)
epoch: str = f'{session[5:7]}.{session[3:5]}.20{session[1:3]}'
bmin: int = int(fmin * 1e6 / df)
bmax: int = int(fmax * 1e6 / df)

# Let's limit the size of the file, before it is too large
Spec: float = np.zeros((Nspec, bmax - bmin))
Freq: float = np.zeros((Nspec, 1))

fd = open(f"{spectra_file}", "rb")

# calculate spectra
for ip in range(Nspec):
    read_data: float = np.fromfile(file=fd, dtype="float32", count=Nfft)
    Spec[ip, :] = read_data[bmin:bmax]
    Freq[ip] = ip * df

print(f'Number of spectra:    {np.shape(Spec)[0]}')
print(f'Frequency resolution: {df}')

# Let's plot the results
fig = go.Figure(data=go.Heatmap(z=Spec, colorscale='Electric'))
# fig = go.Figure(data=go.Heatmap(z=Spec, colorscale='Electric', dict(opacity=0.5)))

rng = int((fmax - fmin) * 1e6)

bin_ticks = range(0, int(rng / df), int(rng / (5 * df)))
freq_ticks = range(0, rng, int(rng / 5))

print(np.array(bin_ticks))
print(np.array(freq_ticks))
fig.update_layout(
    title=f'topocentric frequency detections {station} on {epoch}',
    # title = f'Doppler detections of Mars 2020 Perseverance during its arrival to Mars on 18.02.2021 as seen from Ceduna radio telescope',
    xaxis=dict(
        title_text='S/C signal in narrow bandwith [Hz]',
        tickmode='array',
        #            tickvals = [0,100,200,300,400],
        #            ticktext = [0,500,1000,1500,2000],),
        tickvals=np.array(bin_ticks),
        ticktext=np.array(freq_ticks),
    ),
    yaxis=dict(title_text='Time [s]'),
    # tickmode = 'array',
    # tickvals = [0,20,40,60,80,100,120,140],
    # ticktext = [0,100,200,300,400,500,600,700]),
)

# Add images
'''
fig.add_layout_image(
        dict(
            source="https://images.plot.ly/language-icons/api-home/python-logo.png",
            xref="x",
            yref="y",
            x=0,
            y=3,
            sizex=2,
            sizey=2,
            sizing="stretch",
            opacity=0.5,
            layer="below")
)
'''
fig.show()
