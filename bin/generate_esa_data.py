#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 6 20:41:24 2024

generate_esa_data.py

Prepare data for ESA data format and generate lblx files

v0.1 Create script
@author: gofrito
"""
import os
import shutil

from colorama import Fore
from pysctrack import esa_lblx, __version__

print(f'[INFO]    Creating folder structure         {Fore.GREEN}[OK]{Fore.BLACK}')
if not os.path.exists('./data'):
    os.makedirs('./data')
if not os.path.exists('./dop0'):
    os.makedirs('./dop0')
if not os.path.exists('./dop2'):
    os.makedirs('./dop2')

print(f'[INFO]    Remove unnecessary binary files   {Fore.GREEN}[OK]{Fore.BLACK}')
spec = [f for f in os.listdir() if 'spec.bin' in f]
for file in spec:
    os.remove(f'./{file}')

print(f'[INFO]    Move data into folders            {Fore.GREEN}[OK]{Fore.BLACK}')
phases = [f for f in os.listdir() if 'Phases.' in f]
for file in phases:
    shutil.move(file, f'./dop2/{file}')

fdets = [f for f in os.listdir() if 'r2i.txt' in f]
for file in fdets:
    shutil.move(file, f'./dop2/{file}')

fdets = [f for f in os.listdir() if 'r0i.txt' in f]
for file in fdets:
    shutil.move(file, f'./dop0/{file}')

ini = [f for f in os.listdir() if 'inifile' in f]
for file in ini:
    shutil.move(file, f'./data/{file}')

ini = [f for f in os.listdir() if 'inifile' in f]
for file in ini:
    shutil.move(file, f'./data/{file}')

data = [f for f in os.listdir() if 'tone0.bin' in f]
if len(data) > 0 :
    #if os.path.exists(data[0]):
    session = data[0].split('_')[0]

    data = [f for f in os.listdir() if session in f]
    for file in data:
        shutil.move(file, f'./data/{file}')

print(f'[INFO]    Generating Labels in Doppler 0    {Fore.GREEN}[OK]{Fore.BLACK}')
os.chdir('./dop0/')
files = [f for f in os.listdir()]

for file in files:
    if os.path.splitext(file)[-1]=='.txt':
        description = 'Fdets file'
        type = 'fdets'
        fg = esa_lblx.xmlClass(file, __version__, description)
        fg.generate_xml(type)

os.chdir('./../')

print(f'[INFO]    Generating Labels in Doppler 2    {Fore.GREEN}[OK]{Fore.BLACK}')
os.chdir('./dop2/')
files = [f for f in os.listdir()]
for file in files:
    #print(file.endswith('txt') and file.startswith('Fdets'))

    if file.startswith('Fdets') and file.endswith('txt'):
        description = 'Fdets file'
        type = 'fdets'
        fg = esa_lblx.xmlClass(file, __version__, description)
        fg.generate_xml(type)
    elif file.startswith('Phase') and file.endswith('txt'):
        description = 'Phase file'
        type = 'phase'
        fg = esa_lblx.xmlClass(file, __version__, description)
        fg.generate_xml(type)
    else:
        description = 'unknown'

os.chdir('./../')

print(f'[INFO]    Generating Labels in Doppler 3    {Fore.GREEN}[OK]{Fore.BLACK}')

print(f'[INFO]    Actions Completed                 {Fore.GREEN}[OK]{Fore.BLACK}')