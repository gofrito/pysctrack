#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat My 14 07:49:56 2020

CheckSpectra.py plots the spectra of a VLBI observation
Settings are  placed by default for new VGOS system: 32e6 Hz bandwidth

v0.1 original version from pre-repo
@author: gofrito
"""

import argparse
import errno
import os
import re
import time

import numpy as np


# take time
Tstart: float = time.process_time()

# find the directory where pysctrack and catalogues are installed
PYSCTRACK_DIR: str = os.path.dirname(os.path.realpath(__file__))[:-3]
PYSCTRACK_CAT: str = os.path.join(PYSCTRACK_DIR, 'cat')


class hunt_tones:
    def __init__(self):
        parser = argparse.ArgumentParser()
        parser.add_argument(
            '-p',
            '--plot',
            help='plot S/C signal with the aid of a menu',
            action='store_true',
            default=False,
        )
        parser.add_argument(
            '-sf',
            '--skyfrequency',
            help='change  labels to show results in sky frequency',
            action='store_true',
            default=False,
        )
        parser.add_argument(
            '-bw', dest='bandwidth', help='observing bandwidth', type=float, default=32
        )
        parser.add_argument(
            '-ep',
            dest='epoch',
            help='specify your own epoch - format ex. 2020.02.02',
            type=str,
        )
        parser.add_argument(
            '-rf',
            dest='radio_frequency',
            help='initial frequency band of the channel',
            type=float,
            default=8400,
        )
        parser.add_argument(
            '-t0',
            dest='initialtime',
            help='initial time within the scan',
            type=int,
            default=0,
        )
        parser.add_argument(
            '-t1', dest='endtime', help='end time within the scan', type=int, default=0
        )
        parser.add_argument('--version', action='version', version='%(prog)s 0.1')
        parser.add_argument(
            'filename',
            nargs='?',
            help='spectra input file',
            default='check_string_for_empty',
        )
        args = parser.parse_args()

        self.filename: str = args.filename
        self.bandwidth: float = args.bandwidth

        # Check if the file exists
        if os.path.exists(args.filename):
            path, self.filename = os.path.split(args.filename)
            self.path = f'{os.path.abspath(path)}/'
            print(self.path)
        else:
            raise FileNotFoundError(
                errno.ENOENT, os.strerror(errno.ENOENT), args.filename
            )

        match = re.search(
            r'^([^_]*)_([^_]*)_(?:([^_]*)_)?(?:N)?([oabcdefgh])?(\d+)_(\d+)(?:pt)?_(\d+)s?_(?:ch)?([^_]+)_(?:swspec|scspec)',
            self.filename,
            re.I,
        )

        # Extract all observing params from the filename
        self.session: str = match.group(1)
        self.station: str = match.group(2)
        self.raw_format: str = match.group(3)
        self.ifband: str = match.group(4)
        self.scan_number: int = int(match.group(5))
        self.FFTpt: int = int(match.group(6))
        self.dts: int = int(match.group(7))
        self.channel: str = match.group(8)

        fsize: int = os.path.getsize(f'{self.path}{self.filename}')
        self.Nspec: int = int(np.floor(0.5 * fsize / (self.FFTpt + 1)))

    def hunt(self):
        Nfft: int = int(self.FFTpt / 2 + 1)

        # Read file
        fd = open(f'{self.path}{self.filename}', 'rb')

        # Read the data and prepare the matrix
        for ip in np.arange(self.Nspec):
            np.fromfile(file=fd, dtype='float32', count=Nfft)


hunt_tones()
