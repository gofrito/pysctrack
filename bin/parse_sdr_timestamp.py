#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Mon May 25 20:41:24 2023

Extract time from the SDR recorded file into
start timing file from swspec
v0.1 - Parse SDR timing
@author: gofrito
"""

import sys
import astropy.time as time
import argparse

help_description = """\
 Populate the starttiming.txt file with the
 timestamp from the Ettus in the file name.

 > parse_sdr_timestamp.py input_runlog.txt"""

# We parse the runlog file from swspec: it contains the basefilename and
# the original raw data file with timestamps
parser = argparse.ArgumentParser(
    formatter_class=argparse.RawDescriptionHelpFormatter, description=help_description
)

parser.add_argument("filename", nargs="?", help="Parse swspec runlog file")
parser.add_argument("--version", action="version", version="%(prog)s 0.1")

args = parser.parse_args()

# Pass input file
runlog = args.filename

# open Runlog and retrieve
fd = open(runlog)

# Read everything and assume regular output
log_file = fd.readlines()

print(log_file[1].split(" ")[0])

# Usually 2nd line contains filename information
if log_file[1].split(" ")[0] == "Input":
    input_file = log_file[1].split(" ")[-1]
else:
    # Sometimes the second line contains SkipSeconds
    input_file = log_file[2].split(" ")[-1]

print(f"The raw input file is: {input_file}")

# Make a simple string split
yy = input_file.split("-")[1]
mm = input_file.split("-")[2]
dd = input_file.split("-")[3]
HH = input_file.split("-")[4]
MM = input_file.split("-")[5]
SS = input_file.split("-")[6].split("_")[0]

# Convert to astropy time
t = time.Time(f"{yy}-{mm}-{dd}T{HH}:{MM}:{SS}", format="isot", scale="utc")

# We need mjd and seconds of the day
mjd = int(t.mjd)
sec = int(86400 * (t.mjd - mjd))
fd.close()

startlog = runlog.replace("_runlog.txt", "_starttiming.txt")

# Overwrite the start timing filee
fd = open(startlog, "w")
fd.write("// USRP Ettus start time <MJD sec ns>\n")
fd.write(f"{mjd} {sec} 0.000000")
fd.close()
