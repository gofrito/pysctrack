#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 25 20:41:24 2020

Core of the digital Phased-Lock-Loop. Run a sctracker
version with a narrow-band tone and extract the residual
phase.
v0.1 - simplify PLL for the analysis of Phase Cal tones
@author: gofrito
"""

import argparse
import errno
import os
import re
import time

import matplotlib.pyplot as plt
import numpy as np
import scipy.signal as sps
import scipy.stats as stats

from pysctrack import core, handler


class function_PLL:
    def __init__(self):
        # List of parsed parameters
        parser = argparse.ArgumentParser()
        parser.add_argument("filename", nargs="?", help="Input spectra file")
        parser.add_argument("-p", "--plot", help="Plot a summary of the results on screen", action="count", default=False)
        parser.add_argument("-pc", "--powercentre", help="Do not use an neighbour average to estimate Frequency Max", action="store_true", default=False)
        parser.add_argument("-dp", "--doppler", help="Generate Doppler rev. 3", action="store_true", default=False)
        parser.add_argument("-p1", dest="cpp1", help="First Polynomial", type=int, default=2)
        parser.add_argument("-p2", dest="cpp2", help="Second Polynomial", type=int, default=3)
        parser.add_argument("-p3", dest="cpp3", help="Third Polynomial", type=int, default=6)
        parser.add_argument("-t0", dest="starttime", help="Start time", type=float, default=0)
        parser.add_argument("-t1", dest="endtime", help="End time", type=float, default=0)
        parser.add_argument("-nf", dest="nfft", help="Number FFT points", type=int, default=20000)
        parser.add_argument("-ch", dest="channel", help="Number of channel", type=int, default=1)
        parser.add_argument("-ft", dest="forcetime", help="Force integration time if swspec and sctracker use different", type=int)
        parser.add_argument("-ff", dest="forcefft", help="Force number of FFT if swspec and sctracker use different", type=int)
        parser.add_argument("-rf", dest="radiofrequency", help="Starting Radio Frequency", type=float, default=0)
        parser.add_argument("-ep", dest="epoch", help="specify your own epoch - format ex. 2020.02.02", type=str)
        parser.add_argument("-bw2", dest="bandwidthStep2", help="Bandwidth used with 2nd fit", type=float, default=100)
        parser.add_argument("-bw3", dest="bandwidthStep3", help="Bandwidth used with 3rd fit", type=float, default=20)
        parser.add_argument("-ovl", dest="overlap", help="FFT overlapping factor", type=float, default=2)
        parser.add_argument("-nav", dest="nav", help="Number of averaged FFT", type=float, default=2)
        parser.add_argument("-pad", dest="padding", help="Padding factor", type=float, default=2)

        args = parser.parse_args()

        self.powcen: bool = args.powercentre
        self.dopp3: bool = args.doppler
        self.Tskip: int = args.starttime
        self.Nfft: int = args.nfft
        self.channel = args.channel

        if args.forcetime:
            self.ft: int = args.forcetime
        else:
            self.ft: bool = False

        if args.forcefft:
            self.fFFT: int = args.forcefft
        else:
            self.fFFT: bool = False

        self.BW: int = 2e3
        self.BW2: int = args.bandwidthStep2
        self.BW3: int = args.bandwidthStep3
        self.Npp1: int = args.cpp1
        self.Npp2: int = args.cpp2
        self.Npp3: int = args.cpp3
        self.Ovlp: int = args.overlap
        self.Nav: int = args.nav
        self.Padd: int = args.padding
        self.Fstart: float = args.radiofrequency

        self.verbose: bool = False
        self.fverbose: bool = False
        if args.plot == 1:
            self.verbose: bool = True
        elif args.plot > 1:
            self.fverbose: bool = True

        # Check if the file exists
        if os.path.exists(args.filename):
            path, self.filename = os.path.split(args.filename)
            self.path = f"{os.path.abspath(path)}/"
            self.fil_lng = len(self.filename) - 10
        else:
            raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), args.filename)

        match = re.search(r"^([^_]*)_([^_]*)_(?:([^_]*))_([^_]*)_(\d+)(?:pt)?_(\d+)s?_(?:ch)?([^_]+)_tone", self.filename, re.I)

        self.session: str = match.group(1)
        self.station: str = match.group(2)
        self.raw_format: str = match.group(3)
        self.scan_number: str = match.group(4)[2:6]
        self.scanMode: str = match.group(4)[0:2]
        self.FFT: int = match.group(5)
        self.ints: int = match.group(6)
        self.channel: int = match.group(7)

        # Estimate the number of samples in the file
        fsize = os.path.getsize(self.filename)

        if args.endtime:
            self.Tspan: float = args.endtime
            Nt: int = self.Tspan * 2 * 8 * self.BW
        else:
            self.Tspan: float = fsize / (2 * 8 * self.BW)
            Nt: int = fsize / 8

        print(f"Length of the file : {self.Tspan}")
        print(f"Size of the file : {Nt}")

        """
        Change 30 May 2020: it was divided by 10 assuming that 2 kHz/20000 Nfft. Now :
        dts = Nfft/BW
        """
        dts: float = self.Nfft / self.BW
        self.Tspan: float = np.floor(self.Tspan / dts) * dts

        # fill the epoch variable
        if args.epoch:
            self.epoch: str = args.epoch
        else:
            self.epoch: str = f"20{self.session[1:3]}.{self.session[3:5]}.{self.session[5:7]}"

        # Assign to variable cal
        self.spacecraft: str = 'cal'

    def set_variables(self):
        # ==============================================================================
        # Nfft:  Number of FFT points
        # Tskip: Time to skip into the function PLL
        # Tspan: Ending time
        # ==============================================================================
        self.Npf1: int = self.Npp1 - 1
        self.Npf2: int = self.Npp2 - 1

        log_file = f"{self.session}_{self.station}_{self.raw_format}_{self.scanMode}{self.scan_number}_{self.FFT}pt_{self.ints}s_ch{self.channel}"

        # Generic case unless overwritten from command line
        pol_file = log_file

        # we force the integration time
        if self.ft:
            pol_file = f"{self.session}_{self.station}_{self.raw_format}_{self.scanMode}{self.scan_number}_{self.FFT}pt_{self.ft}s_ch{self.channel}"

        # we force the FFT
        if self.fFFT:
            pol_file = f"{self.session}_{self.station}_{self.raw_format}_{self.scanMode}{self.scan_number}_{self.fFFT}pt_{self.ints}s_ch{self.channel}"

        # we force the FFT and the integration time
        if self.fFFT and self.ft:
            # We force both
            pol_file = f"{self.session}_{self.station}_{self.raw_format}_{self.scanMode}{self.scan_number}_{self.fFFT}pt_{self.ft}s_ch{self.channel}"

        self.tonebin = f"{self.path}{log_file}_tonebinning.txt"
        self.timebin = f"{self.path}{log_file}_starttiming.txt"
        self.filecpp = f"{self.path}{pol_file}.poly{self.Npp1}.txt"
        self.filecfs = f"{self.path}{pol_file}.X{self.Npf1}cfs.txt"

        if os.path.isfile(self.filecpp) == False:
            print(f"\033[91 mCPP file not found : {self.filecpp}\033[0m")
            raise FileNotFoundError

    def read_swspec_files(self):
        # Read Tone and Start time to know the beginning of the raw data file
        Tbinfo = np.loadtxt(self.tonebin)
        Tsinfo = np.loadtxt(self.timebin, skiprows=1)

        # We will use the To for the recording start and the initial tone bin
        self.StartF = Tbinfo[3]
        self.StartT = Tsinfo[1]
        self.MJD = Tsinfo[0]

        # Read the Phase Polynomial Coefficients iteration 1
        self.Cpp1 = np.loadtxt(self.filecpp)
        self.Cfs1 = np.loadtxt(self.filecfs)

    def data_processing(self):
        start: float = time.process_time()
        SR: int = 2 * self.BW  # Sampling Rate
        Nt: int = int(self.Tspan * SR)  # Total number of samples

        dt: float = 1 / SR  # Sampling interval
        df: float = SR / self.Nfft
        jt = np.arange(Nt)
        tw = dt * self.Nfft
        tt = jt * dt

        Nspec: int = int(Nt / (self.Nfft * self.Nav))
        sk: int = int(self.Tskip * df / 2)
        Nspek: int = Nspec - sk
        jspek = np.linspace(sk, Nspec - 1, Nspek)
        Bav: int = self.Nfft * self.Nav
        tspek: float = (jspek + 0.5) * Bav * dt
        Npadd: int = int(self.Nfft * (self.Padd - 1))
        dpadd = np.zeros(Npadd)

        print(f"Skip  # of spectra: {sk}")
        print(f"Total # of spectra: {Nspec}")

        if self.Padd:
            Npadd = 1

        jps = np.arange(self.Nfft)
        Win = np.power(np.cos(np.pi / self.Nfft * (jps - 0.5 * self.Nfft + 0.5)), 2)
        Nfp = self.Nfft * self.Padd / 2 + 1
        jfs = np.arange(Nfp)
        dfs = 1 / (tw * self.Padd)
        ffs = jfs * dfs

        # make_spec is the core function that reads the tone and outputs the spectra
        Sp = core.make_spec(f"{self.path}{self.filename}", Nspec, self.Nfft, self.Nav, self.Ovlp, Win, self.Padd, dpadd)
        xSp: float = np.mean(Sp)
        Sp = np.divide(Sp, xSp)
        Spa = np.divide(Sp.sum(axis=0), Nspec)

        if self.fverbose:
            plt.title(f"Step 1 - Last integration averaged spectra")
            plt.plot(ffs, np.log10(Spa))
            plt.ylabel("Spectrum")
            plt.xlabel("Frequency [Hz]")
            plt.show()

        if self.fverbose:
            plt.title(f"Step 1 - Zoom to the spectra")
            plt.semilogy(ffs, Spa, marker=".", linestyle=None)
            plt.ylabel("Spectrum")
            plt.xlabel("Frequency [Hz]")
            plt.xlim([950, 1050])
            plt.show()

        half_window: int = 40
        line_avoidance: int = 10

        xf = np.zeros((Nspek, 3))
        rmsd = np.zeros((Nspek, 3))

        dxc = np.zeros(Nspek)
        SNR = np.zeros(Nspek)
        Smax = np.zeros(Nspek)
        Fdet = np.zeros(Nspek)
        MJD = np.zeros(Nspek)
        Weight = np.ones((Nspek, 1))

        for ip in np.arange(sk, Nspec):
            jp = ip - sk
            xf[jp] = core.FindMax(Sp[ip], ffs)
            Smax[jp] = xf[jp, 2]
            Fdet[jp] = dfs * xf[jp, 1] + ffs[0]
            if self.powcen == 1:
                dxc = core.PowCenter(Sp[ip], xf[jp, 1], 3) * dfs
                Fdet[jp] = Fdet[jp] + np.transpose(dxc)
            rmsd[jp] = core.GetRMS(Sp[ip], xf[jp, 1], half_window / dfs, line_avoidance / dfs)
            SNR[jp] = (xf[jp, 2] - rmsd[jp, 0]) / rmsd[jp, 1]

        mSNR: float = SNR.mean()
        mFdet: float = Fdet.mean()

        MJD[0:Nspek]: int = self.MJD

        Weight = np.power(SNR, 2) / mSNR

        Ffit = core.PolyfitW1(tspek, Fdet, Weight, self.Npf2)
        rFdet = Fdet - Ffit
        Cf2 = core.PolyfitW1C(tspek, Fdet, Weight, self.Npf2)

        if self.verbose:
            plt.suptitle(f"Step 2 - Frequency Fit")
            plt.subplot(4, 1, 1)
            plt.plot(ffs, np.log10(Spa))
            plt.ylabel("Spectrum")
            plt.xlabel("Frequency [Hz]")
            plt.subplot(4, 1, 2)
            plt.plot(tspek, SNR, "rx")
            plt.ylabel("SNR")
            plt.subplot(4, 1, 3)
            plt.plot(tspek, Fdet, "r.")
            plt.plot(tspek, Ffit, "b")
            plt.ylabel("Freq [Hz]")
            plt.subplot(4, 1, 4)
            plt.plot(tspek, rFdet, "k.")
            plt.xlabel("Time [s]")
            plt.ylabel("Residual Freq [Hz]")
            plt.show()

        if self.fverbose:
            plt.suptitle(f"Step 2 - Frequency Fit")
            plt.subplot(3, 1, 1)
            plt.plot(tspek, SNR, "rx")
            plt.xlabel("Time [s]")
            plt.ylabel("SNR")
            plt.subplot(3, 1, 2)
            plt.plot(tspek, Fdet, "r.")
            plt.plot(tspek, Ffit, "b")
            plt.xlabel("Time [s]")
            plt.ylabel("Freq. [Hz]")
            plt.subplot(3, 1, 3)
            plt.plot(tspek, rFdet, "k.")
            plt.xlabel("Time [s]")
            plt.ylabel("Residual Freq [Hz]")
            plt.xlim([0, 1140])
            plt.show()

        if self.fverbose:
            plt.title(f"Step 2 - Frequency Fit Residuals")
            n, bins, patches = plt.hist(rFdet, density=1)
            (mu, sigma) = stats.norm.fit(rFdet)
            y = stats.norm.pdf(bins, mu, sigma)
            plt.plot(bins, y, "r--")
            plt.xlabel("Residual Freq [Hz]")
            plt.ylabel("Probability")
            plt.show()

        print(f"\033[94mStd dev  : {np.std(rFdet):.3f} \033[0m")
        print(f"\033[94mSNR mean : {np.mean(SNR):.3f} \033[0m")

        Cfs2 = np.zeros(self.Npf2 + 1)
        Cpp2 = np.zeros(self.Npp2 + 1)
        Ffirst = np.zeros(Nspek)

        for ip in np.arange(self.Npf2 + 1):
            Cfs2[ip] = Cf2[ip] * np.power(self.Tspan - self.Tskip, -ip)
            Cpp2[ip + 1] = np.multiply(Cfs2[ip], np.power(ip + 1, -1, dtype=float))

        if self.Npp1 < self.Npp2:
            Cfs2[self.Npp1 : self.Npp2] = 0

        for ip in np.arange(Nspek):
            for jp in np.arange(1, self.Npp1):
                Ffirst[ip] = Ffirst[ip] + self.Cfs1[jp] * np.power(tspek[ip], jp)

        # Fvideo includes the 1st polynomials the Fdet only the 2nd approach
        Fvideo: float = self.StartF + Ffirst + Fdet

        tts = tspek + self.StartT

        # Storing the day of the day, timestamp, SNR, Spectral max, Fdet, Residual Fdets
        self.fdets = np.array([MJD, tts, SNR, Smax, Fvideo, rFdet])

        handler.write_fdets(self, 2)

        cppname = f"{self.path}{self.filename[0 : self.fil_lng]}.poly{self.Npp2}.rev2.txt"
        cfsname = f"{self.path}{self.filename[0 : self.fil_lng]}.X{self.Npf2}cfs.rev2.txt"

        np.savetxt(cppname, Cpp2)
        np.savetxt(cfsname, Cfs2)

        FO: float = np.floor(self.BW / self.BW2)
        Nffto: int = int(self.Nfft / FO)
        Nsegm: int = int(Nt / self.Nfft) * self.Ovlp - (self.Ovlp - 1)
        jpso: int = np.arange(Nffto)
        Npfo: float = Nffto * 0.5 + 1
        BWih: float = 0.5 * self.BW2
        # Nto = int(Nt / FO)
        Nto: int = int((self.Tspan - self.Tskip) * SR / FO)
        dto: float = dt * FO
        jto: int = np.arange(Nto)
        tto: float = jto * dto
        dfto: float = 1 / (self.Tspan - self.Tskip)
        fto: float = dfto * jto
        Oshifti: float = self.Nfft / self.Ovlp

        Wini = np.cos(np.pi / self.Nfft * (jps - 0.5 * self.Nfft + 0.5))
        Wino = np.cos(np.pi / Nffto * (jpso - 0.5 * Nffto + 0.5))

        Cf1 = np.zeros(self.Npf2 + 1)
        Cf2 = np.zeros(self.Npf2 + 1)

        for ip in np.arange(self.Npp1):
            Cf1[ip] = self.Cpp1[ip + 1] * (ip + 1) / (2 * np.pi)

        for ip in np.arange(self.Npp2):
            Cf2[ip] = Cpp2[ip + 1] * (ip + 1) / (2 * np.pi)

        Cf = Cf1 + Cf2
        Cf[0] = Cf2[0]
        Cpp = np.zeros(self.Npp2 + 1)

        for ip in np.arange(1, self.Npp2 + 1):
            Cpp[ip] = 2 * np.pi * Cf[ip - 1] / ip

        Fcc = Cpp2[1]

        ts = np.divide(tt, self.Tspan - self.Tskip)

        # DEBUG: here the second polynomial do nothing again
        # Cpp[2] = 0; Cpp[3] = 0

        res = []
        for jp in np.arange(2, self.Npf2 + 1):
            res.append(Cpp[jp] * np.power(ts, jp))

        PhDopp = np.multiply(np.sum(np.array(res), axis=0), self.Tspan - self.Tskip)

        # if we add one here we get exactly one bin less than in Matlab
        # the end bin is exactly the same as python dont count the last
        Bsc: int = int(np.floor((Fcc - BWih) / df - 1))
        Bec: int = int(np.floor(Bsc + Npfo))
        Fstartc = (Bsc + 1) * df  # If we add one here we get exactly the same as matlab
        Pssc = Fstartc * Oshifti * dt - np.floor(Fstartc * Oshifti * dt)
        Esc = np.exp(1j * 2 * np.pi * Pssc)
        Esc = -Esc

        spf = core.MakeFiltX(f'{self.path}{self.filename}', PhDopp, Bsc, Bec, Esc, Nto, Bav, Nspec, Wini, Wino, Nsegm, self.Nfft, Nffto, self.Ovlp)

        spf = spf[sk * Nffto * self.Nav :]
        ssf = np.fft.fft(spf)
        ssfp = np.power(np.abs(ssf), 2)
        xssfp: float = ssfp.max()
        ssfp = np.divide(ssfp, xssfp)

        if self.fverbose:
            plt.suptitle(f"Step 2 - Spacecraft tone after phase correction")
            ax = plt.subplot(1, 1, 1)
            plt.semilogy(fto, ssfp)
            plt.grid(True, which="both", axis="y")
            ax.minorticks_on()
            plt.xlim([0, self.BW2])
            plt.ylim([1e-8, 1])
            plt.xlabel("Freq. [Hz]")
            plt.ylabel("Normalized Power")
            plt.show()

        # ==============================================================================
        # Determine the frequency of the max power
        # after phase correction
        # ==============================================================================
        xft = core.FindMax(ssfp, fto)
        fmax: float = dfto * xft[1]
        spnoise = ssfp[200:900]
        SNR: float = np.divide(1, np.std(spnoise))
        dBSNR: float = 10 * np.log10(SNR)

        print(f"Frequency MAX after PHC: {fmax:.2f} and SNR: {dBSNR:.2f}")

        """
        Step 2.
        We can take a close look to the new tone in narrower bandwidth. 
        The power of the signal is still split between multiple spectral bins.
        The phase correction is NOT error free.
        We iterate a new step on a narrower band to improve it.
        """

        # ==============================================================================
        # We rotate the tone by half of the output frequency (BW3)
        # Ftarg: Target frequency for the tone to rest
        # Frot: Rotation frequency necessary
        # sfc:  down converted signal
        # ssf:  down converted FFT
        # ssfp: down converted Power spectrum (normalized)
        # ==============================================================================
        Ftarg = np.floor(0.1 * self.BW3)
        Frot = fmax - Ftarg

        sfc = spf * np.exp(-1j * 2 * np.pi * Frot * tto)
        ssf = np.fft.fft(sfc)
        ssfp = np.power(np.abs(ssf), 2)
        ssfp /= ssfp.max()

        if self.fverbose:
            plt.title(f"Step 2 - Power spectrum down converted S/C tone")
            plt.semilogy(fto, ssfp)
            plt.xlim([0, self.BW3 / 4])
            plt.ylim([1e-9, 1])
            plt.ylabel("Power (log scale)")
            plt.xlabel("Frequency [Hz]")
            plt.show()

        # We filter everything above the output bandwidth BW3
        ssff = ssf
        bwo: int = np.where(fto == self.BW3)[0][0]
        ssff[bwo:] = 0

        # Return the filtered signal to time domain
        sfc = np.fft.ifft(ssff)

        # Measure the amplitude and phase of the signal in time-domain
        # Phr: Spacecraft tone phase unwrapped
        # dPhr: Linear trend free phase (it could be + or -)
        sc_amplitude = np.abs(sfc)
        sc_phase = np.angle(sfc)
        Phr = np.unwrap(sc_phase)
        dPhr = Phr - 2 * np.pi * Ftarg * tto

        if self.fverbose:
            plt.title(f"Step 2 - Spacecraft tone amplitude")
            plt.plot(tto, sc_amplitude, "r")
            plt.ylabel("Amplitude")
            plt.xlabel("Time")
            plt.show()

        """
        Step 3. Clean the residual phase from noise
        a. We generate the phase polynomial fit (3rd fit correction).
        Defined as self.Npp3
        # rdPhr: residual of the linear trend free phase
        """
        wto = np.ones(Nto)
        PhFit = core.PolyfitW(tto, dPhr, wto, self.Npp3)
        Cf3 = core.PolyfitWC(tto, dPhr, wto, self.Npp3)
        rdPhr = dPhr - PhFit

        if self.fverbose or self.verbose:
            plt.suptitle(f"Step 3 - Summary of the PLL tone detection")
            plt.subplot(2, 1, 1)
            plt.plot(tto, dPhr, "r")
            plt.plot(tto, PhFit, "b")
            plt.ylabel("phase [rad]")
            plt.subplot(2, 1, 2)
            plt.plot(tto, rdPhr, "c")
            plt.ylabel("phase [rad]")
            plt.xlabel("time [sec]")
            plt.show()

        '''
        b. We discard some initial values at the edges. We do a safe solution
        to use +15 and -15 offset
        Store the phase data in Phases.S/C.YYYY.MM.DD.ST.txt format
        '''
        tmp = np.where(tto == 15)
        bmin = tmp[0][0]
        tmp = np.where(tto == self.Tspan - self.Tskip - 15)
        bmax = tmp[0][0]

        print(f"\033[94mPhase rms: {np.std(rdPhr[bmin:bmax])} \033[0m")

        self.phase = np.array([tto[bmin:bmax], dPhr[bmin:bmax], rdPhr[bmin:bmax]])

        handler.write_phase(self)

        print(f"Total running time: {time.process_time()-start}")

        """
        Step 4. Remove the 3rd Phase Polynomial Fit from the signal
        We correct the downconverted signal with the linear trend
        free phase
        """
        sfcc = sfc * np.exp(-1j * PhFit)
        ssf = np.fft.fft(sfcc)
        ssfp = np.power(np.abs(ssf), 2)
        ssfp /= np.max(ssfp)

        rmsf = core.GetRMSf(ssfp, fto, Ftarg, 0.4 * self.BW3, 0.1 * self.BW3)
        SNR = np.divide(1 - rmsf[0], rmsf[1])

        print(f"\033[94mDownconverted Freq RMS: {rmsf} \033[0m")
        print(f"\033[94mDownconverted Freq SNR: {np.mean(SNR)} \033[0m")

        if self.fverbose:
            plt.title(f"Step 4 - Power spectrum downconverted with linear-trend free phase")
            plt.semilogy(fto, ssfp)
            plt.xlim([0, self.BW3])
            plt.ylim([1e-11, 1])
            plt.ylabel("Power (log scale)")
            plt.xlabel("Frequency [Hz]")
            plt.show()


dPLL = function_PLL()
dPLL.set_variables()
dPLL.read_swspec_files()
dPLL.data_processing()
