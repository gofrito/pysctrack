#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 23 10:58:23 2016

Convert the files with residual phase to residual frequency
a.k.a Fdets3.
v0.2 - reformat - output is uncorrect still
v0.1 - Unfinished version 
@author: gofrito
"""

import argparse
import numpy as np

parser = argparse.ArgumentParser()
parser.add_argument('filename', help='File of residual phases',
                    nargs='?', default='check_string_for_empty')
parser.add_argument('intTime', help='Integration time', nargs='?',
                    type=int, default=10)
parser.add_argument('order', help='Determine accuracy order', nargs='?',
                    type=int, default=1)
args = parser.parse_args()

filename = args.filename
avTime   = args.intTime
order    = args.order

phases   = np.loadtxt(filename, skiprows=4)

nscans   = np.size(phases,1)-1

print('')
print(f'Opening File:      {filename}')
print(f'Number of scans:   {nscans}')
print(f'Integration time:  {avTime} s')
print(f'Order of accuracy: {order}')
print('')

# Check time resolution and span
tr = phases[1,0] - phases[0,0]
ts = np.size(phases,0)
tt = np.linspace(0,ts*tr,ts)
tx = np.linspace(0,ts*tr,np.floor(ts*tr/avTime))

# Tsample is the total number of output samples
NUM_SAMPLES = len(tx)
Fdets3  = np.zeros([NUM_SAMPLES, nscans])

for jp in np.arange(nscans):
    for ip in np.arange(NUM_SAMPLES - 1):
        x0 = int(np.floor(avTime/tr*(ip+0.5)))
        x1 = int(np.floor(avTime/tr*(ip+1.5)))
        x2 = int(np.floor(avTime/tr*(ip+2.5)))
        x3 = int(np.floor(avTime/tr*(ip+3.5)))
        x4 = int(np.floor(avTime/tr*(ip+3.5)))
        if order == 1:
            Fdets3[ip,jp] = (-1.0/1.*phases[x0,jp+1]
                            + 1*phases[x1,jp+1])/(2.*np.pi*avTime)
        elif order == 2:
            Fdets3[ip,jp] = (-1.5/1.*phases[x0,jp+1] + 2.*phases[x1,jp+1]
                            - 0.5*phases[x2,jp+1])/(2.*np.pi*avTime)
        elif order == 3:
            Fdets3[ip,jp] = (-11/6.*phases[x0,jp+1]  + 3.*phases[x1,jp+1]
                            - 3/2.*phases[x2,jp+1] + 1/3.*phases[x3,jp+1])/(2.*np.pi*avTime)
        elif order == 4:
            Fdets3[ip,jp] = (-25/12.*phases[x0,jp+1] + 4.*phases[x1,jp+1] - 3.*phases[x2,jp+1]
                            + 4/3.*phases[x3,jp+1] - 1/4.*phases[x4,jp+1])/(2.*np.pi*avTime)

for jp in np.arange(nscans):
    print(f'\033[94m{jp + 1}: {Fdets3[:,jp].std() * 1e3:.2f} mHz\033[94m')

# Store the data in a TXT file
