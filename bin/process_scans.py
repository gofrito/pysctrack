#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 25 20:41:24 2020

process_scans.py - starts a script to run swspec, calculate_cpp and
sctracker
@author: gofrito, owhite
"""

import argparse
import os
import re
import shutil
import subprocess
import sys
import time

import numpy as np

# find the directory where pysctrack and catalogues are installed
PYSCTRACK_DIR: str = os.path.dirname(os.path.realpath(__file__))[:-3]
PYSCTRACK_CAT: str = os.path.join(PYSCTRACK_DIR, 'cats')
PYSCTRACK_BIN: str = os.path.join(PYSCTRACK_DIR, 'bin/')

# Basic definitions
antenna_list = [
    'Bd',
    'Cd',
    'Ef',
    'Hb',
    'Hh',
    'Ho',
    'Ht',
    'Mc',
    'Mh',
    'On',
    'Sv',
    'Sh',
    'T6',
    'Ur',
    'Km',
    'Ke',
    'Wa',
    'Ww',
    'Wn',
    'Wz',
    'Yg',
    'Ys',
    'Zc',
    'Oe',
    'Ow',
    'Mp',
]

parser = argparse.ArgumentParser(
    description='Run in parallel several instances of the SDtracker software',
    epilog='Options 0 and 4 do not use filename. Option 0 requires specification of the station.',
    usage='%(prog)s [options]',
)

parser.add_argument('filename', help='raw data or binary file', type=str)

parser.add_argument(
    '-pr', dest='pr', help='parse a file with the spacecraft scans', type=str
)
parser.add_argument('--version', action='version', version='%(prog)s 0.3')

args = parser.parse_args()

phase_ref: str = args.pr
if args.filename:
    input_file: str = args.filename

# Let's define mode 0 as default and 1 phase-referencing
mode: bool = True
if phase_ref == None:
    mode = False


def ask_cores(prompt, retries=2, complaint='Valid number 1 to 12'):
    while True:
        cores = int(input(prompt))
        if cores < 12:
            return cores
        else:
            retries = retries - 1
        if retries < 0:
            raise IOError('Error to add scan numbers')


def ask_first_scan(prompt, retries=2, complaint='Insert a correct number'):
    while True:
        first_scan = int(input(prompt))
        if first_scan > 0:
            return first_scan
        else:
            retries = retries - 1
        if retries < 0:
            raise IOError('Error with the first scan to process')
        print(complaint)


def ask_last_scan(prompt, retries=2, complaint='Insert a correct number'):
    while True:
        last_scan = int(input(prompt))
        if last_scan > 0:
            return last_scan
        else:
            retries = retries - 1
        if retries < 0:
            raise IOError('Error with the first scan to process')
        print(complaint)


def ask_program(prompt, retries=2, complaint='Option not supported'):
    while True:
        program = int(input(prompt))
        if program in np.arange(8):
            return program
        else:
            print('Option invalid')
            retries = retries - 1
        if retries < 0:
            raise IOError('Option not supported')


def ask_station(prompt, retries=2, complaint='Station not supported'):
    while True:
        station = input(prompt)
        if station in antenna_list:
            return station
        else:
            retries = retries - 1
        if retries < 0:
            raise IOError('Error in specifying the station')


def main(program, mode, phase_ref):
    """
    program: option to run
    mode:
    phase_ref: boolean True if it's the phase referencing mode
    """
    print(f'\n')
    cores: int = 1
    station: str = None
    scans: int = [1]
    scan_param = None

    if (mode == False) & (program > 0) & (program < 7):
        first_scan: int = ask_first_scan('Select the first scan: ')
        last_scan: int = ask_last_scan('Select the last scan: ')
        scans: int = np.arange(first_scan, last_scan + 1)
    elif (mode == True) & (program > 0) & (program < 7):
        if os.path.isfile(phase_ref):
            fn = open(phase_ref, 'r')
            line = fn.readline()
            stscans = line.split(',')

            # using map() to convert a string to int list
            scans = list(map(int, stscans))
        else:
            raise FileNotFoundError

    if program == 0:
        # Init program
        print(f'The station has been preselected as the command line argument')

    # SWspec
    elif program == 1:
        cores = ask_cores('Select number of instances to run in parallel: ')
        station = ask_station('Select station to be processed: ')

    # Calculate_cpp
    elif program == 2:
        print(f'Running calculate Cpp in series')
        init_time: int = input('Specify initial time offset: ')
        scan_time: int = input('Specify desired length of scans: ')
        cline = input(
            'Pass any other required command line arguments (-t0, -t1 and -sn are unnecessary): '
        )
        scan_param = [init_time, scan_time, cline]

    # Calculate_cpp for PCal
    elif program == 3:
        print(f'Running calculate Pcal Cpp')

    # Calculate_cpp for multiple files
    elif program == 4:
        print(f'Running calculate Cpp for multiple files')
        scan_param = input('Pass any required command line arguments (-sn is unnecessary): ')

    # SCtracker
    elif program == 5:
        cores = ask_cores('Select number of instances to run in parallel: ')
        station = ask_station('Select station to be processed: ')

    # PLL
    elif program == 6:
        print('Running digital PLL in series')
        scan_param = input('Pass any required command line arguments: ')

    # compile results
    elif program == 7:
        print('Running compiling results')
        scan_param = input('Pass the number of fdets or phases files to be compiled: ')

    # Move files to correct directories
    elif program == 8:
        print('Moving files into directories')

    return cores, station, scans, scan_param


def run_tasks(program, scan, ncore, input_file, station, scan_param):
    """
    Core of the program, it runs the main scripts

    """
    if program == 0:
        station: str = input_file
        session: str = os.getcwd().split('/')[-1]
        print(f'Initialising all basic files in {session}')

        p = subprocess.Popen(['cp', f'{PYSCTRACK_CAT}/OffsetsNone.txt', '.'])
        p = subprocess.Popen(['cp', f'{PYSCTRACK_CAT}/inifile.Xx', '.'])
        p = subprocess.Popen(['cp', f'{PYSCTRACK_CAT}/inifileXx', '.'])

        time.sleep(0.5)

        # Manipulate inifile.ini for swspec
        s = open(f'inifile.Xx').read()
        s = s.replace('StationID', f'{station}')
        s = s.replace('ProjDate', f'{session}')
        s = s.replace('Instrument', 'VDIF')
        s = s.replace('ScanNo', 'No0001')

        f = open(f'inifile.{station}', 'w+')
        f.write(s)
        f.close()

        # Manipulate inifile.ini for sctracker
        s2 = open(f'inifileXx').read()
        s2 = s2.replace('StationID', f'{station}')
        s2 = s2.replace('Stationid', f'{station.lower()}')
        s2 = s2.replace('ProjDate', f'{session}')
        s2 = s2.replace('Exper', f'{session[0]}{session[3:]}')
        s2 = s2.replace('Instrument', 'VDIF')
        s2 = s2.replace('ScanNo', 'No0001')
        s2 = s2.replace('Scanno', 'no0001')

        f = open(f'inifile{station}', 'w+')
        f.write(s2)
        f.close()

        subprocess.Popen(['rm', 'inifile.Xx', 'inifileXx'])

        return p

    if program == 1:
        match = re.search(r'^([^_]*)_([^_]*)_([^_]*)', input_file, re.I)
        init = match.group(3)[2:6]

        s = open(f'inifile.{station}').read()

        # Replace input file name number
        # Double check init is correct instead of 'init'
        cn = input_file.replace(init, f'{scan:04d}')

        # Replace output file name number in Inifile
        s = s.replace('0001', f'{scan:04d}')

        iniFile = f'inifile.{station}{ncore + 1}'

        f = open(iniFile, 'w')
        f.write(s)
        f.close()

        print(f'Core {ncore + 1} - scan {scan} : Running swspectrometer {iniFile} {cn}')
        p = subprocess.Popen(['swspectrometer', iniFile, cn])

        return p

    if program == 2:
        match = re.search(
            r'^([^_]*)_([^_]*)_(?:([^_]*))_([^_]*)_(\d+)(?:pt)?_(\d+)s?_(?:ch)?([^_]+)_swspec.bin',
            input_file,
            re.I,
        )
        init = match.group(4)[2:6]

        t_0: int = int(scan_param[0]) + (scan - 1) * int(scan_param[1])
        t_1: int = int(scan_param[0]) + scan * int(scan_param[1])

        comm = scan_param[2].split()

        comm.insert(0, input_file)
        comm.insert(0, PYSCTRACK_BIN + 'calculate_cpp.py')
        comm = comm + ['-t0', str(t_0), '-t1', str(t_1), '-sn', str(scan)]

        print(f"Running: {' '.join(comm)}")
        p = subprocess.Popen(comm)

        return p

    if program == 3:
        match = re.search(
            r'^([^_]*)_([^_]*)_(?:([^_]*))_([^_]*)_(\d+)(?:pt)?_(\d+)s?_(?:ch)?([^_]+)_swspec.bin',
            input_file,
            re.I,
        )
        init = match.group(4)[2:6]

        cn = input_file.replace(init, f'{scan:04d}')

        print(f'Running: CalculatePcalCpp.py {cn}')
        p = subprocess.Popen(['calculatePcalCpp.py', cn])

        return p
    
    if program == 4:
        match = re.search(
            r'^([^_]*)_([^_]*)_(?:([^_]*))_([^_]*)_(\d+)(?:pt)?_(\d+)s?_(?:ch)?([^_]+)_swspec.bin',
            input_file,
            re.I,
        )
        init = match.group(4)[2:6]

        comm = scan_param.split()

        cn = input_file.replace(init, f'{scan:04d}')

        comm.insert(0, cn)
        comm.insert(0, PYSCTRACK_BIN + 'calculate_cpp.py')
        comm = comm + ['-sn', str(scan)]

        print(f"Running: {' '.join(comm)}")
        p = subprocess.Popen(comm)

        return p

    if program == 5:
        iniFile = f'inifile{station}{scan}'

        print(f'Core {ncore+1} scan {scan}: Running sctracker {iniFile}')
        p = subprocess.Popen(['sctracker', iniFile])

        return p

    if program == 6:
        match = re.search(
            r'^([^_]*)_([^_]*)_(?:([^_]*))_([^_]*)_(\d+)(?:pt)?_(\d+)s?_(?:ch)?([^_]+)_tone0.bin',
            input_file,
            re.I,
        )
        init = match.group(4)[2:6]

        cn = input_file.replace(init, f'{scan:04d}')

        comm = scan_param.split()

        comm.insert(0, cn)
        comm.insert(0, 'dPLL.py')

        print(f"Running: {' '.join(comm)}")
        p = subprocess.Popen(comm)

        return p

    if program == 7:
        comm = ['compile_results.py', input_file, scan_param]

        print(f"Running: {' '.join(comm)}")
        p = subprocess.Popen(comm)

        return p

    if program == 8:
        print(f'Move files into directories')
        dop0_dir = './dop0'
        dop2_dir = './dop2'
        data_dir = './data'

        if not os.path.exists(dop0_dir):
            os.mkdir(dop0_dir)
        if not os.path.exists(dop2_dir):
            os.mkdir(dop2_dir)
        if not os.path.exists(data_dir):
            os.mkdir(data_dir)

        inifiles = [f for f in os.listdir() if 'inifile' in f.lower()]
        for inifile in inifiles:
            shutil.move(inifile, data_dir + '/' + inifile)
        if os.path.exists('OffsetsNone.txt'):
            shutil.move('OffsetsNone.txt', data_dir + '/OffsetsNone.txt')

        phase_files = [f for f in os.listdir() if 'phases' in f.lower()]
        fdets2_files = [f for f in os.listdir() if 'r2i.txt' in f.lower()]
        for phase_file in phase_files:
            shutil.move(phase_file, dop2_dir + '/' + phase_file)
        for fdets2_file in fdets2_files:
            shutil.move(fdets2_file, dop2_dir + '/' + fdets2_file)

        fdets0_files = [f for f in os.listdir() if 'r0i.txt' in f.lower()]
        for fdets0_file in fdets0_files:
            shutil.move(fdets0_file, dop0_dir + '/' + fdets0_file)

        other_files = [f for f in os.listdir() if os.path.isfile(f)]
        for other_file in other_files:
            shutil.move(other_file, data_dir + '/' + other_file)

        p = None

        return p


print(f'Select which part of the software you wish to run:\n \
    0- Initialize directories and config files\n \
    1- Run SWspec - input raw file and the inifile in place are required\n \
    2- Calculate Polyfit - the spectra binary file is required\n \
    3- Calculate Polyfit for Phase Cal - the spectra binary file is required\n \
    4- Calculate Polyfit for multiple files - the first spectra binary file is required\n \
    5- Run SCtracker - inifiles are already created - specify a random character\n \
    6- Run the digital Phase-Locked-Loop - specify the tone file\n \
    7- Compile results - specify the fdets or phases file\n \
    8- Move files - specify a random character\n')

program = ask_program(
    'What do you wanna a do mate?:\n \
                      Current options: 0, 1, 2, 3, 4, 5, 6, 7, 8 - '
)

cores, station, scans, scan_param = main(program, mode, phase_ref)

for ip in np.arange(0, len(scans), cores):
    processes = []
    for jp in np.arange(0, cores):
        if ip + jp < scans[-1]:
            p = run_tasks(program, scans[ip + jp], jp, input_file, station, scan_param)
            processes.append(p)
    for p in processes:
        if p is not None:
            p.wait()
