#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Mon May 25 20:41:24 2020

Core of the digital Phased-Lock-Loop. Run a sctracker
version with a narrow-band tone and extract the residual
phase only for scintillation analysis
v0.6 - Print updates and fixed wrong use of cpp1 + cpp2
v0.5 - separate scintillation analysis from ultra-narrow tracking
v0.4 - Moved to use only f-strings
v0.3 - New version with Fdets3 output
@author: gofrito
"""

import argparse
import errno
import os
import re
import time

import matplotlib.pyplot as plt
import numpy as np
import scipy.signal as sps
import scipy.stats as stats

from pysctrack import core, handler


class function_PLL:
    def __init__(self):
        # List of parsed parameters
        parser = argparse.ArgumentParser()
        parser.add_argument("filename", nargs="?", help="Input spectra file")
        parser.add_argument("-p", "--plot", help="Plot a summary of the results on screen", action="count", default=False)
        parser.add_argument("-vx", "--vexade", help="Fast move of the S/C do not use weighted functions based on SNR", action="store_true", default=False)
        parser.add_argument("-pc", "--powercentre", help="Do not use an neighbour average to estimate Frequency Max", action="store_true", default=False)
        parser.add_argument("-p1", dest="cpp1", help="First Polynomial", type=int, default=6)
        parser.add_argument("-p2", dest="cpp2", help="Second Polynomial", type=int, default=6)
        parser.add_argument("-p3", dest="cpp3", help="Third Polynomial", type=int, default=6)
        parser.add_argument("-ta", dest="addtime", help="Time added in SCtracker", type=float, default=0)
        parser.add_argument("-t0", dest="starttime", help="Start time", type=float, default=0)
        parser.add_argument("-t1", dest="endtime", help="End time", type=float, default=0)
        parser.add_argument("-nf", dest="nfft", help="Number FFT points", type=int, default=20000)
        parser.add_argument("-ch", dest="channel", help="Number of channel", type=int, default=1)
        parser.add_argument("-ft", dest="forcetime", help="Force integration time if swspec and sctracker use different", type=int)
        parser.add_argument("-ff", dest="forcefft", help="Force number of FFT if swspec and sctracker use different", type=int)
        parser.add_argument("-rf", dest="radiofrequency", help="Starting Radio Frequency", type=float, default=0)
        parser.add_argument("-bw", dest="bandwidth", help="Bandwidth", type=float, default=2000)
        parser.add_argument("-sc", dest="spacecraft", help="specify spacecraft ortherwise use filename", type=str)
        parser.add_argument("-ep", dest="epoch", help="specify your own epoch - format ex. 2020.02.02", type=str)
        parser.add_argument("-bw2", dest="bandwidthStep2", help="Bandwidth used with 2nd fit", type=float, default=20)
        parser.add_argument("-bw3", dest="bandwidthStep3", help="Bandwidth used with 3rd fit", type=float, default=20)
        parser.add_argument("-ovl", dest="overlap", help="FFT overlapping factor", type=float, default=2)
        parser.add_argument("-nav", dest="nav", help="Number of averaged FFT", type=float, default=2)
        parser.add_argument("-pad", dest="padding", help="Padding factor", type=float, default=2)

        args = parser.parse_args()

        self.vexade: bool = args.vexade
        self.powcen: bool = args.powercentre
        self.Tadd: int = args.addtime
        self.Tskip: int = args.starttime
        self.Nfft: int = args.nfft
        self.channel = args.channel

        if args.forcetime:
            self.ft: int = args.forcetime
        else:
            self.ft: bool = False

        if args.forcefft:
            self.fFFT: int = args.forcefft
        else:
            self.fFFT: bool = False

        self.BW: int = args.bandwidth
        self.BW2: int = args.bandwidthStep2
        self.BW3: int = args.bandwidthStep3
        self.npp1: int = args.cpp1
        self.npp2: int = args.cpp2
        self.npp3: int = args.cpp3
        self.Ovlp: int = args.overlap
        self.Nav: int = args.nav
        self.Padd: int = args.padding
        self.baseband_freq: float = args.radiofrequency

        self.verbose: bool = False
        self.fverbose: bool = False
        if args.plot == 1:
            self.verbose: bool = True
        elif args.plot > 1:
            self.fverbose: bool = True

        # Check if the file exists
        if os.path.exists(args.filename):
            path, self.filename = os.path.split(args.filename)
            self.path = f"{os.path.abspath(path)}/"
            self.fil_lng = len(self.filename) - 10
        else:
            raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), args.filename)

        match = re.search(r"^([^_]*)_([^_]*)_(?:([^_]*))_([^_]*)_(\d+)(?:pt)?_(\d+)s?_(?:ch)?([^_]+)_tone", self.filename, re.I)

        self.session: str = match.group(1)
        self.station: str = match.group(2)
        self.raw_format: str = match.group(3)
        self.scan_number: int = int(match.group(4)[2:6])
        self.scanMode: str = match.group(4)[0:2]
        self.FFT: int = match.group(5)
        self.ints: int = match.group(6)
        self.channel: int = match.group(7)

        # Estimate the number of samples in the file
        fsize: float = os.path.getsize(f'{self.path}{self.filename}')
        self.total_file: float = fsize / (2 * 8 * self.BW)

        if args.endtime:
            self.Tspan: float = args.endtime
            Nt: int = self.Tspan * 2 * 8 * self.BW
        else:
            self.Tspan: float = fsize / (2 * 8 * self.BW)
            Nt: int = fsize / 8

        # Debug info on screen
        print('\n# Settings')
        print(f"Length of the file   : {self.total_file} sec")
        print(f"Length selected      : {self.Tspan} sec")
        print(f"Samples selected     : {Nt} samples")

        """
        Change 30 May 2020: it was divided by 10 assuming that 2 kHz/20000 Nfft. Now :
        dts = Nfft/BW
        """
        dts: float = self.Nfft / self.BW
        self.Tspan: float = np.floor(self.Tspan / dts) * dts

        # fill the epoch variable
        if args.epoch:
            self.epoch: str = args.epoch
        else:
            self.epoch: str = f"20{self.session[1:3]}.{self.session[3:5]}.{self.session[5:7]}"

        # Finding which spacecraft has been observed.
        if args.spacecraft:
            self.spacecraft: str = args.spacecraft
        else:
            self.spacecraft: str = handler.assign_spacecraft(self.session[0])

    def set_variables(self):
        # ==============================================================================
        # Nfft:  Number of FFT points
        # Tadd:  Initial time added to sctracker
        # Tskip: Time to skip into the function PLL
        # Tspan: Ending time
        # ==============================================================================
        self.npf1: int = self.npp1 - 1
        self.npf2: int = self.npp2 - 1

        log_file = f"{self.session}_{self.station}_{self.raw_format}_{self.scanMode}{self.scan_number:04d}_{self.FFT}pt_{self.ints}s_ch{self.channel}"

        # Generic case unless overwritten from command line
        pol_file = log_file

        # we force the integration time
        if self.ft:
            pol_file = f"{self.session}_{self.station}_{self.raw_format}_{self.scanMode}{self.scan_number:04d}_{self.FFT}pt_{self.ft}s_ch{self.channel}"

        # we force the FFT
        if self.fFFT:
            pol_file = f"{self.session}_{self.station}_{self.raw_format}_{self.scanMode}{self.scan_number:04d}_{self.fFFT}pt_{self.ints}s_ch{self.channel}"

        # we force the FFT and the integration time
        if self.fFFT and self.ft:
            # We force both
            pol_file = f"{self.session}_{self.station}_{self.raw_format}_{self.scanMode}{self.scan_number:04d}_{self.fFFT}pt_{self.ft}s_ch{self.channel}"

        self.tonebin = f"{self.path}{log_file}_tonebinning.txt"
        self.timebin = f"{self.path}{log_file}_starttiming.txt"
        self.filecpp = f"{self.path}{pol_file}.poly{self.npp1}.txt"
        self.filecfs = f"{self.path}{pol_file}.X{self.npf1}cfs.txt"

        if os.path.isfile(self.filecpp) == False:
            print(f"\033[91 mCPP file not found : {self.filecpp}\033[0m")
            raise FileNotFoundError

    def read_swspec_files(self):
        # Read Tone and Start time to know the beginning of the raw data file
        Tbinfo: str = np.loadtxt(self.tonebin)
        Tsinfo: str = np.loadtxt(self.timebin, skiprows=1)

        # We will use the To for the recording start and the initial tone bin
        self.StartF: float = Tbinfo[3]
        self.StartT: float = Tsinfo[1]
        self.MJD: int = Tsinfo[0]

        # Read the Phase Polynomial Coefficients iteration 1
        self.cpp1: float = np.loadtxt(self.filecpp)
        self.cfs1: float = np.loadtxt(self.filecfs)

    def data_processing(self):
        start: float = time.process_time()
        SR: int = 2 * self.BW  # Sampling Rate
        Nt: int = int(self.Tspan * SR)  # Total number of samples
        dt: float = 1 / SR  # Sampling interval
        df: float = SR / self.Nfft  # frequency resolution
        jt: float = np.arange(Nt)
        tw: float = dt * self.Nfft
        tt: float = jt * dt

        # save them for plotting and to write to disk with handle
        self.sampling_interval = dt
        self.df = df

        Nspec: int = int(Nt / (self.Nfft * self.Nav))
        sk: int = int(self.Tskip * df / 2)
        Nspek: int = Nspec - sk
        jspek: int = np.linspace(sk, Nspec - 1, Nspek)
        Bav: int = self.Nfft * self.Nav
        tspek: float = (jspek + 0.5) * Bav * dt
        Npadd: int = int(self.Nfft * (self.Padd - 1))
        dpadd: int = np.zeros(Npadd)

        if self.Padd:
            Npadd = 1


        print(f'Skip  # of spectra   : {sk}')
        print(f'Total # of spectra   : {Nspec}')
        print(f'Frequency resolution : {self.df} Hz')
        print(f'Sampling interval    : {1e3 * self.sampling_interval} ms')
        print('\n# Measurements')

        jps: int = np.arange(self.Nfft)
        Win: float = np.power(np.cos(np.pi / self.Nfft * (jps - 0.5 * self.Nfft + 0.5)), 2)
        Nfp: float = self.Nfft * self.Padd / 2 + 1
        jfs: float = np.arange(Nfp)
        dfs: float = 1 / (tw * self.Padd)
        ffs: float = jfs * dfs
        self.df = df

        # make_spec is the core function that reads the tone and outputs the spectra
        Sp = core.make_spec(f"{self.path}{self.filename}", Nspec, self.Nfft, self.Nav, self.Ovlp, Win, self.Padd, dpadd)
        xSp: float = np.mean(Sp)
        Sp = np.divide(Sp, xSp)
        Spa = np.divide(Sp.sum(axis=0), Nspec)

        if self.fverbose:
            plt.title(f"Step 1 - Full spectra average")
            plt.semilogy(ffs, Spa, color='blue')
            plt.xlim([0, self.BW])
            plt.ylabel("Power spectrum")
            plt.xlabel("Frequency [Hz]")
            plt.show()

        if self.fverbose:
            plt.title(f"Step 1 - Zoom at the full spectra")
            plt.semilogy(ffs, Spa, marker=".", linestyle=None)
            plt.ylabel("Power spectrum")
            plt.xlabel("Frequency [Hz]")
            plt.xlim([0.475 * self.BW, 0.525 * self.BW])
            plt.show()

        half_window: int = 40
        line_avoidance: int = 10

        xf: float = np.zeros((Nspek, 3))
        rmsd: float = np.zeros((Nspek, 3))

        dxc: float = np.zeros(Nspek)
        SNR: float = np.zeros(Nspek)
        Smax: float = np.zeros(Nspek)
        Fdet: float = np.zeros(Nspek)
        MJD: float = np.zeros(Nspek)
        Weight: float = np.ones((Nspek, 1))

        for ip in np.arange(sk, Nspec):
            jp = ip - sk
            xf[jp] = core.FindMax(Sp[ip], ffs)
            Smax[jp] = xf[jp, 2]
            Fdet[jp] = dfs * xf[jp, 1] + ffs[0]
            if self.powcen == 1:
                dxc = core.PowCenter(Sp[ip], xf[jp, 1], 3) * dfs
                Fdet[jp] = Fdet[jp] + np.transpose(dxc)
            rmsd[jp] = core.GetRMS(Sp[ip], xf[jp, 1], half_window / dfs, line_avoidance / dfs)
            SNR[jp] = (xf[jp, 2] - rmsd[jp, 0]) / rmsd[jp, 1]

        mSNR: float = SNR.mean()
        mFdet: float = Fdet.mean()

        MJD[0:Nspek] = self.MJD

        if self.vexade:
            Fdet = Fdet - mFdet
        else:
            Weight = np.power(SNR, 2) / mSNR

        Ffit = core.PolyfitW1(tspek, Fdet, Weight, self.npf2)
        cf2 = core.PolyfitW1C(tspek, Fdet, Weight, self.npf2)

        rFdet = Fdet - Ffit

        if self.verbose or self.fverbose:
            plt.suptitle(f"Step 2 - Summary of the frequency fit rev. 2")
            plt.subplot(4, 1, 1)
            plt.semilogy(ffs, Spa)
            plt.ylabel("Spectrum")
            plt.xlabel("Frequency [Hz]")
            plt.xlim([0, self.BW])
            plt.subplot(4, 1, 2)
            plt.plot(tspek, SNR, "rx")
            plt.xlabel("Time [s]")
            plt.ylabel("SNR")
            plt.xlim([0, np.max(tspek)])
            plt.subplot(4, 1, 3)
            plt.plot(tspek, Fdet, "r.")
            plt.plot(tspek, Ffit, "b")
            plt.xlabel("Time [s]")
            plt.ylabel("Freq. [Hz]")
            plt.xlim([0, np.max(tspek)])
            plt.subplot(4, 1, 4)
            plt.plot(tspek, rFdet, "k.")
            plt.xlabel("Time [s]")
            plt.ylabel("Residual Freq [Hz]")
            plt.xlim([0, np.max(tspek)])
            plt.show()

        if self.fverbose:
            plt.title(f"Step 2 - Histogram of the frequency fit residuals")
            n, bins, patches = plt.hist(rFdet, density=1)
            (mu, sigma) = stats.norm.fit(rFdet)
            y = stats.norm.pdf(bins, mu, sigma)
            plt.plot(bins, y, "r--")
            plt.xlabel("Residual Freq [Hz]")
            plt.ylabel("Probability")
            plt.show()

        if self.vexade:
            cf2[0] = cf2[0] + mFdet

        # Print out results
        print(f"\033[94mStd dev  : {rFdet.std() * 1e3:.3f} mHz\033[0m")
        print(f"\033[94mSNR mean : {SNR.mean():.3f} \033[0m")

        cfs2 = np.zeros(self.npf2 + 1)
        cpp2 = np.zeros(self.npp2 + 1)
        Ffirst = np.zeros(Nspek)

        for ip in np.arange(self.npf2 + 1):
            cfs2[ip] = cf2[ip] * np.power(self.Tspan - self.Tskip, -ip)
            cpp2[ip + 1] = np.multiply(cfs2[ip], np.power(ip + 1, -1, dtype=float))

        if self.npp1 < self.npp2:
            cfs2[self.npp1 : self.npp2] = 0

        for ip in np.arange(Nspek):
            for jp in np.arange(1, self.npp1):
                Ffirst[ip] = Ffirst[ip] + self.cfs1[jp] * np.power(tspek[ip], jp)

        # Fvideo includes the 1st polynomials the Fdet only the 2nd approach
        Fvideo = self.StartF + Ffirst + Fdet
        if self.vexade == 1:
            Fvideo = Fvideo + mFdet

        tts = tspek + self.StartT + self.Tadd

        # Storing the day of the day, timestamp, SNR, Spectral max, Fdet, Residual Fdets
        self.fdets = np.array([MJD, tts, SNR, Smax, Fvideo, rFdet])

        handler.write_fdets(self, 2)

        cppname = f"{self.path}{self.filename[0 : self.fil_lng]}.poly{self.npp2}.rev2.txt"
        cfsname = f"{self.path}{self.filename[0 : self.fil_lng]}.X{self.npf2}cfs.rev2.txt"

        np.savetxt(cppname, cpp2)
        np.savetxt(cfsname, cfs2)

        FO: float = np.floor(self.BW / self.BW2)
        Nffto: int = int(self.Nfft / FO)
        Nsegm: int = int(Nt / self.Nfft) * self.Ovlp - (self.Ovlp - 1)
        jpso = np.arange(Nffto)
        npfo: float = Nffto * 0.5 + 1
        BWih: float = 0.5 * self.BW2
        # Nto = int(Nt / FO)
        Nto: int = int((self.Tspan - self.Tskip) * SR / FO)
        dto: float = dt * FO
        jto = np.arange(Nto)
        tto: float = jto * dto
        dfto: float = 1 / (self.Tspan - self.Tskip)
        fto: float = dfto * jto
        Oshifti: float = self.Nfft / self.Ovlp

        Wini = np.cos(np.pi / self.Nfft * (jps - 0.5 * self.Nfft + 0.5))
        Wino = np.cos(np.pi / Nffto * (jpso - 0.5 * Nffto + 0.5))

        ''' section removed since we dont use this here'''
        #cf1 = np.zeros(self.npf2 + 1)
        #cf2 = np.zeros(self.npf2 + 1)

        #for ip in np.arange(self.npp1):
        #    cf1[ip] = self.cpp1[ip + 1] * (ip + 1) / (2 * np.pi)

        #for ip in np.arange(self.npp2):
        #    cf2[ip] = cpp2[ip + 1] * (ip + 1) / (2 * np.pi)

        #cf = cf1 + cf2
        #cf[0] = cf2[0]
        #cpp = np.zeros(self.npp2 + 1)

        #for ip in np.arange(1, self.npp2 + 1):
        #    cpp[ip] = 2 * np.pi * cf[ip - 1] / ip

        Fcc: np.double = cf2[0]

        ts = np.divide(tt, self.Tspan - self.Tskip)
        res = []
        for jp in np.arange(2, self.npp2 + 1): # Change npf2 for npp2, otherwise we miss the last polynomial - no big change
            res.append(cpp2[jp] * np.power(ts, jp))

        PhDopp = np.multiply(np.sum(np.array(res), axis=0), self.Tspan - self.Tskip)

        Bsc: int = int(np.floor((Fcc - BWih) / df))
        Bec: int = int(Bsc + npfo)
        Fstartc = Bsc * df

        Pssc = Fstartc * Oshifti * dt - np.floor(Fstartc * Oshifti * dt)
        Esc = np.exp(1j * 2 * np.pi * Pssc)

        spf = core.MakeFiltX(f'{self.path}{self.filename}', PhDopp, Bsc, Bec, Esc, Nto, Bav, Nspec, Wini, Wino, Nsegm, self.Nfft, Nffto, self.Ovlp)

        spf = spf[sk * Nffto * self.Nav :]
        ssf = np.fft.fft(spf)
        ssfp = np.power(np.abs(ssf), 2)
        xssfp: float = ssfp.max()
        ssfp = np.divide(ssfp, xssfp)
        #        np.save('test.npy',[fto,ssfp])

        if self.fverbose:
            plt.suptitle(f"Step 3 - Spacecraft tone after phase correction")
            ax = plt.subplot(1, 1, 1)
            plt.semilogy(fto, ssfp)
            plt.grid(True, which="both", axis="y")
            ax.minorticks_on()
            plt.xlim([0, self.BW2])
            plt.ylim([1e-9, 1])
            plt.xlabel("Frequency [Hz]")
            plt.ylabel("Normalized Power")
            plt.show()

        # ==============================================================================
        # Determine the frequency of the max power
        # after phase correction
        # ==============================================================================
        xft = core.FindMax(ssfp, fto)
        fmax: float = dfto * xft[1]
        spnoise = ssfp[200:900]
        SNR: float = np.divide(1, np.std(spnoise))
        dBSNR: float = 10 * np.log10(SNR)

        print('\n# PLL')
        print(f"Frequency : {(self.baseband_freq * 1e6 + self.StartF + fmax):.3f} Hz")
        print(f"SNR       : {dBSNR:.2f} dB")
        """
        Step 2.
        We can take a close look to the new tone in narrower bandwidth. 
        The power of the signal is still split between multiple spectral bins.
        The phase correction is NOT error free.
        We iterate a new step on a narrower band to improve it.
        """

        # ==============================================================================
        # We rotate the tone by half of the output frequency (BW3)
        # Ftarg: Target frequency for the tone to rest
        # Frot: Rotation frequency necessary
        # sfc:  down converted signal
        # ssf:  down converted FFT
        # ssfp: down converted Power spectrum (normalized)
        # ==============================================================================
        Ftarg = np.floor(0.5 * self.BW3)
        Frot = fmax - Ftarg

        sfc = spf * np.exp(-1j * 2 * np.pi * Frot * tto)
        ssf = np.fft.fft(sfc)
        ssfp = np.power(np.abs(ssf), 2)
        ssfp /= ssfp.max()

        if self.fverbose:
            plt.title(f"Step 3 - Power spectrum down converted S/C tone - Zoomed")
            plt.semilogy(fto, ssfp)
            plt.xlim([self.BW3 / 4, 3 * self.BW3 / 4])
            plt.ylim([1e-9, 1])
            plt.ylabel("Power (log scale)")
            plt.xlabel("Frequency [Hz]")
            plt.show()

        # We filter everything above the output bandwidth BW3
        ssff = ssf
        bwo: int = np.where(fto == self.BW3)[0][0]
        ssff[bwo:] = 0

        # Return the filtered signal to time domain
        sfc = np.fft.ifft(ssff)

        # Measure the amplitude and phase of the signal in time-domain
        # Phr: Spacecraft tone phase unwrapped
        # dPhr: Linear trend free phase (it could be + or -)
        sc_amplitude = np.abs(sfc)
        sc_phase = np.angle(sfc)
        Phr = np.unwrap(sc_phase)

        dPhr = Phr - 2 * np.pi * Ftarg * tto

        if self.fverbose:
            plt.title(f"Step 3 - Spacecraft tone amplitude")
            plt.plot(tto, sc_amplitude, "r")
            plt.ylabel("Amplitude")
            plt.xlabel("Time")
            plt.show()

        """
        Step 3. Clean the residual phase from noise
        a. We generate the phase polynomial fit (3rd fit correction).
        Defined as self.npp3
        # rdPhr: residual of the linear trend free phase
        """
        wto = np.ones(Nto)
        phase_fit = core.PolyfitW(tto, dPhr, wto, self.npp3)
        cf3 = core.PolyfitWC(tto, dPhr, wto, self.npp3)
        rdPhr = dPhr - phase_fit

        '''
        b. We discard some initial values at the edges. We do a safe solution
        to use +15 and -15 offset
        Store the phase data in Phases.S/C.YYYY.MM.DD.ST.txt format
        '''
        tmp = np.where(tto == 15)
        bmin = tmp[0][0]
        tmp = np.where(tto == self.Tspan - self.Tskip - 15)
        bmax = tmp[0][0]

        if self.fverbose or self.verbose:
            plt.suptitle(f"Step 3 - Summary of the PLL tone detection")
            plt.subplot(2, 1, 1)
            plt.plot(tto[bmin:bmax], dPhr[bmin:bmax], "r")
            plt.plot(tto[bmin:bmax], phase_fit[bmin:bmax], "b")
            plt.xlim([tto[bmin], tto[bmax]])
            plt.ylabel("phase [rad]")
            plt.subplot(2, 1, 2)
            plt.plot(tto[bmin:bmax], rdPhr[bmin:bmax], "c")
            plt.xlim([tto[bmin], tto[bmax]])
            plt.ylabel("phase [rad]")
            plt.xlabel("time [sec]")
            plt.show()

        print(f"\033[94mPhase rms : {rdPhr[bmin:bmax].std():.2f} rads\033[0m\n")

        # Dump phase and time to a text file
        self.phase = np.array([tto[bmin:bmax], rdPhr[bmin:bmax]])
        handler.write_phase(self)

        """
        c.  Very quick and dirty estimate of the phase scintillation
        It gives an overlook of the quality of the data
        """
        bss: int = int(0.003 / df + 1.5)
        bse: int = int(3.003 / df + 1.5)
        lph: int = np.size(self.phase[1, :])
        wcos: float = sps.windows.cosine(lph)
        sp: float = np.fft.fft(self.phase[1, :])
        spw: float = np.fft.fft(np.multiply(self.phase[1, :], wcos))
        pspw: float = np.power(np.abs(spw), 2)
        pspw = 2 * pspw / self.BW3
        dta: float = self.phase[0, 1] - self.phase[0, 0]
        Tsa: float = dta * lph
        dfa: float = 1 / Tsa
        ff = np.arange(0, self.BW2 * 2 - dfa, dfa)

        FiltScint = np.zeros(lph)
        FiltScint[bss:bse] = 2

        spsc = np.zeros((np.size(self.phase[1, :])), dtype=np.complex64)
        spsc = np.multiply(sp, FiltScint)
        phs = np.real(np.fft.ifft(spsc, axis=0))
        rmsphs = np.std(phs, axis=0)

        if self.fverbose:
            print(f"Phase scintillation RMS: {rmsphs}")
            plt.loglog(ff, pspw)
            plt.title(f'Step 3: Quick estimation of the fluctuations density power')
            plt.vlines(0.003, 1, 1e12, colors='red')
            plt.vlines(3.003, 1, 1e12, colors='red')
            plt.xlim([1e-4, 10])
            plt.ylim([1e-6, 10 * np.max(pspw)])
            plt.show()
            plt.vlines

        print(f"Total running time: {time.process_time()-start:.2f} sec")


dPLL = function_PLL()
dPLL.set_variables()
dPLL.read_swspec_files()
dPLL.data_processing()
