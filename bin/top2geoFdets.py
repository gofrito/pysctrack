#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 07 10:49:40 2021

Convert Fdets file to geocentric

v0.1 original version from pre-repo
@author: gofrito
"""
import argparse
import errno
import os
import re

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import spiceypy as spice
from spiceypy.spiceypy import et2datetime
from pysctrack import converter, core, handler

def derivative(x, y, points=5, poly=2):
    """
    Calculate a derivative dy/dx at each x
    Don't forget to normalise output
    """
    dydx = np.zeros_like(y)
    for n0, xi in enumerate(x):
        # number of points to cut from the left-hand side
        nl = int(np.floor(points / 2.0))
        # number of points to cut from the right-hand side
        nr = int(np.ceil(points / 2.0))
        # check/correct bounds:
        if len(x[:n0]) < nl:
            nr = int(nr + nl - len(x[:n0]))
            nl = int(len(x[:n0]))
        if len(x[n0:]) < nr:
            nl = int(nl + nr - len(x[n0:]))
            nr = int(len(x[n0:]))

        # make a fit
        yfit = np.polyfit(x[n0 - nl : n0 + nr], y[n0 - nl : n0 + nr], poly)
        dydx[n0] = np.polyval(np.polyder(yfit), xi)

    return dydx


parser = argparse.ArgumentParser()

c_light = 299792458

parser.add_argument("fdetsfile", nargs="?", help="Frequency detections file", type=str, default="check_string_for_empty")
parser.add_argument("delayfile", nargs="?", help="Set of VLBI delays generated with pypride", type=str, default="check_string_for_empty")
parser.add_argument("-p", "--plot", help="plot frequency detections for debugging", action="store_true", default=False)

args = parser.parse_args()

fdetsfile: str = args.fdetsfile
delayfile: str = args.delayfile
plot: bool = args.plot

# Check if file Fdets exists
if os.path.exists(fdetsfile):
    fdetspath, fdetsfn = os.path.split(fdetsfile)
    fdetspath = f"{os.path.abspath(fdetspath)}/"
else:
    raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), fdetsfile)

# Check if the Delays exists
if os.path.exists(delayfile):
    delaypath, delayfn = os.path.split(delayfile)
    delaypath = f"{os.path.abspath(delaypath)}/"
else:
    raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), delayfile)

# Read the Fdets file as a Pandas data frame
fdets = pd.read_csv(f"{fdetspath}{fdetsfn}", skiprows=4, delimiter=" ", skipinitialspace=True, names=["MJD", "Time", "SNR", "Smax", "Fdets", "Dnoise"])

# Is a file with a single or multiple scans
if fdetsfn.count(".") == 7:
    "we have one scan"
    match = re.search(r"^([^.]*).([^.]*).([^.]*).([^.]*).([^.]*).([^.]*).([^.]*).", fdetsfn, re.I)

    file: str = match.group(1)
    spacecraft: str = match.group(2)[0:3]
    yy: str = match.group(2)[3:5]
    mm: str = match.group(3)
    dd: str = match.group(4)
    station: str = match.group(5)
    scan_number: str = match.group(6)
    rev = match.group(7)
elif fdetsfn.count(".") == 6:
    "we have multiple scans"
    match = re.search(r"^([^.]*).([^.]*).([^.]*).([^.]*).([^.]*).([^.]*).", fdetsfn, re.I)

    file: str = match.group(1)
    spacecraft: str = match.group(2)[0:3]
    yy: str = match.group(2)[3:5]
    mm: str = match.group(3)
    dd: str = match.group(4)
    station: str = match.group(5)
    rev = match.group(6)
else:
    "File might be invalid"
    print(f'Fdets {fdetsfn} does not have correct data format')
    raise Exception

# Make the output copy
fdets_gc = fdets

# Read the Fdets file as a Pandas data frame
delay_cols = ["yy", "mm", "dd", "HH", "MM", "SS", "Source", "u", "v", "w", "delay_geom", \
    "delay_antenna", "delay_axis", "delay_tropo", "delay_iono", "phase", "amp", "clock"]
delays = pd.read_csv(f"{delaypath}{delayfn}", skiprows=26, delimiter=' ', skipinitialspace=True, names=delay_cols)

# Use a data Frame for space craft data to simplify operations
VLBI_delay = delays[delays['Source'] == spacecraft.upper()]
n_rows = VLBI_delay.shape[0]

if delays[delays['Source'].str.contains('spacecraft.upper()')].any:
    print(f'Spacecraft {spacecraft.upper()} is valid')
else:
    print(f'Spacecraft {spacecraft.upper()} is not in the delays file - Select correct S/C or check S/C name in the delay file')
    raise ValueError

# Initialise
mjd: int = np.zeros(n_rows)
Delay_rate: float = np.zeros(n_rows)

# Convert to modified julian date
for ip in range(n_rows):
    mjd[ip] = converter.mjuliandate(VLBI_delay['yy'].iloc[ip], VLBI_delay['mm'].iloc[ip], VLBI_delay['dd'].iloc[ip], \
        VLBI_delay['HH'].iloc[ip], VLBI_delay['MM'].iloc[ip], VLBI_delay['SS'].iloc[ip])

# sampling output of the delays to calculate the rate
dt = mjd[1] - mjd[0]
for ip in range(n_rows - 1):
    Delay_rate[ip] = (VLBI_delay['delay_geom'].iloc[ip + 1] - VLBI_delay['delay_geom'].iloc[ip]) / dt

output = np.array([mjd, VLBI_delay['delay_geom'], Delay_rate])

# Build the dataframe called sc_delays
sc_delays = pd.DataFrame(data = output.transpose(), columns = ['MJD', 'VLBI_delay', 'Delay_rate'])

# make a copy of fdets
geo_fdets = fdets

# extract beginning of the frequency channel
'''
I am assuming that the format is:
# Base frequency: 8415.99 MHz
'''
with open(f"{fdetspath}{fdetsfn}", 'r') as fd:
    line0 = fd.readline()
    line1 = fd.readline()
    line2 = fd.readline()
    line3 = fd.readline()

    # Now extract start frequency  
    str_freq: str = line1
    start_freq: float = float(str_freq.split()[3])

    fdets_header = f'{line0}{line1}{line2}{line3}'

fd.close()

# Sanity check the Fdets format is correct
if start_freq < 8000:
    print(f'The initial frequency ({start_freq}) is too low. Is the format wrong?')

#for ip in range(len(fdets['MJD'])):
#    taug = np.interp(x=fdets['MJD'].iloc[ip] + fdets['Time'].iloc[ip], xp = sc_delays['MJD'], fp = sc_delays['VLBI_delay'])
#    rtaug = np.interp(x=fdets['MJD'].iloc[ip] + fdets['Time'].iloc[ip], xp = sc_delays['MJD'], fp = sc_delays['Delay_rate'])

    #geo_fdets['Fdets'].iloc[ip] = fdets['Fdets'].iloc[ip] * ( -taug) * (1 - rtaug)
#    geo_fdets['Fdets'].iloc[ip] = taug

taug = np.interp(x=fdets['MJD'] + fdets['Time'], xp = sc_delays['MJD'], fp = sc_delays['VLBI_delay'])
rtaug = np.interp(x=fdets['MJD'] + fdets['Time'], xp = sc_delays['MJD'], fp = sc_delays['Delay_rate'])
print(fdets['MJD'].iloc[0])
geo_fdets['Fdets'] = np.interp(x=fdets['Time'] - taug,
    xp = fdets['Time'], fp = fdets['Fdets'])

#print(len(geo_fdets))

# Correction for the delay rate
geo_fdets['Fdets'] *= (1 - rtaug)

#print(taug['MJD'].iloc[0])
if plot:
    plt.plot(geo_fdets['Time'], geo_fdets['Fdets'], marker='x'); hold=True;
    plt.plot(fdets['Time'], fdets['Fdets'], marker='x')
    #plt.plot(sc_delays['MJD'], sc_delays['VLBI_delay'])
    plt.title('Geocentric frequency detections')
    plt.ylabel('Frequency [Hz]')
    plt.xlabel('Time [s]')
    plt.show()

if plot:
    plt.plot(geo_fdets['Fdets'] - fdets['Fdets'])
    plt.show()
'''
# Define epoch
epoch: str = f"20{yy}.{mm}.{dd}"
geocenter: str = "EARTH"
ref_frame: str = "ITRF93"
light_cor: str = "CN+S"
stat: str = "YARRAGAD"

stations = {"Yg": "YARRAGAD"}

# Check if the station exists
if station in stations:
    st = stations[station]
else:
    print(f"Station not found")

# Check times
t_0 = fdets["Time"][0]
dts = fdets["Time"][1] - fdets["Time"][0]
# Convert t_0 to ET
HH, MM, SS = converter.sec2hmi(t_0)
ut_0: str = f"20{yy}-{mm}-{dd}T{int(HH):02d}:{int(MM):02d}:{int(SS):02d}"

ut = []
for ip in np.arange(fdets.shape[0]):
    ut.append(f"20{yy}-{mm}-{dd}T{int(HH):02d}:{MM:02d}:{int(SS):02d}.000")

# Load metakernel
spice.furnsh("/Users/gofrito/Treball/Repos/pysctrack-nb/nb/aux/kernels/naifLAST.tls")
spice.furnsh("/Users/gofrito/Treball/Repos/pysctrack-nb/nb/aux/kernels/de440.bsp")
spice.furnsh("/Users/gofrito/Treball/Repos/pysctrack-nb/nb/aux/kernels/vlbistns_itrf93_oceania_131127.bsp")
spice.furnsh("/Users/gofrito/Treball/Repos/pysctrack-nb/nb/aux/kernels/vlbistns_itrf93_topo_oceania_131127.tf")
spice.furnsh("/Users/gofrito/Treball/Repos/pysctrack-nb/nb/aux/kernels/earth_000101_210510_210216.bpc")

# convert times
et = ut
et = spice.utc2et(ut_0)

# Convert
tt = spice.unitim(et, "TDB", "TDT")

# Provide cords for the station
"Done above"

rv_st = np.zeros(fdets.shape[0])
d_st = np.zeros(fdets.shape[0])
# Use spice kernels and reference geocentre to calculate velocities
for ip in np.arange(fdets.shape[0]):
    # Calculate the position
    r_st, v_st = spice.spkezr(st, tt, ref_frame, light_cor, geocenter)

    # Total radial velocity
    rv_st[ip] = np.linalg.norm(v_st)

    # Total distance antenna center
    d_st[ip] = np.linalg.norm(r_st)

    # Next sample
    tt += dts

t_st = d_st / 299792.458
print(t_st)

dr_st = derivative(t_st, rv_st, points=5, poly=2)

print(dr_st)
# Calculate change of velocity
dv_st = np.zeros(fdets.shape[0])
for ip in np.arange(fdets.shape[0] - 1):
    dv_st[ip] = rv_st[ip + 1] - rv_st[ip]
dv_st[-1] = dv_st[-2]  # km/s

# Convert velocity to geocentre
st2gc = np.zeros(fdets.shape[0])
st2gc = dv_st * 8400e6 / 299792.458
ast2gc = np.zeros(fdets.shape[0])
for ip in np.arange(fdets.shape[0]):
    ast2gc[ip] = st2gc[ip] - st2gc[0]

# print(ast2gc)
# Substract Hz to Fdets
fdets_gc["Fdets"] -= st2gc

fdets_gc2 = (fdets_gc["Fdets"] + 8415e6) * (et - t_st) * (1 - dr_st)
print(fdets_gc2)

plt.plot(fdets_gc2)
plt.show()
## store file
# Just copy the header
fd = open(f"{fdetspath}{fdetsfile}", "r")
fdets_header = []
for ip in np.arange(4):
    fdets_header.append(fd.readline())
fd.close()

# Create the new Fdets file
fdets_ofn: str = f"{fdetspath}Fdets.{spacecraft}{epoch}.{station}.{scan_number}.gc.r{rev}i.txt"

# Save the file with numpy.savetxt
np.savetxt(fdets_ofn, np.array(fdets_gc), newline="\n")
'''