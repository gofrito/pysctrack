#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May  8 13:21:04 2020

@author: gofrito
"""
import os
import requests

from colorama import Back, Fore, Style
from . import converter, core, handler

__version__ = "0.8.3"
__all__ = ['converter', 'core', 'handler', 'sctraker_ini', 'spacecraft']

def get_cats():
    """
    Get necessary catalogs and ephemerides from Git repo
    :return:
    """
    try:
        _homedir = os.environ['HOME']
        _cache_dir = os.path.join(_homedir, '.pysctrack')

        # should be unnecessary
        if not os.path.exists(_cache_dir):
            os.makedirs(_cache_dir)

        # fetch catalogs
        fetch_cats(_cache_dir)

    except Exception as e:
        print(f'{Fore.LIGHTRED_EX}Failed to update catalogs and ephemerides: {e}{Style.RESET_ALL}')


def fetch_cats(_cache_dir):
    """
    Fetch necessary catalogs and ephemerides from Git repo
    :return:
    """
    _cats_dir = os.path.join(_cache_dir, 'cats')
    if not os.path.exists(_cats_dir):
        os.makedirs(_cats_dir)

    base_url = 'https://gitlab.com/gofrito/pysctrack/-/tree/master/cats/'

    cats = ['OffsetsNone.txt',
            'antenna_codes.txt ',
            'glo.sit',
            'ns_codes.dat',
            'glo.crd',
            'glo.sit',
            'glo.vel',
            'ns-codes.txt',
            'mex_param.cat',
            'naif_codes.dat',
            'sc_param.cat',
            'scint_template.txt',
            'scka_param.dat',
            'scx_param.dat',
            'TecMars.NFSn720.txt',
            'TecVenus.NFSn720.txt',
            'inifile.Xx',
            'inifileXx',
            'OffsetsNone.txt']

    for cat in cats:
        try:
            url = os.path.join(base_url, cat)

            r = requests.get(url)

            if r.status_code == 200:
                with open(os.path.join(_cats_dir, cat), 'wb') as f:
                    f.write(r.content)
                    print(f'{Fore.BLUE}Fetched {url}{Style.RESET_ALL}')
            else:
                print(f'{Fore.LIGHTRED_EX}Failed to fetch {url}{Style.RESET_ALL}')

        except Exception as e:
            print(f'{Fore.LIGHTRED_EX}Failed to fetch {url}: {e}{Style.RESET_ALL}')

homedir = os.environ['HOME']

cache_dir = os.path.join(homedir, '.pysctrack')
cats_dir = os.path.join(cache_dir, 'cats')

if not os.path.exists(cache_dir):
    os.makedirs(cache_dir)

    # fetch catalogs on first run
    fetch_cats(cache_dir)

    # make aux dirs:
    ''' Not necessary at the moment'''
