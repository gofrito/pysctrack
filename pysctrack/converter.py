#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat May  9 07:59:22 2020

Library of functions for analysis of scintillation
@author: gofrito
"""

import numpy as np
from astropy.time import Time


def cart2sph(xyz):
    """
    Converts cartesian to spherical coordenates

    Parameters
    ----------
    xyz : list
        3 list elements for cartesian coordenates.

    Returns
    -------
    rpt : array
        coordenates in spherical.

    """
    if type(xyz) != type(np.zeros(3)):
        xyz = np.array(xyz)
    rpt = np.zeros(xyz.shape)
    try:
        xy = xyz[:, 0] ** 2 + xyz[:, 1] ** 2
        rpt[:, 0] = np.sqrt(xy + xyz[:, 2] ** 2)  # rho
        rpt[:, 1] = np.arctan2(xyz[:, 2], np.sqrt(xy))  # phi ('elevation')
        rpt[:, 2] = np.arctan2(xyz[:, 1], xyz[:, 0])  # theta ('azimuth')
    except (IndexError):  # 1d-array case
        xy = xyz[0] ** 2 + xyz[1] ** 2
        rpt[0] = np.sqrt(xy + xyz[2] ** 2)  # rho
        rpt[1] = np.arctan2(xyz[2], np.sqrt(xy))  # phi ('elevation')
        rpt[2] = np.arctan2(xyz[1], xyz[0])  # theta ('azimuth')
    return rpt


def sph2cart(rpt):
    """
    Converts spherical to cartesian coordenates

    Parameters
    ----------
    rpt : array
        coordenates in spherical (1 vector 3 elements).

    Returns
    -------
    xyz : list
        3 list elements.

    """
    if type(rpt) != type(np.zeros(3)):
        rpt = np.array(rpt)
    xyz = np.zeros(rpt.shape)
    try:
        xyz[:, 0] = rpt[:, 0] * np.cos(rpt[:, 1]) * np.cos(rpt[:, 2])
        xyz[:, 1] = rpt[:, 0] * np.cos(rpt[:, 1]) * np.sin(rpt[:, 2])
        xyz[:, 2] = rpt[:, 0] * np.sin(rpt[:, 1])
    except (IndexError):  # 1d-array case
        xyz[0] = rpt[0] * np.cos(rpt[1]) * np.cos(rpt[2])
        xyz[1] = rpt[0] * np.cos(rpt[1]) * np.sin(rpt[2])
        xyz[2] = rpt[0] * np.sin(rpt[1])
    return xyz


def sot2pho(dist, sot, s):
    """
    Convert the Solar-Observer-Target angle to Phase orbital. You can select the body (s)
    and it uses different values for the R and dturn

    Parameters
    ----------
    dist : float
        Distance between the target and the observer.
    sot : float
        Solar-Observer-Target angle.
    s : body
        {v} is for Venus
        {m} is for Mars

    Returns
    -------
    pho : float
        Returns the Phase orbital value.

    """
    ns = np.size(dist)
    if s == "v":
        R = 0.723
        dturn = 0.719
    elif s == "m":
        R = 1.524
        dturn = 0
    else:
        R = 0.000
        dturn = 0

    beta = np.arccos((np.power(R, 2) + 1 - np.power(dist, 2)) / (2 * R)) * 180 / np.pi
    gama = np.arcsin((1 / R * np.sin(np.multiply(sot, np.pi) / 180))) * 180 / np.pi

    pho = 180 - gama - sot
    phos = np.zeros(ns)

    for ip in np.arange(ns):
        if dist[ip] > dturn:
            phos[ip] = pho[ip]
        else:
            phos[ip] = beta[ip]

    pho = 180 - phos
    return pho


def sot2gop(dist, sot, s):
    """
    Convert the Solar-Observer-Target to Geocentric Orbital Phase. Function is used
    to display TEC and IPS as a function of the Phase Geocentric Orbit with
    respect to the Earth position.

    Parameters
    ----------
    dist : array_like
        distance observer-target.
    sot : float
        solar-observer-target angle.
    s : str
        select body in use

    Returns
    -------
    phi : float
        angle of the Phase geocentric orbit.

    """
    ns = np.size(dist)
    if s == "v":
        R = 0.723
        dturn = 0.719
    elif s == "m":
        R = 1.524
        dturn = 0
    else:
        dturn = 0

    phi = np.zeros(ns)
    gam = np.zeros(ns)

    for ip in np.arange(ns):
        phi[ip] = np.arcsin(np.multiply(dist[ip], np.sin(np.deg2rad(sot[ip]))) / R)
        gam[ip] = np.pi - np.arcsin(dist[ip] * np.sin(np.deg2rad(sot[ip])) / R)

    for ip in np.arange(ns):
        if dist[ip] < dturn:
            phi[ip] = gam[ip]

    phi = np.rad2deg(phi)

    return phi


def ra2rad(ra):
    """
    Convert right ascension in hours, minutes seconds to angle in radians

    Parameters
    ----------
    ra : list [3 elements]
        3 elements [hours,minutes,seconds].

    Returns
    -------
    alpha : float
        return the right ascension in radians.

    """
    hh = np.float(ra[0]) * 15
    mm = np.float(ra[1]) / 60 * 15
    ss = np.float(ra[2]) / 3600 * 15

    alpha_deg = hh + mm + ss
    alpha = np.deg2rad(alpha_deg)

    return alpha


def dec2rad(dec):
    """
    Convert declination to angle in radians

    Parameters
    ----------
    dec : list [3 elements]
        degrees minutes seconds.

    Returns
    -------
    declination : float
        return the declination in radians.

    """
    hh = np.abs(np.float(dec[0]))
    mm = np.float(dec[1]) / 60
    ss = np.float(dec[2]) / 3600

    decdeg = hh + mm + ss
    if float(dec[0]) < 0:
        decdeg = -decdeg

    declination = np.deg2rad(decdeg)

    return declination


def gd2mjd(yy, mm, dd, HH, MM, SS):
    """
    Converts Gregorian date to Modified Julian Date
    https://docs.astropy.org/en/stable/time/
    Parameters
    ----------
    yy : TYPE
        DESCRIPTION.
    mm : TYPE
        DESCRIPTION.
    dd : TYPE
        DESCRIPTION.
    HH : TYPE
        DESCRIPTION.
    MM : TYPE
        DESCRIPTION.
    SS : TYPE
        DESCRIPTION.

    Returns
    -------
    jd : TYPE
        DESCRIPTION.

    """
    jdn = 367 * yy - (7 * (yy + 5001 + (mm - 9) / 7)) / 4 + (275 * mm) / 9 + dd + 1729777
    jd = jdn + (HH - 12) / 24 + MM / 1440 + SS / 86400

    # if using astropy

    # jd = t.mjd
    return jd


def mjd2jd(mjd):
    """
    Converts Modified Julian Date to Gregorian Date
    For convenience, use astropy package
    Parameters
    """
    t = Time(mjd, format='mjd', scale='utc')

    # Use the jd function to convert to Julian Date
    return t.jd


def mjd2gd(mjd):
    """
    Converts Modified Julian Date to Gregorian Date
    For convenience, use astropy package
    Parameters
    """
    t = Time(mjd, format='mjd', scale='utc')

    # Using function ymdhms to return year, month, day
    return (t.ymdhms[0], t.ymdhms[1], t.ymdhms[2])

    # Use the jd function to convert to Julian Date
    return t.jd


def mjuliandate(year, month, day, hour=0, minu=0, sec=0):
    """
    Calculate Modified Julian Date
    """
    if month <= 2:  # January & February
        year -= 1
        month += 12
    jd = np.floor(365.25 * (year + 4716)) + np.floor(30.6001 * (month + 1)) + 2 - np.floor(year / 100) + np.floor(np.floor(year / 100) / 4) + day - 1524.5 + (hour + minu / 60 + sec / 3600) / 24
    mjd: float = jd - 2400000.5

    return mjd


def freq2vel(f_nom, doppler):
    """
    Converts frequency shift to radial velocity

    Parameters
    ----------
    f_nom : TYPE
        DESCRIPTION.
    doppler : TYPE
        DESCRIPTION.

    Returns
    -------
    stdnoise : TYPE
        DESCRIPTION.

    """
    stdnoise: float = doppler * 3e8 / f_nom

    return stdnoise


def sec2hmi(seconds):
    hours: int = int(np.floor(seconds / 3600))
    minutes: int = int(np.floor((seconds - 3600 * hours) / 60))
    secs: int = seconds - hours * 3600 - minutes * 60

    return hours, minutes, secs
