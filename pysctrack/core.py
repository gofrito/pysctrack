#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May  8 06:11:43 2020

calculate various polynomials fits for phase delay compensation on spacecraft
@author: gofrito
"""

from __future__ import absolute_import

import numpy as np

def ChePolyfit(x, y, n: int):
    """
    Calculate the Chebishev Polynomial Fit to a set of Doppler measurements

    Parameters
    ----------
    x : array_like
        time sample points in a given scan
    y : array_like
        Doppler frequency detections in a given scan
    n : int
        Degree of the fitting polynomial

    Returns
    -------
    yf : array_like
        Return the residuals after a polynomial fit of degree n

    """
    nx = len(x)
    xx = np.max(x)
    xy = np.min(x)
    xc = 0.5 * (xx + xy)
    xa = 0.5 * (xx - xy)
    xn = (x - xc) / xa

    Vp = np.zeros(n + 1)
    Mp = np.zeros((n + 1, n + 1))
    yf = np.zeros(nx)

    for ip in np.arange(n + 1):
        Vp[ip] = np.sum(np.Tcheb(ip, xn) * y)
        for jp in range(n + 1):
            Mp[ip, jp] = Mp[ip, jp] + np.sum(np.Tcheb(ip, xn) * np.Tcheb(jp, xn))

    Mr = np.linalg.pinv(Mp)
    Cp = np.dot(Mr, Vp.T)

    for ip in range(nx):
        for jp in range(n):
            yf[ip] = yf[ip] + np.Tcheb(jp, xn[ip]) * Cp[jp]

    return yf


def ChePolyfitW(x, y, w, n):
    """
    Calculate the Chebishev Weighted Polynomial Fit to a set of Doppler measurements

    Parameters
    ----------
    x : array_like
        time sample points in a given scan
    y : array_like
        Doppler frequency detections in a given scan
    w : array_like
        Weights to apply to the y-coordinates of the sample points.
    n : int
        Degree of the fitting polynomial

    Returns
    -------
    yf : array_like
        Return the residuals after a polynomial fit of degree n
    """
    nx = len(x)
    xx = np.max(x)
    xy = np.min(x)
    xc = 0.5 * (xx + xy)
    xa = 0.5 * (xx - xy)
    xn = (x - xc) / xa

    Vp = np.zeros(n + 1)
    Mp = np.zeros((n + 1, n + 1))
    yf = np.zeros(nx)

    for ip in np.arange(n + 1):
        Vp[ip] = np.sum(np.Tcheb(ip, xn) * y * w)
        for jp in range(n + 1):
            Mp[ip, jp] = Mp[ip, jp] + np.sum(np.Tcheb(ip, xn) * np.Tcheb(jp, xn) * w)

    Mr = np.linalg.pinv(Mp)
    Cp = np.dot(Mr, Vp.T)

    for ip in range(nx):
        for jp in range(n + 1):
            yf[ip] = yf[ip] + np.Tcheb(jp, xn[ip]) * Cp[jp]

    return yf


def DeWrap(ph, nargouts=1):
    """
    Phase de wrap

    Parameters
    ----------
    ph : TYPE
        DESCRIPTION.
    nargouts : TYPE, optional
        DESCRIPTION. The default is 1.

    Returns
    -------
    phc : array_like
        de-wrapped phase.

    """
    nph = len(ph)
    dph = np.zeros(nph)
    qph = np.zeros(nph)
    phc = np.zeros(nph)

    for ip in np.arange(1, nph):
        tmp = ph[ip] - ph[ip - 1]
        if np.abs(tmp) < np.pi:
            dph[ip] = 0
        else:
            dph[ip] = np.sign(ph[ip] - ph[ip - 1])

    for ip in np.arange(1, nph):
        qph[ip] = qph[ip - 1] + dph[ip]

    phc = ph - 2 * np.pi * qph
    return phc


def FindMax(Spec: float, Fscale: float):
    """
    Find the peak and determine its weighted frequency bin

    Parameters
    ----------
    Spec : array_like
        Spectral power
    Fscale : array_like
        Frequency series

    Returns
    -------
    out : vector
        Returns  bin position, averaged frequency position, maximum spectral power measurement

    """
    max_spec: float = Spec.max()
    xbin: int = Spec.argmax()

    max_bin: int = len(Spec) - 1

    '''
    Avoid errors when max is at the edge
    Replaced -1 for max_bin - 1
    '''
    if xbin == 0:
        xbin = 1
    elif xbin == max_bin:
        xbin = max_bin - 1

    a: float = 0.5 * (Spec[xbin + 1] - Spec[xbin - 1])
    b: float = Spec[xbin + 1] + Spec[xbin - 1] - 2 * Spec[xbin]

    # Calculate the center of mass
    centroid_bin: float = xbin - a / b

    out = (xbin, centroid_bin, max_spec)
    return out

def GetSNR(ts: float, efficiency: float, dish: float, Tsys: float, Pt: float, Gt: float, f: float, R: float):
    '''
    Function based on Pogrebenko et al. 2004
    Parameters
    ----------
    ts : float
        integration time [s]
    efficiency : float
        antenna efficiency nominal value 60%
    dish : float
        antenna diameter [m]
    Tsys : float
        system temperature [K]
    Pt : float
        craft transmitting power [W]
    Gt : float
        craft antenna gain [unitless]
    f : float
        broadband ratio [Hz]
    R : float
        distance to target [m]
    '''

    # Boltzmann constant
    kb: float = 1.380640e-23    # j/K

    # Calculate the SNR
    SNR: float = ts * efficiency * np.power(dish, 2) * Pt * Gt / (4 * kb * Tsys * 4 * f * np.power(R, 2))

    return SNR

def GetRMS(Spec: float, Fdet: float, Hwin: int, Fvoid: int):
    """
    Parameters
    ----------
    Spec : array_like
        Array with the zoomed spectra
    Fdet : float
        Frequency measurement of the peak
    Hwin : int
        Frequency window to calculate the RMS.
    Fvoid : int
        Frequency range to avoid.

    Returns
    -------
    out : vector
        mm: mean value in the window
        rm: rms value in the value
        ww: lenght of the window
    """
    fwin_min: int = int(Fdet - Hwin)
    fwin_max: int = int(Fdet + Hwin)
    fvoid_min: int = int(Fdet - Fvoid)
    fvoid_max: int = int(Fdet + Fvoid)

    # Calculate the total sum of noise
    total_noise: float = Spec[fwin_min : fvoid_min].sum() + Spec[fvoid_max : fwin_max].sum()

    # Calculate the frequency bandwidth
    bandwidth: float = 2 * (Hwin - Fvoid)

    # Calculate the noise mean
    mean_noise: float = total_noise / bandwidth

    # Calculate the variance of upper and lower power spectra
    var_pl: float = np.power(Spec[fwin_min : fvoid_min] - mean_noise, 2)
    var_pu: float = np.power(Spec[fvoid_max : fwin_max] - mean_noise, 2)

    # Total power spectra mean square of the noise
    total_var_noise: float = var_pl.sum() + var_pu.sum()

    # Root mean square
    rms: float = np.sqrt(total_var_noise / bandwidth)

    out = (mean_noise, rms, bandwidth)
    return out

def GetRMSv2(Spec: float, Fdet: float, Hwin: int, Fvoid: int):
    """
    Parameters
    ----------
    Spec : array_like
        Array with the zoomed spectra
    Fdet : float
        Frequency measurement of the peak
    Hwin : int
        Frequency window to calculate the RMS.
    Fvoid : int
        Frequency range to avoid.

    Returns
    -------
    out : vector
        mm: mean value in the window
        rm: rms value in the value
        ww: lenght of the window
    """
    fwin_min: int = int(Fdet - Hwin)
    fwin_max: int = int(Fdet + Hwin)
    fvoid_min: int = int(Fdet - Fvoid)
    fvoid_max: int = int(Fdet + Fvoid)

    # Calculate the total sum of noise
    total_noise: float = Spec[fwin_min : fvoid_min].sum() + Spec[fvoid_max : fwin_max].sum()

    # Calculate the frequency bandwidth
    bandwidth: float = 2 * (Hwin - Fvoid)

    # Calculate the noise mean
    mean_noise: float = total_noise / bandwidth

    # Calculate the variance of upper and lower power spectra
    var_pl: float = Spec[fwin_min : fvoid_min].variance()
    var_pu: float = Spec[fvoid_max : fwin_max].variance()

    # Total power spectra mean square of the noise
    total_var_noise: float = var_pl.sum() + var_pu.sum()

    # Root mean square
    rms: float = np.sqrt(total_var_noise / bandwidth)

    out = (mean_noise, rms, bandwidth)
    return out

def GetRMSf(Spec: float, Fscale: float, Fdet: float, Hwin: int, Fvoid: int):
    """
    Parameters
    ----------
    Spec : TYPE
        DESCRIPTION.
    Fscale : TYPE
        DESCRIPTION.
    Fdet : float
        DESCRIPTION.
    Hwin : int
        DESCRIPTION.
    Fvoid : int
        DESCRIPTION.

    Returns
    -------
    out : TYPE
        DESCRIPTION.

    """
    ns = len(Spec)
    mw = 0
    ww = 0
    dw = 0

    for ip in np.arange(ns):
        if (np.abs(Fscale[ip] - Fdet) > Fvoid) and (np.abs(Fscale[ip] - Fdet) < Hwin):
            ww = ww + 1
            mw = mw + Spec[ip]

    mm = mw / ww

    for ip in np.arange(ns):
        if (np.abs(Fscale[ip] - Fdet) > Fvoid) and (np.abs(Fscale[ip] - Fdet) < Hwin):
            dw = dw + np.power(Spec[ip] - mm, 2)

    rm = np.sqrt(dw / ww)
    out = (mm, rm, ww)
    return out

def make_spec(filename, num_spectra, num_fft, fft_integration, overlap, \
              window, padding, padding_vector, data_format=np.complex64):
    """
    Calculate the spectra from a given file

    Parameters
    ----------
    filename : string
        The spectra recorded in binary format from the tone file
    num_spectra : int
        Number of averaged spectrum to perform
    num_fft : int
        The number of spectral points to use for the transformation
    fft_integration : int
        Number of FFT to integrate
    overlap: int
        Overlap factor for the FFT
    window : array_like
        Spectral window to apply in the FFT.
    padding : int
        Padding factor on the FFT.
    dpadd : array_like
        padding array.

    Returns
    -------
    spa : array_like
        returns the array of averaged windowed-overlapped spectra.

    """
    # Number of spectral points
    num_fft_points: int = int(num_fft * padding / 2 + 1)

    # Initialise the array
    spectra: np.double = np.zeros((num_spectra, num_fft_points))

    # Number of samples accounting for overlap
    # Error fix 23.03.23 - It was skipping last FFT to average, removed -> -(overlap - 1)
    num_samples_average: int = int(fft_integration * overlap)

    # Segment blocks of data to read
    block_average: int = fft_integration * num_fft

    # Double check that we can read only count=block_average * num_spectra
    #data_in: np.complex64 = np.fromfile(file=filename, dtype=data_format, \
    #                                    count=int(block_average * (num_spectra + 1) + 1))
    data_in = np.memmap(filename, dtype=data_format)

    # Loop of spectra to iterate
    for ip in np.arange(num_spectra):
        # Numbler of segments to average
        for jp in np.arange(num_samples_average):
            # Data is in a vector -> start & stop bins
            init_bin: int = int(block_average * ip + jp * num_fft / overlap)
            stop_bin: int = int(init_bin + num_fft)

            # Multiply data segment by complex exponent
            data_block: np.complex64 = data_in[init_bin : stop_bin]

            # Apply window function
            win_data: np.complex64 = data_block * window

            # Add Padding if necessary
            if padding > 1:
                win_data_fft: np.complex64 = np.append(win_data, padding_vector)
            else:
                win_data_fft: np.complex64 = win_data

            # Calculate the FFT
            spectrum: np.complex64 = np.fft.fft(win_data_fft)

            # Accumulate spectra and store it into an array
            spectra[ip, :] += np.power(np.real(spectrum[0 : num_fft_points]), 2) + \
                                np.power(np.imag(spectrum[0 : num_fft_points]), 2)

    return spectra


def make_spec_phase_corr(filename, num_spectra, nfft, fft_integration, overlap, window, padding, padding_vector, phase_corr, data_format=np.complex64):
    """
    Calculate the spectra from a given file and
    apply phase correction to the input signal

    Parameters
    ----------
    filename : string
        The spectra recorded in binary format from the tone file
    num_spectra : int
        Number of averaged spectrum to perform
    nfft : int
        The number of spectral points to use for the transformation
    fft_integration : int
        Number of FFT to integrate
    overlap: int
        Overlap factor for the FFT
    window : array_like
        Spectral window to apply in the FFT.
    padding : int
        Padding factor on the FFT.
    dpadd : array_like
        padding array.
    phase_corr: array_like
        phase correction to apply

    Returns
    -------
    spectra : array_like
        returns the array of averaged windowed-overlapped spectra.
    """
    # Number of spectral points
    num_fft_points: int = int(nfft * padding / 2 + 1)

    # Initialise the array
    spectra: np.double = np.zeros((num_spectra, num_fft_points))

    # Number of samples accounting for overlap
    # Error fix 23.03.23 - It was skipping last FFT to average, removed -> -(overlap - 1)
    num_samples_average: int = int(fft_integration * overlap)

    # Segment blocks of data to read
    block_average: int = fft_integration * nfft

    # Read data
    data_in: np.complex64 = np.fromfile(file=filename, dtype=data_format, count=int(block_average * (num_spectra + 1) + 1))

    # Loop of spectra to iterate
    for ip in np.arange(num_spectra):
        # Numbler of segments to average
        for jp in np.arange(num_samples_average):

            # Data is in a vector -> start & stop bins
            init_bin: int = int(block_average * ip + jp * nfft / overlap)
            stop_bin: int = int(init_bin + nfft)

            # Read the correspondent correction phase for the data segment
            exp_phase_corr: np.complex64 = np.exp(-1j * phase_corr[init_bin : stop_bin])

            # Multiply data segment by complex exponent
            data_block: np.complex64 = data_in[init_bin : stop_bin] * exp_phase_corr

            # Apply window function
            win_data: np.complex64 = data_block * window

            # Add Padding if necessary
            if padding > 1:
                win_data_fft: np.complex64 = np.append(win_data, padding_vector)
            else:
                win_data_fft: np.complex64 = win_data

            # Calculate the FFT
            spectrum: np.complex64 = np.fft.fft(win_data_fft)

            # Accumulate and store in the array
            spectra[ip, :] += np.power(np.real(spectrum[0 : num_fft_points]), 2) + np.power(np.imag(spectrum[0 : num_fft_points]), 2)

    return spectra


def MakeFiltX(filename, phase_corr, Fbinstart, Fbinend, Es, Nto, block_average, num_spectra, input_window, output_window, num_segments, Npsi, Npso, overlap, data_format=np.complex64):
    """
    Core of the Phase-Locked Loop function. It conducts the filtering, phase stop and phase delay compensation
    Replaced float32 and convert to complex, by data type complex64
    Parameters
    ----------

    Returns
    -------
    fout : array_like
        Returns the carrier signal (time domain) after phase stop and compensation.

    """
    # Create output array
    fout: np.complex64 = np.zeros(Nto, dtype=np.complex64)

    # Number of output FFT points
    Npfo: int = int(Npso / 2)

    Oshifti: int = int(Npsi / overlap)
    Oshifto: int = int(Npso / overlap)

    # Padding array
    dpadd: int = np.zeros(Npfo - 1)

    # Read the data and copy it to an array
    data_in: np.complex64 = np.fromfile(file=filename, dtype=data_format, count=int(block_average * (num_spectra + 1) + 1))

    # Loop through all the segments
    for ip in np.arange(start=0, stop=num_segments):

        # Data is in a vector -> start & stop bins
        init_bin: int = int(ip * Oshifti)
        stop_bin: int = int(init_bin + Npsi)

        # Cut a segment of data
        data_block: np.complex64 = data_in[init_bin : stop_bin]

        # Apply window function
        win_data: np.complex64 = data_block * input_window

        # Read the correspondent correction phase for the data segment
        exp_phase_corr: np.complex64 = np.exp(-1j * phase_corr[init_bin : stop_bin])

        # Apply phase correction
        win_data *= exp_phase_corr

        # Calculate the FFT
        spectrum: np.complex64 = np.fft.fft(win_data)

        # We do the filtering around freq start and stop bins
        spo = spectrum[Fbinstart : Fbinend]

        # Supress manually DC components
        spo[0] = np.real(spo[0])
        spo[-1] = np.real(spo[-1])

        # Stack a Padding array at the end to improve granuality
        spop = np.append(spo, dpadd)

        # After filtering reconvert to time domain
        out = np.fft.ifft(spop)

        # Apply the output window
        dout = out * output_window

        # Hilbert transform
        for jp in np.arange(Npso):
            fout[jp + ip * Oshifto] += np.multiply(dout[jp], np.power(Es, ip))
    return fout

def PolyfitW(x, y, w, n: int):
    """
    Calculate the Weighted Averaged Polynomial Fit to a set of Doppler measurements

    Parameters
    ----------
    x : array_like
        time sample points in a given scan
    y : array_like
        Doppler frequency detections in a given scan
    w : array_like
        Weights to apply to the y-coordinates of the sample points.
    n : int
        Degree of the fitting polynomial

    Returns
    -------
    yf : array_like
        Return the residuals after a polynomial fit of degree n

    """
    nx = len(x)
    xx = x.max()
    xy = x.min()
    xc = 0.5 * (xx + xy)
    xa = 0.5 * (xx - xy)
    xn = (x - xc) / xa

    Vp = np.zeros(n + 1)
    Mp = np.zeros((n + 1, n + 1))
    yf = np.zeros(nx)

    for jp in np.arange(n + 1):
        Vp[jp] = np.sum(y * w * np.power(xn, jp))
        for ip in np.arange(n + 1):
            Mp[jp, ip] = np.sum(np.power(xn, jp + ip) * w)

    Mr = np.linalg.pinv(Mp)
    Cp = np.dot(Mr, Vp)

    for ip in np.arange(nx):
        for jp in np.arange(n + 1):
            yf[ip] = yf[ip] + np.power(xn[ip], jp) * Cp[jp]

    return yf

def PolyfitW1(x, y, w, n):
    """
    Calculate the Weighted Polynomial Fit to a set of Doppler measurements
    Does not include sample averaging

    Parameters
    ----------
    x : array_like
        time sample points in a given scan
    y : array_like
        Doppler frequency detections in a given scan
    w : array_like
        Weights to apply to the y-coordinates of the sample points.
    n : int
        Degree of the fitting polynomial

    Returns
    -------
    yf : array_like
        Return the residuals after a polynomial fit of degree n

    """
    nx = len(x)
    xx = x.max()
    xn = x / xx

    Vp = np.zeros(n + 1)
    Mp = np.zeros((n + 1, n + 1))
    yf = np.zeros(nx)

    for jp in np.arange(n + 1):
        Vp[jp] = np.sum(y * w * np.power(xn, jp))
        for ip in np.arange(n + 1):
            Mp[jp, ip] = np.sum(np.power(xn, jp + ip) * w)

    Mr = np.linalg.pinv(Mp)
    Cp = np.dot(Mr, Vp)

    for ip in np.arange(nx):
        for jp in np.arange(n + 1):
            yf[ip] = yf[ip] + Cp[jp] * np.power(xn[ip], jp)

    return yf


def PolyfitWC(x, y, w, n):
    """
        Calculate the Polynomial Coefficients to an Averaged fit a set of Doppler measurements

    Parameters
    ----------
    x : array_like
        time sample points in a given scan
    y : array_like
        Doppler frequency detections in a given scan
    w : array_like
        Weights to apply to the y-coordinates of the sample points.
    n : int
        Degree of the fitting polynomial

    Returns
    -------
    yf : array_like
        Return the polynomial coefficients after a polynomial fit of degree n
    """
    xx = np.max(x)
    xy = np.min(x)
    xc = 0.5 * (xx + xy)
    xa = 0.5 * (xx - xy)
    xn = (x - xc) / xa

    Vp = np.zeros(n + 1)
    Mp = np.zeros((n + 1, n + 1))

    for jp in np.arange(n + 1):
        Vp[jp] = np.sum(y * w * np.power(xn, jp))
        for ip in np.arange(n + 1):
            Mp[jp, ip] = np.sum(np.power(xn, jp + ip) * w)

    Mr = np.linalg.pinv(Mp)
    Cp = np.dot(Mr, Vp)
    return Cp.T


def PolyfitW1C(x, y, w, n):
    """
        Calculate the Polynomial Coefficients to fit a set of Doppler measurements

    Parameters
    ----------
    x : array_like
        time sample points in a given scan
    y : array_like
        Doppler frequency detections in a given scan
    w : array_like
        Weights to apply to the y-coordinates of the sample points.
    n : int
        Degree of the fitting polynomial

    Returns
    -------
    yf : array_like
        Return the polynomial coefficients after a polynomial fit of degree n
    """
    xn = x / np.max(x)

    Vp = np.zeros(n + 1)
    Mp = np.zeros((n + 1, n + 1))

    for jp in np.arange(n + 1):
        Vp[jp] = np.sum(y * w * np.power(xn, jp))
        for ip in np.arange(n + 1):
            Mp[jp, ip] = np.sum(np.power(xn, jp + ip) * w)

    Mr = np.linalg.pinv(Mp)
    Cp = np.dot(Mr, Vp)

    return Cp

def smooth_hanning(vector, box):
    """
    Smooth the data as Sergei's implementation
    """
    nd = len(vector)
    dh = np.zeros(nd)

    dh[0] = 0.5 * (vector[0] + vector[1])
    dh[-1] = 0.5 * (vector[-2] + vector[-1])

    for ip in range(1, nd-1):
        dh[ip] = 0.5 * (0.5 * vector[ip-1] + vector[ip] + 0.5 * vector[ip+1])

    return dh

def PowCenter(Spec, Xm, Nx):
    """
    Parameters
    ----------
    Spec : float
        spectrum.
    Xm : int
        bin number found by FindMax.
    Nx : int
        half-width of the integration window - default 3

    Returns
    -------
    dxo : float
        Bin position of the peak based on the spectra centroid.

    """
    xo: int = round(Xm)
    mp: float = Spec[xo - Nx : xo + Nx + 1].sum()
    wp: float = Spec[xo - Nx : xo + Nx + 1] * (np.arange(xo - Nx, xo + Nx + 1) - Xm)
    dxo: float = wp.sum() / mp

    return dxo


"""
#==============================================================================
# Linear extrapolator
#==============================================================================
"""


def extrap(x, xp, yp, interp_type="linear"):
    """np.interp function with linear extrapolation"""
    # convert to numpy arrays if necessary:
    if type(x).__module__ != np.__name__:
        x = np.array(x)
    if type(xp).__module__ != np.__name__:
        xp = np.array(xp)
    if type(yp).__module__ != np.__name__:
        yp = np.array(yp)
    if interp_type == "linear":
        y = np.interp(x, xp, yp)
    elif interp_type == "lag":
        y, _ = lagint(min(len(xp), 15), xp, yp, x)
    elif interp_type == "cheb":
        p = np.polynomial.chebyshev.chebfit(xp, yp, min(len(xp), 15))
        y = np.polynomial.chebyshev.chebval(x, p)
    # linear fit outside:
    y[x < xp[0]] = yp[0] + (x[x < xp[0]] - xp[0]) * (yp[0] - yp[1]) / (xp[0] - xp[1])
    y[x > xp[-1]] = yp[-1] + (x[x > xp[-1]] - xp[-1]) * (yp[-1] - yp[-2]) / (xp[-1] - xp[-2])
    return y


"""
#==============================================================================
# Use interpolator as exptrapolator
#==============================================================================
"""

def extrap1d(interpolator):
    xs = interpolator.x
    ys = interpolator.y

    def pointwise(x):
        if x < xs[0]:
            return ys[0] + (x - xs[0]) * (ys[1] - ys[0]) / (xs[1] - xs[0])
        elif x > xs[-1]:
            return ys[-1] + (x - xs[-1]) * (ys[-1] - ys[-2]) / (xs[-1] - xs[-2])
        else:
            return interpolator(x)

    def ufunclike(xs):
        return np.array(list(map(pointwise, np.array(xs))))

    return ufunclike


def lagint(npo, xi, yi, xo):
    """
     Lagrange interpolation
    :param npo: number of points to use = order + 1
    :param xi: input x's
    :param yi: input y's
    :param xo: interpolation point(s)
    :return:
    """
    l = len(xo) if hasattr(xo, "__len__") else 1
    m = len(xi)

    # np - number of points = order + 1
    # order of the interpolant must be <= m
    if npo > m:
        npo = m

    wl = int(np.floor(npo / 2))
    wr = int(np.ceil(npo / 2))

    y = np.empty(l)
    dy = np.empty(l)

    for k in range(l):
        xok = xo[k] if hasattr(xo, "__len__") else xo
        # nearest nod in the grid right to xo, number of elements left to xo:
        nl = int(np.searchsorted(xi, xok))
        # number of elements right to xo
        nr = m - nl
        # cut npo points around xo
        if wl > nl:
            xin = xi[0:npo]
            yin = yi[0:npo]
        elif wr > nr:
            xin = xi[m - npo :]
            yin = yi[m - npo :]
        else:
            xin = xi[nl - wl : nl + wr]
            yin = yi[nl - wl : nl + wr]

        # Perform the Lagrange interpolation with the Aitken method. y: interpolated value. dy: error estimated.
        for i in range(1, npo + 1):
            for j in range(1, npo - i + 1):
                xi1 = xin[j - 1]
                xi2 = xin[j + i - 1]
                fi1 = yin[j - 1]
                fi2 = yin[j]
                # print(i, j, '\t', xi1, xi2, fi1, fi2)
                yin[j - 1] = (xok - xi1) / (xi2 - xi1) * fi2 + (xok - xi2) / (xi1 - xi2) * fi1

        y[k] = yin[0]
        dy[k] = (abs(y[k] - fi1) + abs(y[k] - fi2)) / 2.0

    return y, dy
