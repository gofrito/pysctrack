from xml.etree.ElementTree import Element, SubElement, ElementTree, tostring
import xml.dom.minidom
import hashlib
import os

import numpy as np
import pandas as pd
from pathlib import Path

from datetime import datetime, timezone

BUNDLE: str = 'juice_pride'

class xmlClass:

    def __init__(self, file, version, description):
        self.time = datetime.now()
        self.input_file = file
        self.output_file = str(Path(file).with_suffix('.lblx'))
        self.version = version
        self.description = description

    def calculate_vars (self):
        with open(self.input_file, 'rb') as f:
            file_contents = f.read()
            total_len = len(file_contents)

            hash_object = hashlib.md5(file_contents)
            self.md5 = hash_object.hexdigest()

        self.size: str = str(os.stat(self.input_file).st_size)
        mod_time = os.path.getmtime(self.input_file)
        m_time = datetime.fromtimestamp(mod_time, tz=timezone.utc)
        cre_time = os.path.getctime(self.input_file)
        c_time = datetime.fromtimestamp(cre_time, tz=timezone.utc)
        self.created_date = c_time
        self.modified_date: str = f'{m_time.year}-{int(m_time.month):02d}-{int(m_time.day):02d}'
        self.created_date: str = f'{c_time.year}-{int(c_time.month):02d}-{int(c_time.day):02d}'
        self.year_created: str = f'{c_time.year}'

        # Calculate records as This is the total number of records in the table. Note that in a Table_Character table, each record
        #including the last must have a carriage-return/linefeed record delimiter at the end.
        with open(self.input_file, 'rb') as f:
            next(f); next(f); next(f); next(f)
            data_len = len(f.read())
            header_len = total_len - data_len

        self.data_len = data_len
        self.header_len = header_len

        if '00' not in self.input_file:
            ab  = pd.read_csv(self.input_file, skiprows=4, sep='\s+', \
                                names=['index', 'time', 'snr', 'spectra', 'doppler', 'residuals'])
        else:
            ab = pd.read_csv(self.input_file, skiprows=4, sep='\s+', \
                                names=['time', 'snr', 'spectra', 'doppler', 'residuals'])
        self.start_time = ab['time'].iloc[0]
        self.stop_time = ab['time'].iloc[-1]
        self.rows, self.columns = ab.shape

        sc = self.input_file.split('.')[1]

        if sc[:3] == 'jui':
            self.spacecraft = 'JUICE'

    def phase_columns(self, root):
        table_character = SubElement(root, "Table_Character")

        SubElement(table_character, "name").text = "Residual phase"
        SubElement(table_character, "offset").text = str(self.header_len)
        SubElement(table_character, "records").text = str(self.rows)
        SubElement(table_character, "description").text = "This table contains relative timestamp and residual phase"
        SubElement(table_character, "record_delimiter").text = "Carriage-Return Line-Feed"

        #"THIS CLASS DESCRIBES THE STRUCTURE OF ONE COMPLETE RECORD IN THE TABLE"
        record_character = SubElement(table_character, "Record_Character")

        with open(self.input_file, 'r') as file:
            for ip in range(5):
                line = file.readline()
            num_characters = len(line)
            num_columns = len(line.split())

        # Add the fields element
        SubElement(record_character, "fields").text = str(num_columns)
        SubElement(record_character, "groups").text = "0"

        field_number = 1
        line_offset = 1

        field_character_0 = SubElement(record_character, "Field_Character")
        SubElement(field_character_0, "name").text = "Scan timestamp"
        SubElement(field_character_0, "field_number").text = str(field_number)
        SubElement(field_character_0, "field_location").text = str(line_offset)
        SubElement(field_character_0, "data_type").text = "ASCII_Real"
        SubElement(field_character_0, "field_length").text = "24"
        SubElement(field_character_0, "description").text = "Timestamp with respect the beginning of the scan."

        field_number += 1
        line_offset += 24

        if '00' in self.input_file:
            field_character_1 = SubElement(record_character, "Field_Character")
            SubElement(field_character_1, "name").text = "Phase [rad]"
            SubElement(field_character_1, "field_number").text = str(field_number)
            SubElement(field_character_1, "field_location").text = str(line_offset)
            SubElement(field_character_1, "data_type").text = "ASCII_Real"
            SubElement(field_character_1, "field_length").text = "26"
            SubElement(field_character_1, "description").text = "Residual phase measurements"
        else:
            character_fields = []

            for ip in np.arange(start=1, stop=num_columns, step=1):
                character_fields.append(f'field_character_{ip}')

            for field in character_fields:
                field = SubElement(record_character, "Field_Character")
                SubElement(field, "name").text = "Phase [rad]"
                SubElement(field, "field_number").text = str(field_number)
                SubElement(field, "field_location").text = str(line_offset)
                SubElement(field, "data_type").text = "ASCII_Real"
                SubElement(field, "field_length").text = "26"
                SubElement(field, "description").text = "Residual phase measurements"

                field_number += 1
                line_offset += 26

        SubElement(record_character, "record_length").text = str(num_characters)

        # HOW TO DO HERE MULTIPLE COLUMNS, EACH COLUMN IS THEN FIELD_CHARACTER


    def fdets_columns(self, root):
        table_character = SubElement(root, "Table_Character")

        SubElement(table_character, "name").text = "Topocentric Doppler determination"
        SubElement(table_character, "offset").text = str(self.header_len)
        SubElement(table_character, "records").text = str(self.rows)
        SubElement(table_character, "description").text = "This table contains Timestamp, SNR, Specral Max, Frequency Doppler and residuals"
        SubElement(table_character, "record_delimiter").text = "Carriage-Return Line-Feed"

        with open(self.input_file, 'r') as file:
            for ip in range(5):
                line = file.readline()
            num_characters = len(line)
            num_columns = len(line.split())

        #"THIS CLASS DESCRIBES THE STRUCTURE OF ONE COMPLETE RECORD IN THE TABLE"
        record_character = SubElement(table_character, "Record_Character")
        SubElement(record_character, "fields").text = str(num_columns)
        SubElement(record_character, "groups").text = "0"
        SubElement(record_character, "record_length").text = str(num_characters)

        field_number = 1
        line_offset = 1

        if num_columns == 6:
            field_character_0 = SubElement(record_character, "Field_Character")
            SubElement(field_character_0, "name").text = "Index"
            SubElement(field_character_0, "field_number").text = str(field_number)
            SubElement(field_character_0, "field_location").text = str(line_offset)
            SubElement(field_character_0, "data_type").text = "ASCII_Integer"
            SubElement(field_character_0, "field_length").text = "5"
            SubElement(field_character_0, "description").text = "Index indicates the observed scan number"
            field_number += 1
            line_offset += 5

        field_character_1 = SubElement(record_character, "Field_Character")
        SubElement(field_character_1, "name").text = "UTC timestamp in ISO format"
        SubElement(field_character_1, "field_number").text = str(field_number)
        SubElement(field_character_1, "field_location").text = str(line_offset)
        SubElement(field_character_1, "data_type").text = "ASCII_Date_Time_YMD_UTC"
        SubElement(field_character_1, "field_length").text = "24"
        SubElement(field_character_1, "description").text = "Timestamp of the measurement expressed in ISO format." + \
            "The detection of the carrier is calculated as a function of the spectra, the time represents the " + \
            "middle point of the integration time. Therefore -> The first timestamp is equal to Initial scan time + integration_time / 2" + \
            "The next row will first timestamp + integration time."
        field_number += 1
        line_offset += 24

        field_character_2 = SubElement(record_character, "Field_Character")
        SubElement(field_character_2, "name").text = "Signal-to-Noise Ratio"
        SubElement(field_character_2, "field_number").text = str(field_number)
        SubElement(field_character_2, "field_location").text = str(line_offset)
        SubElement(field_character_2, "data_type").text = "ASCII_Real"
        SubElement(field_character_2, "field_length").text = "25"
        SubElement(field_character_2, "description").text = "Signal to Noise Ratio of the carrier with respect to the " + \
            "level of the ground floor. The SNR measurement depends on the spectral resolution and integration time selected"
        field_number += 1
        line_offset += 25

        field_character_3 = SubElement(record_character, "Field_Character")
        SubElement(field_character_3, "name").text = "Spectral max"
        SubElement(field_character_3, "field_number").text = str(field_number)
        SubElement(field_character_3, "field_location").text = str(line_offset)
        SubElement(field_character_3, "data_type").text = "ASCII_Real"
        SubElement(field_character_3, "field_length").text = "25"
        SubElement(field_character_3, "description").text = "Normalised value of the spectral peak of the particular spectra." + \
            "The spectral maxima is used to calculate the SNR."
        field_number += 1
        line_offset += 25

        field_character_4 = SubElement(record_character, "Field_Character")
        SubElement(field_character_4, "name").text = "Frequency detections"
        SubElement(field_character_4, "field_number").text = str(field_number)
        SubElement(field_character_4, "field_location").text = str(line_offset)
        SubElement(field_character_4, "data_type").text = "ASCII_Real"
        SubElement(field_character_4, "field_length").text = "22"
        SubElement(field_character_4, "description").text = "Topocentric frequency detection or Doppler measurements in Hz of the arrival" + \
            "frequency of the spacecraft carrier. We use a centroid bin to average the true position of the carrier with the near-by " + \
            "bins to minimise frequency smearing."
        field_number += 1
        line_offset += 22

        field_character_5 = SubElement(record_character, "Field_Character")
        SubElement(field_character_5, "name").text = "Frequency residual"
        SubElement(field_character_5, "field_number").text = str(field_number)
        SubElement(field_character_5, "field_location").text = str(line_offset)
        SubElement(field_character_5, "data_type").text = "ASCII_Real"
        SubElement(field_character_5, "field_length").text = "24"
        SubElement(field_character_5, "description").text = "Doppler noie or frequency residual in Hz. The difference between the measured " + \
            "frequency detected at the station and the frequency polynomial fit."

    def generate_xml(self, type):
        self.calculate_vars()

        # Create the root element
        xml_model_declaration = '<?xml-model href="http://pds.nasa.gov/pds4/pds/v1/PDS4_PDS_1J00.sch"?>\n'

        root = Element("Product_Observational")
        root.set("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance")
        root.set("xmlns", "http://pds.nasa.gov/pds4/pds/v1")
        root.set("xsi:schemaLocation", "http://pds.nasa.gov/pds4/pds/v1 http://pds.nasa.gov/pds4/pds/v1/PDS4_PDS_1J00.xsd")

        # Create Identification_Area
        identification_area = SubElement(root, "Identification_Area")

        SubElement(identification_area, "logical_identifier").text = f"urn:esa:psa:{BUNDLE}"
        SubElement(identification_area, "version_id").text = "1.0"
        SubElement(identification_area, "title").text = f"{self.spacecraft} Pride Instrument Topocentric frequency detections"
        SubElement(identification_area, "information_model_version").text = "1.19.0.0"
        SubElement(identification_area, "product_class").text = "Product_Observational"

        # Identification_Area - Citation Information
        citation_information = SubElement(identification_area, "Citation_Information")

        SubElement(citation_information, "author_list").text = "Molera Calvés, Guifré; "
        SubElement(citation_information, "publication_year").text = self.year_created
        SubElement(citation_information, "doi").text = "doi:10.1017/pasa.2021.56"
        SubElement(citation_information, "description").text = f"This observational product contains the frequency measurements of the {self.spacecraft} PRIDE experiment"

        # Identification_Area - Modification History
        modification_history = SubElement(identification_area, "Modification_History")
        modification_detail = SubElement(modification_history, "Modification_Detail")

        SubElement(modification_detail, "modification_date").text = self.modified_date
        SubElement(modification_detail, "version_id").text = self.version
        SubElement(modification_detail, "description").text = "Version of pysctrack used"

        # Observation Area
        observation_area = SubElement(root, "Observation_Area")

        # Observation Area -> Time Coordiantes
        time_coordinates = SubElement(observation_area, "Time_Coordinates")
        if type == 'fdets':
            SubElement(time_coordinates, "start_date_time").text = self.start_time
            SubElement(time_coordinates, "stop_date_time").text = self.stop_time

        # Observation Area -> Investigation Area
        investigation_area = SubElement(observation_area, "Investigation_Area")
        SubElement(investigation_area, "name").text = "Arrival frequency at the VLBI station"
        SubElement(investigation_area, "type").text = "Observing Campaign"
        SubElement(investigation_area, "Internal_Reference").text = f"urn:esa:psa:{BUNDLE}"

        # Observation Area -> Observation System
        observing_system = SubElement(observation_area, "Observing_System")
        SubElement(observing_system, "name").text = f"PRIDE-{self.spacecraft}"
        SubElement(observing_system, "description").text = "spacecraft"

        # Observation Area -> Observation System -> Observation System Component
        observing_system_component_1 = SubElement(observing_system, "Observing_System_Component")
        SubElement(observing_system_component_1, "name").text = f"{self.spacecraft} communications transponder"
        SubElement(observing_system_component_1, "type").text = "Instrument"

        internal_reference_1 = SubElement(observing_system_component_1, "Internal_Reference")
        SubElement(internal_reference_1, "lid_reference").text = "urn:esa:psa:"
        SubElement(internal_reference_1, "reference_type").text = "is_facility"

        # Observation Area - Target Identification
        target_identification = SubElement(observation_area, "Target_Identification")
        SubElement(target_identification, "name").text = f"{self.spacecraft}"
        SubElement(target_identification, "type").text = "Satellite"

        # Create Reference_List
        reference_list = SubElement(root, "Reference_List")
        internal_reference = SubElement(reference_list, "Internal_Reference")
        SubElement(internal_reference, "lidvid_reference").text = "urn:"
        SubElement(internal_reference, "reference_type").text = "data_to_calibrated_product"
        SubElement(internal_reference, "comment").text = "Another description"

        external_reference = SubElement(reference_list, "External_Reference")
        SubElement(external_reference, "doi").text = "doi:10.1017/pasa.2021.56"
        SubElement(external_reference, "reference_text").text = "High-Resolution spectrometer, PASA, 2021"
        # Internal References (Add as needed)

        #SubElement(file, "file_size").text = self.size
        #SubElement(file, "file_size").set("unit", "byte")
        #SubElement(file, "md5_checksum").text = self.md5
        #SubElement(file, "comment").text = "A brief introduction to the contents of the bundle"

        # Fle Area Observational
        file_area_observational = SubElement(root, "File_Area_Observational")
        file = SubElement(file_area_observational, "file")
        SubElement(file, "file_name").text = self.input_file
        SubElement(file, "creation_date_time").text = self.created_date
        SubElement(file, "record").text = str(self.rows + 4)

        if type == 'fdets':
            self.fdets_columns(file_area_observational)
        elif type == 'phase':
            self.phase_columns(file_area_observational)

        # Create XML tree
        tree = ElementTree(root)

        # Write to file
        # tree.write("output.xml", xml_declaration=True, encoding='UTF-8')

        # Write to file with indentation
        xml_string = xml_model_declaration + tostring(root, encoding='unicode', method='xml')
        xml_pretty_string = xml.dom.minidom.parseString(xml_string).toprettyxml(indent="    ")
        xml_pretty_string = xml_pretty_string.replace('\" x', '\"\n    x')

        with open(self.output_file, "w") as f:
            f.write(xml_pretty_string)
