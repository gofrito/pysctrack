#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May  8 19:57:29 2020

handle i/o files and internet calls
@author: gofrito
"""

import os
import telnetlib
import time

import numpy as np
import pandas as pd
import requests

from astroquery.jplhorizons import Horizons
from astropy.time import Time
from colorama import Fore, Style

# find the directory where pysctrack and catalogues are installed
PYSCTRACK_DIR = os.path.dirname(os.path.realpath(__file__))[:-3]
PYSCTRACK_CAT = os.path.join(PYSCTRACK_DIR, 'cat')

def calc_scans(data, fmt="fdets"):
    """
    Calculate the number of scans in a Fdets or Phase files

    Parameters
    ----------
    data : array
        numpy array with Fdets or Phases information.
    fmt : string
        specify file as fdets or phases. The former as default.

    Returns
    -------
    nscans : int
        number of scans in the session.

    """
    nscans: int = 0

    if fmt == "fdets":
        ts = int(data[1, 1] - data[0, 1])
        nrows, ncols = np.shape(data)
        if ncols == 6:
            nscans = 1
            for ip in np.arange(nrows - 1):
                if np.abs(data[ip + 1, 1] - data[ip, 1]) > 1.02 * ts:
                    nscans += 1
        else:
            # if number of columns is different than 6 means there is an error.
            print("file does not content correct amount of columns")

    elif fmt == "phases":
        ts = data[1, 0] - data[0, 0]
        nrows, ncols = np.shape(data)
        nscans = ncols - 1
    else:
        print(f"file format not correct")

    return nscans


def merge_fdets(filename, nfiles, flag):
    """
    Merge multiple Frequency detections file

    Parameters
    ----------
    filename : Fdets files
        text file.
    nfiles : int
        Total number of files to merge.
    flag : list
        File number to exclude

    """
    print('')

    fdets = np.array([])
    if os.path.isfile(filename):
        fd = open(filename, "r")
        header = []
        for ip in np.arange(4):
            header.append(fd.readline())
        fd.close()
        first = 1

    valid_scan: int= 0

    for nf in np.arange(1, nfiles):
        fdets_file = f"{filename[0:-13]}.{nf:04d}{filename[-8:-3]}txt"
        if os.path.isfile(fdets_file):
            if str(nf) in flag:
                print(f"Opening : {fdets_file}\033[93m Skipped {Style.RESET_ALL}")
            else:
                valid_scan += 1
                if first == 1:
                    fdets = read_fdets(fdets_file)
                    first = 0
                    print(f"Opening : {fdets_file}{Fore.GREEN} OK {Style.RESET_ALL}")
                else:
                    fdets = np.concatenate((fdets, read_fdets(fdets_file)))
                    print(f"Opening : {fdets_file}{Fore.GREEN} OK {Style.RESET_ALL}")
        else:
            print(f"Opening : {fdets_file}{Fore.RED} Not Found {Style.RESET_ALL}")

    # New output file name
    fdets_fn = f"{filename[0:-13]}{filename[-8:-3]}txt"

    # Add number of the scans at the end of line 2
    header_line2 = f'{header[1][2:-1]}Nscans: {valid_scan}\n'

    # Concatenate all the lins
    fdets_header = header[0][2:] + header_line2 + header[2][2:]

    # Save the file
    np.savetxt(fdets_fn, fdets, delimiter=" ", header=fdets_header)

    print(f'')
    print(f"File stored as: {fdets_fn}")


def merge_phases(filename, nfiles, flag):
    """
    Merge multiple residual phases file

    Parameters
    ----------
    filename : Phase files
        text file.
    nfiles : int
        Total number of files to merge.
    flag : list
        File number to exclude

    """
    phase = np.array([])
    if os.path.isfile(filename):
        fd = open(filename, "r")
        header = []
        for ip in np.arange(4):
            header.append(fd.readline())
        fd.close()
        first = 1

    for nf in range(1, nfiles):
        phase_file = f'{filename[0:-9]}.{nf:04d}.txt'
        if os.path.isfile(phase_file):
            if str(nf) in flag:
                print(f"Opening : {phase_file}\033[93m Skipped {Style.RESET_ALL}")
            else:
                if first == 1:
                    phase = read_phases(phase_file)
                    first = 0
                    print(f"Opening : {phase_file}{Fore.GREEN} OK {Style.RESET_ALL}")
                else:
                    phases = read_phases(phase_file)
                    phase = np.append(phase, np.delete(phases, 0, axis=1), axis=1)
                    print(f"Opening : {phase_file}{Fore.GREEN} OK {Style.RESET_ALL}")
        else:
            print(f"Opening : {phase_file}{Fore.RED} Not Found {Style.RESET_ALL}")

    phase_fn = f"{filename[0:-9]}.txt"
    phase_header = header[0][2:] + header[1][2:] + header[2][2:]
    np.savetxt(phase_fn, phase, delimiter=" ", header=phase_header)

    print(f"File stored as: {phase_fn}")


def read_fdets(fdets_file):
    """
    Read a file containing Frequency detections

    Parameters
    ----------
    fdets_file : string
        Filename for the Frequency detections file.

    Returns
    -------
    fdets : np.array
        Returns the aray with 6 columns and n number of rows.

    """
    fdets = np.loadtxt(fdets_file, skiprows=4)
    return fdets


def read_phases(phase_file):
    """
    Read phases

    Parameters
    ----------
    phase_file : TYPE
        DESCRIPTION.

    Returns
    -------
    phase : TYPE
        DESCRIPTION.

    """
    phase = np.loadtxt(phase_file, skiprows=4)
    return phase


def write_fdets(self, rev):
    """
    Writes to a text file the Frequency summary of the observations

    Parameters
    ----------
    fdets : class
        Includes main parameters extracted from the observations
    rev : int
        Version of the Frequency detections (swspec:0, sctracker:1, PLL:{2,3})

    Returns
    -------
    None.
    """
    # Create the new Fdets file
    fdets_fn: str = f"{self.path}Fdets.{self.spacecraft}{self.epoch}.{self.station}.{self.scan_number:04d}.r{rev}i.txt"

    # Write down the header of the Fdets file
    fdets_header: str = (
        f'Observation conducted on {self.epoch} at {self.station} rev. {rev} \nBase frequency: {self.baseband_freq:.2f} MHz dF: {self.df} Hz dT: {self.sampling_interval} s Nscans: 1 \nFormat: Modified JD   |       Time(UTC) [s]    | Signal-to-Noise ratio  |      Spectral max      |  Freq. detection [Hz]  |  Doppler noise [Hz] \n'
    )

    num_columns = np.shape(self.fdets.transpose())[1]

    if num_columns == 6:
        np.savetxt(fdets_fn, self.fdets.transpose(), fmt='%i %.3f %.18f %.18f %.3f %.18f', newline="\n", header=fdets_header)
    elif num_columns == 5:
        np.savetxt(fdets_fn, self.fdets.transpose(), fmt='%15s %.18f %.18f %.3f %.18f', newline="\n", header=fdets_header)
    # Save the file with numpy.savetxt
    #np.savetxt(fdets_fn, np.transpose(self.fdets), newline="\n", header=fdets_header)


def write_phase(self):
    """
    Save the residual phase into a text file. Format includes 4 lines of header and
    timestamp + phase

    Returns
    -------
    None.
    """
    phase_fn = f"{self.path}Phases.{self.spacecraft}{self.epoch}.{self.station}.{self.scan_number:04d}.txt"

    phase_header = f"Residual Phases session {self.epoch} at {self.station}\nMJD - Time [s] -  mSNR  - mDoppler noise \n \{int(self.fdets[0, 0])} {self.fdets[1, 0]:.1f} {np.mean(self.fdets[2, :]):.2f} {np.mean(self.fdets[5, :]):.3f}\n  Time stamp [s]    |     Phase [rad]"

    np.savetxt(phase_fn, np.transpose(self.phase), fmt="%.16e", delimiter=" ", newline="\n", header=phase_header)


def write_stats(self, rev):
    """
    Save in a file the stats extracted from calculateCpp

    Returns
    ------
    None.
    """
    stats_fn: str = f"{self.path}Stats.{self.spacecraft}{self.epoch}.{self.station}.{self.scan_number:04d}.txt"

    # Check if the file exists
    if os.path.isfile(stats_fn):
        # File exists append lines at the end
        fd = open (stats_fn, 'a')
        ap = np.loadtxt(stats_fn, skiprows=4)

        if (self.FFTpt in ap[:,0]) and (self.sampling_interval in ap[:,1]):
            print('Measurements done already')
        else:
            output = np.array([self.FFTpt, self.sampling_interval, self.SNR.mean(), self.rFit.std(), np.abs(self.Fdet[0] - self.Fdet[-1]), self.Fdet.max() - self.Fdet.min()])
            np.savetxt(fd, output.transpose())
    else:
        # Create a new file
        stats_header: str = f'Stats of the first detection with Calculate Cpp\n epoch: {self.epoch} at {self.station} scan {self.scan_number:04d}\n'
        output = np.array([self.FFTpt, self.sampling_interval, self.SNR.mean(), self.rFit.std(), np.abs(self.Fdet[0] - self.Fdet[-1]), self.Fdet.max() - self.Fdet.min()])

        np.savetxt(stats_fn, output.transpose(), header=stats_header)

def scint2array(filename):
    """
    Read values from the Scintillation Summary Table and extract a copy to
    a numpy array with specific labels for each column. It works in version 3.

    Parameters
    ----------
    filename : str
        filename of the Table with data.

    Returns
    -------
    sc : array_like
        scintilation values in a numpy array format.

    """
    scintTable = []
    with open(filename) as f:
        f_lines = f.readlines()

    for line in f_lines:
        if len(line) > 20 and line[0] != "/":
            line_parsed = line.replace("|", " ")
            line_parsed = line_parsed.replace("'", " ")
            scintTable.append(line_parsed)

    sc = np.loadtxt(
        scintTable,
        dtype=[
            ("TotalScanNr", "u2"),
            ("SessionNr", "u2"),
            ("ScanNr", "u2"),
            ("StationCode", "u2"),
            ("YY", "i2"),
            ("MM", "i2"),
            ("DD", "i2"),
            ("hh", "i2"),
            ("mm", "i2"),
            ("ss", "i2"),
            ("dur", "i2"),
            ("rah", "f2"),
            ("ram", "f2"),
            ("ras", "f2"),
            ("deh", "f2"),
            ("dem", "f2"),
            ("des", "f2"),
            ("az", "f2"),
            ("el", "f2"),
            ("LatEcl", "f2"),
            ("LonEcl", "f2"),
            ("AUdist", "f2"),
            ("SOT", "f2"),
            ("STO", "f2"),
            ("PhSc", "f2"),
            ("PhN", "f2"),
            ("ScintSlope", "f2"),
            ("ErrorSlope", "f2"),
            ("Peak3mHz", "f2"),
            ("SysNoise", "f1"),
            ("DopSNR", "f2"),
            ("SNR", "f4"),
            ("SolarAct", "f2"),
            ("Tx", "u1"),
            ("VEX_Ion_TEC", "f2"),
            ("Ceb_Ion_TEC", "f2"),
            ("TEC", "f2"),
        ],
    )

    return sc

'''
IS IT USE?
'''
def retrieve_jpleph(target, observer, startTime, stopTime):
    """
    Retrieve paramaters of the observation from NASA website. At the moment it allows,
    defining target,observer, startTime and stopTime

    Parameters
    ----------
    target : string
        name of the target of observation
    observer : string
        format long, lat, alt 147.439167,-42.805,0.043
    startTime : string
        format : yyyy-mm-dd HH:MM
    stopTime : string
        format : yyyy-mm-dd HH:MM

    Returns
    -------
    yy : TYPE
        DESCRIPTION.
    mm : TYPE
        DESCRIPTION.
    dd : TYPE
        DESCRIPTION.
    HH : TYPE
        DESCRIPTION.
    MM : TYPE
        DESCRIPTION.
    rah : TYPE
        DESCRIPTION.
    ram : TYPE
        DESCRIPTION.
    ras : TYPE
        DESCRIPTION.
    dech : TYPE
        DESCRIPTION.
    decm : TYPE
        DESCRIPTION.
    decs : TYPE
        DESCRIPTION.
    delta : TYPE
        DESCRIPTION.
    sot : TYPE
        DESCRIPTION.

    """
    HOST = "horizons.jpl.nasa.gov"
    PORT = 6775

    """
    Define what params we are interested from the Horizons web site
    1  Astrometric RA & DEC
    20 Observer range
    23 Sun-Observer-Target - elongation angle
    24 Sun-Target-Observer - phase engle
    """
    quantities = "1, 20, 23, 24\n"

    tn = telnetlib.Telnet(HOST, PORT)
    time.sleep(5)

    # Specify which spacecraft are we tracking (499 stands for MEX)
    if target == "Mars":
        label = "499"
    else:
        label = "0"

    tn.write("{}\n".format(label).encode("ascii"))
    time.sleep(5)

    # Read header and metadata information
    tn.read_until("<cr>:".encode("ascii"))

    tn.write("E\n".encode("ascii"))

    # Select Observer mode
    tn.read_until(" : ".encode("ascii"))
    tn.write("o\n".encode("ascii"))

    # Select Coordinate center
    tn.read_until(" : ".encode("ascii"))
    tn.write("coord\n".encode("ascii"))

    # Selected geodetic input
    tn.read_until(" : ".encode("ascii"))
    tn.write("g\n".encode("ascii"))

    # Select station coordinates, here we assume Hobart for sake's simplicity
    tn.read_until(" : ".encode("ascii"))
    tn.write("{}\n".format(observer).encode("ascii"))

    # Select today in order to get current information
    tn.read_until(" : ".encode("ascii"))
    tn.write("{}\n".format(startTime).encode("ascii"))
    tn.read_until(" : ".encode("ascii"))
    tn.write("{}\n".format(stopTime).encode("ascii"))
    tn.read_until(" : ".encode("ascii"))

    # Select only one line
    tn.write("60m\n".encode("ascii"))
    tn.read_until(" : ".encode("ascii"))

    # Accept default output? Yes.
    tn.write("y\n".encode("ascii"))
    tn.read_until(" : ".encode("ascii"))
    # Finally select which tables to import from NASA web site
    tn.write(quantities.encode("ascii"))
    ephemer = tn.read_until(" ? : ".encode("ascii"))
    start = ephemer.index("SOE".encode("ascii")) + 6

    yy = ephemer[start + 0 : start + 4].decode()
    mm = ephemer[start + 5 : start + 8].decode()
    dd = ephemer[start + 9 : start + 11].decode()
    HH = ephemer[start + 12 : start + 14].decode()
    MM = ephemer[start + 15 : start + 17].decode()

    rah = ephemer[start + 22 : start + 24].decode()
    ram = ephemer[start + 25 : start + 27].decode()
    ras = ephemer[start + 28 : start + 34].decode()

    dech = ephemer[start + 34 : start + 37].decode()
    decm = ephemer[start + 38 : start + 40].decode()
    decs = ephemer[start + 41 : start + 45].decode()

    delta = ephemer[start + 45 : start + 52].decode()
    sot = ephemer[start + 75 : start + 82].decode()
    sto = ephemer[start + 88 : start + 94].decode()

    return [yy, mm, dd, HH, MM, rah, ram, ras, dech, decm, decs, delta, sot, sto]

'''
IS IT USE?
'''
def retrieve_multiple_jpleph(target, observer, startTime, stopTime):
    """
    Same as above but here is allowed to download multiple times
    Parameters
    ----------
    target : string
        name of the target of observation
    observer : string
        format long, lat, alt 147.439167,-42.805,0.043
    startTime : string
        format : yyyy-mm-dd HH:MM
    stopTime : string
        format : yyyy-mm-dd HH:MM

    Returns
    -------
    yy : TYPE
        DESCRIPTION.
    mm : TYPE
        DESCRIPTION.
    dd : TYPE
        DESCRIPTION.
    HH : TYPE
        DESCRIPTION.
    MM : TYPE
        DESCRIPTION.
    rah : TYPE
        DESCRIPTION.
    ram : TYPE
        DESCRIPTION.
    ras : TYPE
        DESCRIPTION.
    dech : TYPE
        DESCRIPTION.
    decm : TYPE
        DESCRIPTION.
    decs : TYPE
        DESCRIPTION.
    delta : TYPE
        DESCRIPTION.
    sot : TYPE
        DESCRIPTION.

    """
    HOST = "horizons.jpl.nasa.gov"
    PORT = 6775

    """
    Define what params we are interested from the Horizons web site
    1  Astrometric RA & DEC
    4  Apparent AZ &
    18 Delta Delta-Range
    20 Observer range
    21 Oneway light time travel
    23 Sun-Observer-Target - elongation angle
    24 Sun-Target-Observer - phase engle
    """
    quantities = "1, 4, 18, 20, 21, 23, 24\n"

    tn = telnetlib.Telnet(HOST, PORT)
    time.sleep(5)

    # Specify which spacecraft are we tracking (499 stands for MEX)
    if target == "Mercury":
        label = "199"
    elif target == 'Venus':
        label = '299'
    elif target == "Mars":
        label = "499"
    elif target == "Jupiter":
        label = "599"
    elif target == 'Saturn':
        label = '699'
    else:
        label = "0"

    tn.write("{}\n".format(label).encode("ascii"))
    time.sleep(5)

    # Read header and metadata information
    tn.read_until("<cr>:".encode("ascii"))

    tn.write("E\n".encode("ascii"))

    # Select Observer mode
    tn.read_until(" : ".encode("ascii"))
    tn.write("o\n".encode("ascii"))

    # Select Coordinate center
    tn.read_until(" : ".encode("ascii"))
    tn.write("coord\n".encode("ascii"))

    # Selected geodetic input
    tn.read_until(" : ".encode("ascii"))
    tn.write("g\n".encode("ascii"))

    # Select station coordinates, here we assume Hobart for sake's simplicity
    tn.read_until(" : ".encode("ascii"))
    tn.write("{}\n".format(observer).encode("ascii"))

    # Select today in order to get current information
    tn.read_until(" : ".encode("ascii"))
    tn.write("{}\n".format(startTime).encode("ascii"))
    tn.read_until(" : ".encode("ascii"))
    tn.write("{}\n".format(stopTime).encode("ascii"))
    tn.read_until(" : ".encode("ascii"))

    # Select only one line
    tn.write("20m\n".encode("ascii"))
    tn.read_until(" : ".encode("ascii"))

    # Accept default output? Yes.
    tn.write("y\n".encode("ascii"))
    tn.read_until(" : ".encode("ascii"))
    # Finally select which tables to import from NASA web site
    tn.write(quantities.encode("ascii"))
    eph = tn.read_until(" ? : ".encode("ascii"))
    # print(ephemer)

    start = eph.index("SOE".encode("ascii")) + 6

    # Initialise things
    yy = []
    mm = []
    dd = []
    HH = []
    MM = []
    rah = []
    ram = []
    ras = []
    dech = []
    decm = []
    decs = []
    az = []
    el = []
    eclLat = []
    eclLon = []
    delta = []
    deltadot = []
    onewaylt = []
    sot = []
    sto = []

    total: int = 148
    # It is not the most elegant what it calculates the amount of days to retrieve information.
    n_days: int = int(stopTime.split('-')[2].split(' ')[0]) - int(startTime.split('-')[2].split(' ')[0])
    # We need a for loop to read all line
    for ip in np.arange(24 * 3 * n_days):
        yy.append(eph[start + ip * total : start + ip * total + 4].decode())
        mm.append(eph[start + ip * total + 5 : start + ip * total + 8].decode())
        dd.append(eph[start + ip * total + 9 : start + ip * total + 11].decode())
        HH.append(eph[start + ip * total + 12 : start + ip * total + 14].decode())
        MM.append(eph[start + ip * total + 15 : start + ip * total + 17].decode())
        rah.append(eph[start + ip * total + 22 : start + ip * total + 24].decode())
        ram.append(eph[start + ip * total + 25 : start + ip * total + 27].decode())
        ras.append(eph[start + ip * total + 28 : start + ip * total + 32].decode())
        dech.append(eph[start + ip * total + 34 : start + ip * total + 37].decode())
        decm.append(eph[start + ip * total + 38 : start + ip * total + 40].decode())
        decs.append(eph[start + ip * total + 41 : start + ip * total + 45].decode())
        az.append(eph[start + ip * total + 45 : start + ip * total + 51].decode())
        el.append(eph[start + ip * total + 55 : start + ip * total + 60].decode())
        eclLon.append(eph[start + ip * total + 64 : start + ip * total + 69].decode())
        eclLat.append(eph[start + ip * total + 74 : start + ip * total + 78].decode())
        delta.append(eph[start + ip * total + 82 : start + ip * total + 88].decode())
        deltadot.append(eph[start + ip * total + 101 : start + ip * total + 107].decode())
        onewaylt.append(eph[start + ip * total + 114 : start + ip * total + 120].decode())
        sot.append(eph[start + ip * total + 128 : start + ip * total + 134].decode())
        sto.append(eph[start + ip * total + 140 : start + ip * total + 146].decode())

    return [yy, mm, dd, HH, MM, rah, ram, ras, dech, decm, decs, az, el, eclLon, eclLat, delta, deltadot, onewaylt, sot, sto]


def station_coord(antenna_code):
    coor = [
        "24.3930,60.2178,0.07\n",
        "11.6469,44.5205,0.067\n",
        "16.7040,40.6495,0.543\n",
        "14.9890,36.8760,0.143\n",
        "12.8774,49.1449,0.67\n",
        "356.9131,40.5247,0.988\n",
        "37.6283,54.8206,0.239\n",
        "54.393056,11.917778,0\n",
        "27.6844,-25.8898,1.37\n",
        "121.19944,31.0992,0.029\n",
        "102.7959,25.0273,1.974\n",
        "87.1781,43.4715,2.033\n",
        "27.6844,-25.8898,1.37\n",
        "29.78194,60.5322,0.086\n",
        "41.565,43.787,1.175\n",
        "102.2338,51.7703,0.821\n",
        "121.1360,31.0921,0.049\n",
        "174.6631,-36.4347,0.127\n",
        "147.439167,-42.805,0.043\n",
        "147.439167,-42.805,0.043\n",
        "115.3467,-29.0464,0.244\n",
        "132.1527,-14.3754,0.203\n",
        "174.6631,-36.4347,0.127\n",
        "12.8774,49.1449,0.67\n",
        "12.8774,49.1449,0.67\n",
        "126.9430,37.5622,0.5\n",
        "133.8098,-31.8677,0.164\n",
    ]
    coord = coor[antenna_code - 1]
    return coord

'''
NO USED 19.11.2021
'''
def ephem_scintTable(self):
    HOST = "horizons.jpl.nasa.gov"
    PORT = 6775
    quantities = "1, 4, 18, 20, 23, 24 \n"

    tn = telnetlib.Telnet(HOST, PORT)
    time.sleep(5)

    if self.sc == "m":
        tn.write("499\n".encode("ascii"))
    elif self.sc == "v":
        tn.write("299\n".encode("ascii"))
    elif self.sc == "r":
        tn.write("16543\n".encode("ascii"))
    time.sleep(5)
    tn.read_until("<cr>:".encode("ascii"))

    tn.write("E\n".encode("ascii"))
    tn.read_until(" : ".encode("ascii"))
    tn.write("o\n".encode("ascii"))
    tn.read_until(" : ".encode("ascii"))
    tn.write("coord\n".encode("ascii"))
    tn.read_until(" : ".encode("ascii"))
    tn.write("g\n".encode("ascii"))
    tn.read_until(" : ".encode("ascii"))
    tn.write(station_coord(self.code).encode("ascii"))
    tn.read_until(" : ".encode("ascii"))
    start = "{}-{}-{} {}\n".format(self.yy, self.mm, self.dd, self.startt)
    if int(self.stopt[0:2]) < int(self.startt[0:2]):
        self.dd = self.dd.replace(self.dd[0:2], str(int(self.dd[0:2]) + 1))
    stop = "{}-{}-{} {}\n".format(self.yy, self.mm, self.dd, self.stopt)
    tn.write(start.encode("ascii"))
    tn.read_until(" : ".encode("ascii"))
    tn.write(stop.encode("ascii"))
    tn.read_until(" : ".encode("ascii"))
    tn.write("20m\n".encode("ascii"))
    tn.read_until(" : ".encode("ascii"))
    tn.write("y\n".encode("ascii"))
    tn.read_until(" : ".encode("ascii"))
    tn.write(quantities.encode("ascii"))
    ephemerides = tn.read_until(" ? : ".encode("ascii"))

    return ephemerides

'''
extract_ephemerides -> obsolete in benefit of horizons_api
NO LONGER USED 19.11.2021
'''
def extract_ephemerides(eph, nph):
    start = eph.index("SOE".encode("ascii")) + 6
    total = 134
    yy = []
    mm = []
    dd = []
    HH = []
    MM = []
    rah = []
    ram = []
    ras = []
    dech = []
    decm = []
    decs = []
    az = []
    el = []
    eclLon = []
    eclLat = []
    delta = []
    sot = []
    sto = []
    for ip in np.arange(nph):
        yy.append(eph[start + ip * total : start + ip * total + 4])
        mm.append(eph[start + ip * total + 5 : start + ip * total + 8])
        dd.append(eph[start + ip * total + 9 : start + ip * total + 11])
        HH.append(eph[start + ip * total + 12 : start + ip * total + 14])
        MM.append(eph[start + ip * total + 15 : start + ip * total + 17])
        rah.append(eph[start + ip * total + 22 : start + ip * total + 24])
        ram.append(eph[start + ip * total + 25 : start + ip * total + 27])
        ras.append(eph[start + ip * total + 28 : start + ip * total + 32])
        dech.append(eph[start + ip * total + 34 : start + ip * total + 37])
        decm.append(eph[start + ip * total + 38 : start + ip * total + 40])
        decs.append(eph[start + ip * total + 41 : start + ip * total + 45])
        az.append(eph[start + ip * total + 45 : start + ip * total + 51])
        el.append(eph[start + ip * total + 55 : start + ip * total + 60])
        eclLon.append(eph[start + ip * total + 64 : start + ip * total + 69])
        eclLat.append(eph[start + ip * total + 74 : start + ip * total + 78])
        delta.append(eph[start + ip * total + 82 : start + ip * total + 88])
        sot.append(eph[start + ip * total + 111 : start + ip * total + 117])
        sto.append(eph[start + ip * total + 124 : start + ip * total + 129])

    return (yy, mm, dd, HH, MM, rah, ram, ras, dech, decm, decs, az, el, eclLon, eclLat, delta, sot, sto)


def read_scintTable(filename):
    """
    Read the table of the scintillation results

    Parameters
    ----------
    filename : string
        full path of the scintillation table.

    Returns
    -------
    df : data Frame
        Pandas data frama with all the fields in the table.
    """
    tags = [
        "Observation",
        "Run",
        "Scan",
        "Station",
        "Year",
        "Month",
        "Day",
        "Hour",
        "Minutes",
        "Seconds",
        "Duration",
        "RAh",
        "RAm",
        "RAs",
        "DEd",
        "DEm",
        "DEs",
        "Azimuth",
        "Elevation",
        "Longitude",
        "Latitude",
        "Distance",
        "SOT",
        "STO",
        "Scint",
        "SysNoise",
        "SciSlope",
        "ErrSlope",
        "PeakSPD",
        "Noise",
        "Dnoise",
        "CarrierSNR",
        "SolarAct",
        "GroundStation",
        "DownIonos",
        "UpIonos",
        "TEC",
    ]

    df = pd.read_csv(filename, sep="\s+", header=None, skiprows=5, skipfooter=91, engine="python", names=tags)

    return df


def station_code(antenna_code):
    Tantennas = 28
    antenna = ["Mh", "Mc", "Ma", "Nt", "Wz", "Ys", "Pu", "On", "Hh", "Sh", "Km", "Ur", "Ht", "Sv", "Zc", "Bd", "T6", "Ww", "Ho", "Hb", "Yg", "Ke", "Wa", "Wn", "Wd", "Ul", "Cd", "Mp"]

    code = np.arange(1, Tantennas + 1)
    codeInd = code[antenna.index(antenna_code)]
    return codeInd


def assign_spacecraft(code: str):
    '''
    code is the first letter of the spectra file

    returns: the three letter code of the spacecraft
    '''
    if code.startswith('b'):
        spacecraft = "bco"
    elif code.startswith('c'):
        spacecraft = "cal"
    elif code.startswith('g'):
        spacecraft = "gai"
    elif code.startswith('i'):
        spacecraft = "min"
    elif code.startswith('j'):
        spacecraft = "jui"
    elif code.startswith('k'):
        spacecraft = "ras"
    elif code.startswith('m'):
        spacecraft = "mex"
    elif code.startswith('r'):
        spacecraft = "ros"
    elif code.startswith('s'):
        spacecraft = "sol"
    elif code.startswith('v'):
        spacecraft = "vex"
    else:
        spacecraft = "non"

    return spacecraft


def extract_crd(station_code):
    """
    Returns the geodetic coordernates from the glo.crd file

    Parameters
    ----------
    station_code : str
        Two letters station code

    Returns
    -------
    lon
        Float with the station's longitude.
    lat
        Float with the station's latitude
    alt
        Station's altitude

    """
    crd_fn = os.path.join(PYSCTRACK_CAT, 'glo.crd')
    db = pd.read_csv(crd_fn, delimiter=" ", skipinitialspace=True, engine="python", skiprows=7, names=["sta", "cod", "lon", "lat", "alt"])

    st = db[db.cod == station_code]
    return (st.lon.item(), st.lat.item(), st.alt.item())


def extract_mex(ant_code):
    """
    Returns the default values from the MEX observations
    based on Auscope antennas primarily

    Parameters
    ----------
    station_code : str
        Two letters station code

    Returns
    -------
    bandwidth : str
        default bandwidth used
    FreqMinim : str
        Lower frequency limit
    FreqMaxim : str
        Higher frequency limit
    RadioFrequency : str
        Sky frequency of the baseband converter

    """
    mex_fn = os.path.join(PYSCTRACK_CAT, 'spc.mex')
    db = pd.read_csv(mex_fn, delimiter=" ", skipinitialspace=True, engine="python", skiprows=9, names=["ant", "cod", "bw", "f0", "f1", "rf"])

    param = db[db.code == ant_code]
    return (float(param.bw.item()), float(param.f0.item()), float(param.f1.item()), float(param.rf.item()))

def horizons_api(start_time='2021-10-11 08:00', stop_time='2021-10-11 10:00', spacecraft='-41', station='75', step_size="'20 m'", quantities="'1,4,18,20,23,24'"):
    fd = f"!$$SOF\n \
        COMMAND= {spacecraft} \
        OBJ_DATA='YES' \
        MAKE_EPHEM='YES' \
        TABLE_TYPE='OBSERVER' \
        CENTER={station} \
        START_TIME={start_time} \
        STOP_TIME={stop_time} \
        STEP_SIZE={step_size} \
        QUANTITIES={quantities}"

    return fd

def return_naif(target: str):
    sc_id_fn: str = os.path.join(PYSCTRACK_CAT, 'sc_id.dat')

    # Load the text file
    a = pd.read_csv(sc_id_fn, sep="\s+", header=None, skiprows=11, skipfooter=0, engine="python", names=['naif', 'craft'])
    a[target]


def calculate_doppler(rx: str, tx: str, target: str, bos: str, eos: str):

    beginning_of_scan = Time(bos, format='isot', scale='utc')
    end_of_scan = Time(eos, format='isot', scale='utc')

    duration = end_of_scan - beginning_of_scan

    # Use the initial times to calculate the steps in seconds
    num_of_steps: str = str(int(duration.jd2 * 86400))

    # Horizons class for the uplink
    obj_transmitter = Horizons(id=f'{target}', location='New Norcia (35-m, ESTrack DSA-1 NNO-1)', \
                            epochs={'start':beginning_of_scan, 'stop':end_of_scan, 'step':num_of_steps})

    # Horizons class for the downlink
    obj_receiver = Horizons(id=f'{target}', location=f'{rx}', \
                        epochs={'start':beginning_of_scan, 'stop':end_of_scan, 'step':num_of_steps})

    # Use the range rate from ephemerides to calculate in km/s
    uplink_range_rate = obj_transmitter.ephemerides()['delta_rate']
    dwlink_range_rate = obj_receiver.ephemerides()['delta_rate']

    # Convert to frequency Doppler
    uplink_doppler = -uplink_range_rate * 1e3 * 7180.2284e6 / 299792458
    dwlink_doppler = -dwlink_range_rate * 1e3 * 8436.04938e6 / 299792458