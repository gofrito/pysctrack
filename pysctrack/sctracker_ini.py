#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 27 10:49:40 2023

v0.1 ini version
@author: gofrito
"""
import configparser
import numpy as np
import os

config = configparser.ConfigParser()

class SWspecINI:
    '''
    A class to read inifile for pysdtracker
    '''
    def __init__(self, input_file: str):
        self.ini_file = input_file

    def read_ini_file(self):
        if os.path.exists(self.ini_file):
            config.read(self.ini_file)
        else:
            raise FileExistsError

        self.source_format = config['Spectrometer']['SourceFormat']
        self.bitsps = int(config['Spectrometer']['BitsPerSample'])
        self.channel = config['Spectrometer']['UseFile1Channel']
        self.bandwidth = float(config['Spectrometer']['BandwidthHz'])
        self.fftpoints = float(config['Spectrometer']['FFTpoints'])
        self.int_time = int(config['Spectrometer']['FFTIntegrationTimeSec'])
        self.start_time = int(config['Spectrometer']['SourceSkipSeconds'])
        self.stop_time = int(config['Spectrometer']['SourceStopSeconds'])
        self.overlap = int(config['Spectrometer']['FFToverlapFactor'])
        self.window_type = config['Spectrometer']['WindowType']

        return self


class SctrackerINI:
    '''
    A class to populate the inifile for sctracker
    '''
    tone_offset = 'OffsetsNone.txt'
    sink_format = 'Binary'
    sc_tracking = 'Yes'
    double_prec = 'False'
    output_spec = 'False'
    overlap_sam = 1
    poly_sign   = -1
    coeff_type  = 'SampleBased'
    lock_spec   = 5
    overlap     = 2
    padding     = 1
    window      = 'Cosine'
    out_window  = 'Cosine'
    out_bw      = 2000
    num_core    = 1
    buf_size    = 128

    def __init__(self, input_file: str, skip_s: int, stop_s: int, scan_num: int, order: int=6):
        path, filename = os.path.split(input_file)

        runlog = input_file.replace('swspec.bin', 'runlog.txt')

        self.station = filename.split('_')[1]

        with open(runlog, 'r') as fd:
            lines = fd.readlines()
            self.input_source = lines[1].split(':')[1].replace(" ", "")

        # Start or skip time
        self.skip_sec = skip_s

        # Add extra seconds to have a full tone if not equal to 0
        if stop_s == 0:
            self.stop_sec = stop_s
        else:
            self.stop_sec = stop_s + 20

        # Manually selected scan
        self.scan_number = scan_num

        # Phase polynomial order
        self.poly_order = order

        self.input_file = input_file

    def read_swspec(self):
        config.read(f'inifile.{self.station}')

        self.source_format = config['Spectrometer']['SourceFormat']
        self.bitsps = config['Spectrometer']['BitsPerSample']
        self.channels = config['Spectrometer']['SourceChannels']
        self.channel = config['Spectrometer']['UseFile1Channel']
        self.bandwidth = config['Spectrometer']['BandwidthHz']
        self.fftpoints = config['Spectrometer']['FFTpoints']
        self.int_time = int(config['Spectrometer']['FFTIntegrationTimeSec'])
        path, filename = os.path.split(self.input_file)
        session_name = filename.split('_')

        self.base_fn = f'{session_name[0]}_{session_name[1]}_{session_name[2]}_{session_name[3][0:2]}'

    def output_files(self):
        self.PhasePolyCpmFile = self.PhasePolyCppFile = f'{self.base_fn}{self.scan_number:04d}_{self.fftpoints.replace("e3","000")}pt_{self.int_time}s_ch{self.channel}.poly{self.poly_order}.txt'
        self.base_filename = f'{self.base_fn}{self.scan_number:04d}_%fftpoints%pt_%integrtime%s_ch%channel%'

    def write_ini(self):
        # Read paramaters from swspec inifile
        self.read_swspec()

        # Create the output filenames
        self.output_files()

        with open(f'inifile{self.station}{self.scan_number}', 'w+') as fd:
            fd.write('[Settings]\n')
            fd.write(f'InputSource          = {self.input_source}')
            fd.write(f'ToneOffsetsFile      = {self.tone_offset}\n')
            fd.write('\n')
            fd.write(f'SourceFormat         = {self.source_format}\n')
            fd.write(f'SinkFormat           = {self.sink_format}\n')
            fd.write(f'DoSpacecraftTracking = {self.sc_tracking}\n')
            fd.write(f'WriteDoublePrecision = {self.double_prec}\n')
            fd.write(f'WriteOutputSpectra   = {self.output_spec}\n')
            fd.write('\n')
            fd.write(f'BitsPerSample        = {self.bitsps}\n')
            fd.write(f'SourceChannels       = {self.channels}\n')
            fd.write(f'UseChannel           = {self.channel}\n')
            fd.write(f'BandwidthHz          = {self.bandwidth}\n')
            fd.write(f'OverSamplingFactor   = {self.overlap_sam}\n')
            fd.write(f'SourceSkipSeconds    = {self.skip_sec}\n')
            fd.write(f'SourceStopSeconds    = {self.stop_sec}\n')
            fd.write('\n')
            fd.write(f'PhasePolySign        = {self.poly_sign}\n')
            fd.write(f'PhasePolyCoeffType   = {self.coeff_type}\n')
            fd.write(f'PhasePolyOrder       = {self.poly_order}\n')
            fd.write(f'PhasePolyCpmFile     = {self.PhasePolyCpmFile}\n')
            fd.write(f'PhasePolyCppFile     = {self.PhasePolyCppFile}\n')
            fd.write(f'PhaseLockSpectra     = {self.lock_spec}\n')
            fd.write('\n')
            fd.write(f'FFTpoints            = {self.fftpoints}\n')
            fd.write(f'FFTIntegrationTimeSec= {self.int_time}\n')
            fd.write(f'FFToverlapFactor     = {self.overlap}\n')
            fd.write(f'PaddingFactor        = {self.padding}\n')
            fd.write(f'WindowType           = {self.window}\n')
            fd.write(f'WindowOutType        = {self.out_window}\n')
            fd.write(f'FilterBandwidthHz    = {self.out_bw}\n')
            fd.write('\n')
            fd.write(f'NumCores             = {self.num_core}\n')
            fd.write(f'MaxRawBufSize        = {self.buf_size}\n')
            fd.write('\n')
            fd.write('# General pattern for filenames\n')
            fd.write(f'BaseFilename         = {self.base_filename}\n')
            fd.write('\n')
            fd.write('# Appended pattern for tone files\n')
            fd.write('ToneOutPattern       = _tone%tonenr%.bin')
        os.chmod(f'inifile{self.station}{self.scan_number}', 0o755)

        with open('OffsetsNone.txt', 'w+', encoding='utf-8') as fd:
            fd.write('                       0                       0                       0')
        os.chmod("OffsetsNone.txt", 0o755)

#inifile_test = SWspecINI('inifile.Hb')
#inifile_test.read_ini_file()
#ini_file = SctrackerINI('s220826_Hb_VDIX_No0001_1600000pt_2s_ch1_swspec.bin', 0, 1000, 2)
#ini_file.write_ini()