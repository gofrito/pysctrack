#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 27 10:49:40 2023

v0.1 original version from pre-repo
@author: gofrito
"""
import json
import os

import pandas as pd

class Spacecraft:
    '''
    class spacecraft
    dw_freq: float
    up_freq: float
    '''

    # File structure based on my data base
    repos_path: str = os.getenv('REPOS_PATH')
    PYSCTRACK = f'{repos_path}pysctrack/'

    def __init__(self, code):
        self.target: str = code

    def load_table(self):
        with open(f'{self.PYSCTRACK}cats/sc.json', 'r') as json_file:
                data = json.load(json_file)


        self.json_sc = data

    def load_json(self):
        self.df_sc = pd.read_json(f'{self.PYSCTRACK}cats/sc.json')

        # Use the letters code as index
        self.df_sc.set_index(self.df_sc['code'], inplace=True)
        return self.df_sc

    def return_frequencies(self):
        '''
        Return the up and downlink theoretical frequencies
        '''
        self.load_json()

        up_link = self.df_sc.loc[self.df_sc['code'] == self.target].up_link
        dw_link = self.df_sc.loc[self.df_sc['code'] == self.target].down_link_x

        return [up_link, dw_link]

    def return_code(self):
        '''
            Return space craft mission name
        '''
        mission_name = self.df_sc[self.df_sc['code']==self.target]['code']

        return mission_name

    def return_names(self):
        '''
            Return space craft mission name
        '''
        mission_name = self.df_sc[self.df_sc['code']==self.target]['names'].values[0]#[0][0]

        return mission_name

    def return_naif(self):
        '''
        return naif code for Horizons
        '''
        naif = str(self.df_sc[self.df_sc['code']==self.target]['naif_id'].values[0])

        return naif

    def return_esa(self):
        '''
        return ESA kernel as specified in webgeocalc
        '''
        esa_id = int(self.df_sc[self.df_sc['code']==self.target]['esa_id'].values[0])

        return esa_id

    def return_transmit(self):
        tx_power = float(self.df_sc[self.df_sc['code']==self.target]['tx_power'].values[0])
        tx_diameter = float(self.df_sc[self.df_sc['code']==self.target]['tx_diameter'].values[0])
        tx_eff = float(self.df_sc[self.df_sc['code']==self.target]['tx_eff'].values[0])

        return tx_power, tx_diameter, tx_eff