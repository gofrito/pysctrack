#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May  8 13:21:04 2020

@author: gofrito
"""
import os
#from distutils.command.sdist import sdist

from setuptools import find_packages, setup

from pysctrack import __version__

def read(fn):
    f = open(f'{fn}.md')
    print(f.read())

if __name__ == '__main__':
    # package name
    NAME = 'pysctrack'
    # executable scripts in bin
    BIN = 'bin/'

    setup(
 #       cmdclass={"sdist": sdist},
        name=NAME,
        description='Python tools for Doppler spacecraft, Interplanetary Scintillation and Coronal Mass Ejections',
        version=__version__,
        author='Guifré Molera Calvés',
        author_email='gofrito@protonmail.com',
        url='https://gitlab.org/gofrito/pysctrack',
        packages=find_packages(),

        # data files support
        #packages=find_packages("cats"),
        #package_dir={"":"cats"},
        #include_package_data=True,

        # long_description = read('README'),
        install_requires=['astropy', 'colorama', 'numpy', 'matplotlib', 'pandas', 'pyproj', 'altair', 'docutils', 'plotly'],
        classifiers=['Development Status :: 5 = Production/Stable', 'Framework :: IDLE', 'Framework :: IPython', 'Programming Language :: Python :: 3.8'],
    )
